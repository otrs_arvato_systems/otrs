package Kernel::System::Ticket::SLAHandling;

use strict;
use warnings;

our $ObjectManagerDisabled = 1;

# disable redefine warnings in this scope
{
no warnings 'redefine';

# as example redefine the TicketXXX() of Kernel::System::Ticket
sub Kernel::System::Ticket::TicketEscalationDateCalculation {
  my ( $Self, %Param ) = @_;

  # check needed stuff
  for my $Needed (qw(Ticket UserID)) {
      if ( !defined $Param{$Needed} ) {
          $Kernel::OM->Get('Kernel::System::Log')->Log(
              Priority => 'error',
              Message  => "Need $Needed!"
          );
          return;
      }
  }

  # get ticket attributes
  my %Ticket = %{ $Param{Ticket} };

  # do no escalations on (merge|close|remove) tickets
  return if $Ticket{StateType} eq 'merged';
  return if $Ticket{StateType} eq 'closed';
  return if $Ticket{StateType} eq 'removed';

  # get escalation properties
  my %Escalation = $Self->TicketEscalationPreferences(
      Ticket => $Param{Ticket},
      UserID => $Param{UserID} || 1,
  );

  # return if we do not have any escalation attributes
  my %Map = (
      EscalationResponseTime => 'FirstResponse',
      EscalationUpdateTime   => 'Update',
      EscalationSolutionTime => 'Solution',
  );
  my $EscalationAttribute;
  KEY:
  for my $Key ( sort keys %Map ) {
      if ( $Escalation{ $Map{$Key} . 'Time' } ) {
          $EscalationAttribute = 1;
          last KEY;
      }
  }

  return if !$EscalationAttribute;

  # get time object
  my $TimeObject = $Kernel::OM->Get('Kernel::System::Time');

  # calculate escalation times based on escalation properties
  my $Time = $TimeObject->SystemTime();

  my %Data;

  TIME:
  for my $Key ( sort keys %Map ) {

      next TIME if !$Ticket{$Key};

      ## NEUM032 Changed part starts here
      # Add seconds spent in a paused sla state to time granted by sla
      $Ticket{$Key} += $Self->SecondsInSuspensionState(
        TicketID => $Ticket{TicketID},
        Calendar => $Escalation{Calendar},
      );
      ## NEUM032 Changed part ends here

      # get time before or over escalation (escalation_destination_unixtime - now)
      my $TimeTillEscalation = $Ticket{$Key} - $Time;

      # ticket is not escalated till now ($TimeTillEscalation > 0)
      my $WorkingTime = 0;
      if ( $TimeTillEscalation > 0 ) {

          $WorkingTime = $TimeObject->WorkingTime(
              StartTime => $Time,
              StopTime  => $Ticket{$Key},
              Calendar  => $Escalation{Calendar},
          );

          # extract needed data
          my $Notify = $Escalation{ $Map{$Key} . 'Notify' };
          my $Time   = $Escalation{ $Map{$Key} . 'Time' };

          # set notification if notify % is reached
          if ( $Notify && $Time ) {

              my $Reached = 100 - ( $WorkingTime / ( $Time * 60 / 100 ) );

              if ( $Reached >= $Notify ) {
                  $Data{ $Map{$Key} . 'TimeNotification' } = 1;
              }
          }
      }

      # ticket is overtime ($TimeTillEscalation < 0)
      else {
          $WorkingTime = $TimeObject->WorkingTime(
              StartTime => $Ticket{$Key},
              StopTime  => $Time,
              Calendar  => $Escalation{Calendar},
          );
          $WorkingTime = "-$WorkingTime";

          # set escalation
          $Data{ $Map{$Key} . 'TimeEscalation' } = 1;
      }
      my $DestinationDate = $TimeObject->SystemTime2TimeStamp(
          SystemTime => $Ticket{$Key},
      );
      $Data{ $Map{$Key} . 'TimeDestinationTime' } = $Ticket{$Key};
      $Data{ $Map{$Key} . 'TimeDestinationDate' } = $DestinationDate;
      $Data{ $Map{$Key} . 'TimeWorkingTime' }     = $WorkingTime;
      $Data{ $Map{$Key} . 'Time' }                = $TimeTillEscalation;

      # set global escalation attributes (set the escalation which is the first in time)
      if (
          !$Data{EscalationDestinationTime}
          || $Data{EscalationDestinationTime} > $Ticket{$Key}
          )
      {
          $Data{EscalationDestinationTime} = $Ticket{$Key};
          $Data{EscalationDestinationDate} = $DestinationDate;
          $Data{EscalationTimeWorkingTime} = $WorkingTime;
          $Data{EscalationTime}            = $TimeTillEscalation;

          # escalation time in readable way
          $Data{EscalationDestinationIn} = '';
          $WorkingTime = abs($WorkingTime);
          if ( $WorkingTime >= 3600 ) {
              $Data{EscalationDestinationIn} .= int( $WorkingTime / 3600 ) . 'h ';
              $WorkingTime = $WorkingTime
                  - ( int( $WorkingTime / 3600 ) * 3600 );    # remove already shown hours
          }
          if ( $WorkingTime <= 3600 || int( $WorkingTime / 60 ) ) {
              $Data{EscalationDestinationIn} .= int( $WorkingTime / 60 ) . 'm';
          }
      }
  }

  return %Data;
}

sub Kernel::System::Ticket::_TicketGetFirstResponse {
    my ( $Self, %Param ) = @_;

    # check needed stuff
    for my $Needed (qw(TicketID Ticket)) {
        if ( !defined $Param{$Needed} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "Need $Needed!"
            );
            return;
        }
    }

    # get database object
    my $DBObject = $Kernel::OM->Get('Kernel::System::DB');

    # check if first response is already done
    return if !$DBObject->Prepare(
        SQL => 'SELECT a.create_time,a.id FROM article a, article_sender_type ast, article_type art'
            . ' WHERE a.article_sender_type_id = ast.id AND a.article_type_id = art.id AND'
            . ' a.ticket_id = ? AND ast.name = \'agent\' AND'
            . ' (art.name LIKE \'email-ext%\' OR art.name LIKE \'note-ext%\' OR art.name = \'phone\' OR art.name = \'fax\' OR art.name = \'sms\')'
            . ' ORDER BY a.create_time',
        Bind  => [ \$Param{TicketID} ],
        Limit => 1,
    );

    my %Data;
    while ( my @Row = $DBObject->FetchrowArray() ) {
        $Data{FirstResponse} = $Row[0];

        # cleanup time stamps (some databases are using e. g. 2008-02-25 22:03:00.000000
        # and 0000-00-00 00:00:00 time stamps)
        $Data{FirstResponse} =~ s/^(\d\d\d\d-\d\d-\d\d\s\d\d:\d\d:\d\d)\..+?$/$1/;
    }

    return if !$Data{FirstResponse};

    # get escalation properties
    my %Escalation = $Self->TicketEscalationPreferences(
        Ticket => $Param{Ticket},
        UserID => $Param{UserID} || 1,
    );

    if ( $Escalation{FirstResponseTime} ) {

        ## NEUM032 Changed part starts here
        $Escalation{FirstResponseTime} += int ( $Self->SecondsInSuspensionState(
          TicketID => $Param{TicketID},
          Calendar => $Escalation{Calendar},
        ) /60 );
        ## NEUM032 Changed part ends here

        # get time object
        my $TimeObject = $Kernel::OM->Get('Kernel::System::Time');

        # get unix time stamps
        my $CreateTime = $TimeObject->TimeStamp2SystemTime(
            String => $Param{Ticket}->{Created},
        );

        my $FirstResponseTime = $TimeObject->TimeStamp2SystemTime(
            String => $Data{FirstResponse},
        );

        # get time between creation and first response
        my $WorkingTime = $TimeObject->WorkingTime(
            StartTime => $CreateTime,
            StopTime  => $FirstResponseTime,
            Calendar  => $Escalation{Calendar},
        );



        $Data{FirstResponseInMin} = int( $WorkingTime / 60 );
        my $EscalationFirstResponseTime = $Escalation{FirstResponseTime} * 60;
        $Data{FirstResponseDiffInMin} = int( ( $EscalationFirstResponseTime - $WorkingTime ) / 60 );
    }

    return %Data;
}

sub Kernel::System::Ticket::_TicketGetClosed {
    my ( $Self, %Param ) = @_;

    # check needed stuff
    for my $Needed (qw(TicketID Ticket)) {
        if ( !defined $Param{$Needed} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "Need $Needed!"
            );
            return;
        }
    }

    # get close state types
    my @List = $Kernel::OM->Get('Kernel::System::State')->StateGetStatesByType(
        StateType => ['closed'],
        Result    => 'ID',
    );
    return if !@List;

    # Get id for history types
    my @HistoryTypeIDs;
    for my $HistoryType (qw(StateUpdate NewTicket)) {
        push @HistoryTypeIDs, $Self->HistoryTypeLookup( Type => $HistoryType );
    }

    # get database object
    my $DBObject = $Kernel::OM->Get('Kernel::System::DB');

    return if !$DBObject->Prepare(
        SQL => "
            SELECT MAX(create_time)
            FROM ticket_history
            WHERE ticket_id = ?
               AND state_id IN (${\(join ', ', sort @List)})
               AND history_type_id IN  (${\(join ', ', sort @HistoryTypeIDs)})
            ",
        Bind => [ \$Param{TicketID} ],
    );

    my %Data;
    ROW:
    while ( my @Row = $DBObject->FetchrowArray() ) {
        last ROW if !defined $Row[0];
        $Data{Closed} = $Row[0];

        # cleanup time stamps (some databases are using e. g. 2008-02-25 22:03:00.000000
        # and 0000-00-00 00:00:00 time stamps)
        $Data{Closed} =~ s/^(\d\d\d\d-\d\d-\d\d\s\d\d:\d\d:\d\d)\..+?$/$1/;
    }

    return if !$Data{Closed};

    # for compat. wording reasons
    $Data{SolutionTime} = $Data{Closed};

    # get escalation properties
    my %Escalation = $Self->TicketEscalationPreferences(
        Ticket => $Param{Ticket},
        UserID => $Param{UserID} || 1,
    );

    ## NEUM032 Changed part starts here
    $Escalation{SolutionTime} += int ( $Self->SecondsInSuspensionState(
      TicketID => $Param{TicketID},
      Calendar => $Escalation{Calendar},
    ) /60 );
    ## NEUM032 Changed part ends here

    # get time object
    my $TimeObject = $Kernel::OM->Get('Kernel::System::Time');

    # get unix time stamps
    my $CreateTime = $TimeObject->TimeStamp2SystemTime(
        String => $Param{Ticket}->{Created},
    );
    my $SolutionTime = $TimeObject->TimeStamp2SystemTime(
        String => $Data{Closed},
    );

    # get time between creation and solution
    my $WorkingTime = $TimeObject->WorkingTime(
        StartTime => $CreateTime,
        StopTime  => $SolutionTime,
        Calendar  => $Escalation{Calendar},
    );

    $Data{SolutionInMin} = int( $WorkingTime / 60 );

    if ( $Escalation{SolutionTime} ) {
        my $EscalationSolutionTime = $Escalation{SolutionTime} * 60;
        $Data{SolutionDiffInMin} = int( ( $EscalationSolutionTime - $WorkingTime ) / 60 );
    }

    return %Data;
}

# reset all warnings
}

sub SecondsInSuspensionState {
  my ( $Self, %Param ) = @_;

  for my $Needed (qw(TicketID Calendar)) {
      if ( !defined $Param{$Needed} ) {
          $Kernel::OM->Get('Kernel::System::Log')->Log(
              Priority => 'error',
              Message  => "Need $Needed!"
          );
          return;
      }
  }

  my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
  my $TimeObject = $Kernel::OM->Get('Kernel::System::Time');
  my $ConfigObject = $Kernel::OM->Get('Kernel::Config');

  $DBObject->Prepare(
    SQL => 'SELECT ticket_state.name, ticket_history.create_time
      FROM ticket_state, ticket_history
      WHERE ticket_history.ticket_id = ?
      AND ticket_history.state_id = ticket_state.id
      AND ticket_history.history_type_id in (1, 27)
      ORDER BY ticket_history.create_time',
    Bind => [ \$Param{TicketID} ],
  );

  my $SecondsInSuspensionState = 0;
  my $PrevState;
  my $PrevTime;

  # Loop through array and compare previous time and date to current one
  while (my @Row = $DBObject->FetchrowArray()) {
    if (defined $PrevState && defined $PrevTime) {
      if ( $ConfigObject->Get('SLAHandling::EscalationSuspensionStates')->{$PrevState} ) {

        $SecondsInSuspensionState += $TimeObject->WorkingTime(
            StartTime => $TimeObject->TimeStamp2SystemTime( String => $PrevTime ),
            StopTime  => $TimeObject->TimeStamp2SystemTime( String => $Row[1] ),
            Calendar  => $Param{Calendar}
        );

      }
    }
    $PrevState =  $Row[0];
    $PrevTime  =  $Row[1];
  }

  if (defined $PrevState && defined $PrevTime) {
    if ( $ConfigObject->Get('SLAHandling::EscalationSuspensionStates')->{$PrevState} ) {

      $SecondsInSuspensionState += $TimeObject->WorkingTime(
          StartTime => $TimeObject->TimeStamp2SystemTime( String => $PrevTime ),
          StopTime  => $TimeObject->SystemTime(),
          Calendar  => $Param{Calendar}
      );

    }
  }

  return $SecondsInSuspensionState;
}

1;
