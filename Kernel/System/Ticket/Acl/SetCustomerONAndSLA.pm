# --
# Kernel/System/Ticket/Acl/CloseParentAfterClosedChilds.pm - acl module
# - allow no parent close till all clients are closed -
# Copyright (C) 2001-2015 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::System::Ticket::Acl::SetCustomerONAndSLA;

use strict;
use warnings;

our @ObjectDependencies = (
    'Kernel::System::Log',
    'Kernel::System::Ticket',
    'Kernel::System::SLA',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;

    # check needed stuff
    for (qw(Config Acl)) {
        if ( !$Param{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "(SetCustomerONAndSLA) Need $_!"
            );
            return;
        }
    }

    # my $tmp = "Ticket ".$Param{TicketID}."Prio ".$Param{PriorityID}."Service ".$Param{ServiceID}."Customer ".$Param{CustomerID};#Dumper(%Param);

    # $Kernel::OM->Get('Kernel::System::Log')->Log(
    #    Priority => 'info',
    #    Message  => "(SetCustomerONAndSLA) $tmp",
    # );

    # return 1 if !$Param{CustomerID};
    # return 1 if !$Param{ServiceID};
    # return 1 if !$Param{PriorityID};

    if ((!$Param{CustomerID}
	|| !$Param{ServiceID}
	|| !$Param{PriorityID}
	)
	&& defined $Param{TicketID}) {

	my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
		TicketID => $Param{TicketID},
        	UserID => 1,
	);
	$Param{CustomerID} = $Param{CustomerID} || $Ticket{CustomerID};
	$Param{PriorityID} = $Param{PriorityID} || $Ticket{PriorityID};
	$Param{ServiceID} = $Param{ServiceID} || $Ticket{ServiceID};
    } elsif ((!$Param{CustomerID}
        || !$Param{ServiceID}
        || !$Param{PriorityID}
        )
        && !$Param{TicketID}){

	return 1;
    }

    if (!$Param{CustomerID}
#        || !$Param{ServiceID}
#        || !$Param{PriorityID}
       )
    {

        return 1;
    }


    #tmp
    # return 1;

    # get SLA 
    my %SLAList = $Kernel::OM->Get('Kernel::System::SLA')->SLAList(
        ServiceID => $Param{ServiceID},
        Valid     => 0,
        UserID    => 1,
    );

    my $Priority = $Kernel::OM->Get('Kernel::System::Priority')->PriorityLookup(
        PriorityID => $Param{PriorityID},
    );

#    my $tmp = "$Priority";

#    $Kernel::OM->Get('Kernel::System::Log')->Log(
#	Priority => 'info',
#    	Message  => "(SetCustomerONAndSLA) $tmp",
#    );

	
    my @SLAArray;

    foreach my $element (keys %SLAList) {
	
	if  ( $SLAList{$element} =~ /$Param{CustomerID}/ && $SLAList{$element} =~ /$Priority/ ) {
       
		my $tmp = "$SLAList{$element}";

        	# $Kernel::OM->Get('Kernel::System::Log')->Log(
                #	Priority => 'info',
                #	Message  => "(SetCustomerONAndSLA) $tmp",
        	# );
		
		push @SLAArray, $SLAList{$element};
		# $SLAArray .= "$SLAList{$element},";
	}
    }

    if ( !@SLAArray ) {
    #Array leer!

        #my $tmp = "Array leer!";

        #$Kernel::OM->Get('Kernel::System::Log')->Log(
        #       Priority => 'info',
        #       Message  => "(SetCustomerONAndSLA) $tmp",
        #);

    	 $Param{Acl}->{setCustomerONAndSLA} = {

            Properties => {

                Ticket => {
                        CustomerID => [ $Param{CustomerID} ],
                },
            },
            
	    PossibleAdd => {

                Ticket => {
                        DynamicField_Ordernumber => [ "[RegExp]$Param{CustomerID}" ],
                        SLA => [ "[RegExp]$Param{CustomerID}" ],
                },
            },
         };

    }  else {
	        
    	#my $tmp = "Array gefüllt!";
	#use Data::Dumper;
	#my $tmp1;
	#$tmp1 = Dumper(@SLAArray);
	#$tmp1 =~ s/\n//g;

        #$Kernel::OM->Get('Kernel::System::Log')->Log(
        #       Priority => 'info',
        #       Message  => "(SetCustomerONAndSLA) $tmp, Dump: $tmp1",
        #);

         $Param{Acl}->{setCustomerONAndSLA} = {

            Properties => {

                Ticket => {
                       CustomerID => [ $Param{CustomerID} ],
                       # Priority => [ $Priority ],
                       # ServiceID => [ $Param{ServiceID} ],
                },
            },

            PossibleAdd => {

                Ticket => {
                        DynamicField_Ordernumber => [ "[RegExp]$Param{CustomerID}" ],
                        SLA => \@SLAArray,
                },
            },

	};

     }
}
1;
	
