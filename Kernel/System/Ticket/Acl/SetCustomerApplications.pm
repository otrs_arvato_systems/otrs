package Kernel::System::Ticket::Acl::SetCustomerApplications;

use strict;
use warnings;

our @ObjectDependencies = (
    'Kernel::System::Log',
    'Kernel::System::Ticket',
    'Kernel::System::CustomerUser'
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;

    # check needed stuff
    for (qw(Config Acl)) {
        if ( !$Param{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "(SetCustomerApplications) Need $_!"
            );
            return;
        }
    }

    if (!$Param{CustomerID} && $Param{CustomerUserID}) { # In the Customer Portal the only indication to the CustomerID is the CustomerUserID

        my @CustomerIDs = $Kernel::OM->Get('Kernel::System::CustomerUser')->CustomerIDs(
            User => $Param{CustomerUserID},
        );

        $Param{CustomerID} = $CustomerIDs[0] if $CustomerIDs[0];

    } elsif (!$Param{CustomerID} && defined $Param{TicketID}) {

	my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
		TicketID => $Param{TicketID},
        	UserID => 1,
	);
	$Param{CustomerID} = $Param{CustomerID} || $Ticket{CustomerID};
    } elsif (!$Param{CustomerID} && !$Param{TicketID}){
	return 1;
    }

    if(!$Param{CustomerID}) {
	return 1;
    }

    $Param{Acl}->{setCustomerApplications} = {

        Properties => {

     #       CustomerUser => {
     #           UserCustomerID => [ $Param{CustomerID} ],
     #       },
        },

        Possible => {

            Ticket => {
                DynamicField_Application => [ "[RegExp]$Param{CustomerID}" ],
            },
        },
     };
     
     return 1;
}
1;
	
