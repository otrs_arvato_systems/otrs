package Kernel::System::Ticket::Event::Z_RemedyInterface;

use strict;
use warnings;

use Data::Dumper;

our @ObjectDependencies = (
    'Kernel::Config',
    'Kernel::System::Log',
    'Kernel::System::Ticket',
    'Kernel::System::DynamicField',
    'Kernel::System::DynamicField::Backend',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;

    # check needed stuff
    for (qw(Data Event Config UserID)) {
        if ( !$Param{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[RemedyInterface] Need $_!"
            );
            return;
        }
    }
    for (qw(TicketID)) {
        if ( !$Param{Data}->{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[RemedyInterface] Need $_ in Data!"
            );
            return;
        }
    }

    # get ticket object
    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
	
    # get current ticket data
    my %Ticket = $TicketObject->TicketGet(
        TicketID => $Param{Data}->{TicketID},
        UserID => $Param{UserID},
        DynamicFields => 1,
    );

    my $CustomerConfig = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

    return 1 if !$Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::ActiveCustomers")->{$Ticket{CustomerID}};

    return 1 if $Param{UserID} eq $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General")->{WebserviceUserID};

    if ($Param{Event} eq 'ArticleCreate' && $Param{Data}->{ArticleID}) {

        my %Article = $TicketObject->ArticleGet(
            ArticleID => $Param{Data}->{ArticleID},
            UserID => $Param{UserID},
        );

        return 1 if ($Article{SenderType} && $Article{SenderType} eq 'system');

    }
	
    my $DynamicFieldConfig = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
        ID   => $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General")->{DynamicField_RemedyTicketNumber_ID},
    );	
	
    my $RemedyTicketNumber;
 
    $RemedyTicketNumber = $Ticket{"DynamicField_"."$DynamicFieldConfig->{Name}"} if $Ticket{"DynamicField_"."$DynamicFieldConfig->{Name}"};
	
    if (!$Ticket{"DynamicField_"."$DynamicFieldConfig->{Name}"} && $Ticket{Title} =~ /(INC\d\d\d\d\d\d\d\d+)/) {
        $RemedyTicketNumber = $1;
    } elsif (!$Ticket{"DynamicField_"."$DynamicFieldConfig->{Name}"} && $Ticket{Title} =~ /(IM\d\d\d\d\d\d\d\d\d+)/) {
        $RemedyTicketNumber = $1;
    }
	
    # get TaskHandlerObject
    # my $SchedulerObject = $Kernel::OM->Get('Kernel::System::Scheduler');

    # my $tmp = Dumper(%Param);
    # $tmp =~ s/\n/\| /g;

    my $InRemedyQueue = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General")->{RemedyQueue} =~ /(^$Ticket{Queue}$)|(^$Ticket{Queue}\;)|(\;$Ticket{Queue}$)|(\;$Ticket{Queue}\;)/;

    my $NotMonitoring = $Ticket{Queue} ne 'Monitoring';

    my $LogReTnr = $RemedyTicketNumber || 'Not defined';

    $Kernel::OM->Get('Kernel::System::Log')->Log(
        Priority => 'info',
        Message  => "[RemedyInterface] Event: $Param{Event}, called for ticket $Ticket{TicketID}. RemedyTicketNumber: $LogReTnr"
    );

     if ( !$RemedyTicketNumber && $Param{Event} ne "ArticleCreate" && $InRemedyQueue && $NotMonitoring )
     {

		# my $TaskID = $SchedulerObject->TaskRegister(
		#	Type => 'Webservice',
	        #	Data => {
		#		File => "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_TicketCreate.pl",
		#		TicketID => $Ticket{TicketID},
		#		TicketNumber => $Ticket{TicketNumber}
		#	},
		# );

        my $Create = "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_TicketCreate.pl";
        local @ARGV = ("$Ticket{TicketID}");
        do $Create; # or $Kernel::OM->Get('Kernel::System::Log')->Log(Priority => 'error', Message  => "[RemedyInterface] can't execute $Create for TicketID $Ticket{TicketID}");

#    } elsif ($RemedyTicketNumber && $Param{Event} ne "TicketCreate" ) {
     } elsif ( $Param{Event} ne "TicketCreate" ) {

	if(!$RemedyTicketNumber && $InRemedyQueue && $NotMonitoring) 
	{
		#$Kernel::OM->Get('Kernel::System::Log')->Log(
                #	Priority => 'error',
        	#	Message  => "[RemedyInterface] TESTING Remedy Ticket Create during ArticleCreate event, you have reached TicketCreate action"
	        #);

	        my $Create = "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_TicketCreate.pl";
	        local @ARGV = ("$Ticket{TicketID}");
	        do $Create; # or $Kernel::OM->Get('Kernel::System::Log')->Log(Priority => 'error', Message  => "[RemedyInterface] can't execute $Create for TicketID $Ticket{TicketID}");
		
		# get new ticket data
		my %NewTicket = $TicketObject->TicketGet(
			TicketID => $Param{Data}->{TicketID},
			UserID => $Param{UserID},
			DynamicFields => 1,
		);


		$RemedyTicketNumber = $NewTicket{"DynamicField_"."$DynamicFieldConfig->{Name}"} if $NewTicket{"DynamicField_"."$DynamicFieldConfig->{Name}"};

   		if (!$NewTicket{"DynamicField_"."$DynamicFieldConfig->{Name}"} && $NewTicket{Title} =~ /(INC\d\d\d\d\d\d\d\d+)/) {
        		$RemedyTicketNumber = $1;
		} elsif (!$NewTicket{"DynamicField_"."$DynamicFieldConfig->{Name}"} && $NewTicket{Title} =~ /(IM\d\d\d\d\d\d\d\d\d+)/) {
                        $RemedyTicketNumber = $1;
                }
		
		#$Kernel::OM->Get('Kernel::System::Log')->Log(
                #        Priority => 'error',
                #        Message  => "[RemedyInterface] TESTING Remedy Ticket Created, proceed to ArticleCreate"
                #);


	}

       if($RemedyTicketNumber)
       {

        if (!$Ticket{"DynamicField_"."$DynamicFieldConfig->{Name}"} && $RemedyTicketNumber) {	
            my $Success2 = $Kernel::OM->Get('Kernel::System::DynamicField::Backend')->ValueSet(
                DynamicFieldConfig => $DynamicFieldConfig,
                ObjectID => $Param{Data}->{TicketID},
                Value => $RemedyTicketNumber,
                UserID => 1,
            );
		
        }

        if ($Ticket{"DynamicField_"."$DynamicFieldConfig->{Name}"} || $RemedyTicketNumber) {
			# my $TaskID = $SchedulerObject->TaskRegister(
			#	Type => 'Webservice',
			#	Data => {
			#		File => "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_UpdateRemedyTicket.pl",
			#		TicketID => $Ticket{TicketID},
			#		TicketNumber => $Ticket{TicketNumber}
			#	},
			# );
            # There will be an ArticleCreate Event for every solved ticket, so calling the update twice is unnecessary
	    return 1 if ($Ticket{State} eq 'solved' && $Param{Event} eq 'TicketStateUpdate');

	    #$Kernel::OM->Get('Kernel::System::Log')->Log(
            #    Priority => 'error',
            #    Message  => "[RemedyInterface - INFO]  Event: $Param{Event}, UpdateRemedyTicket called for ticketID $Ticket{TicketID} with state $Ticket{State}."
            #);

            my $Update = "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_UpdateRemedyTicket.pl";
            local @ARGV = ("$Ticket{TicketID}", "$Param{Event}");
            do $Update; # or $Kernel::OM->Get('Kernel::System::Log')->Log(Priority => 'error', Message  => "[RemedyInterface] can't execute $Update for TicketID $Ticket{TicketID}");
        } else {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[RemedyInterface]  Event: $Param{Event}, called for ticket $Ticket{TicketID}. This should not happen"
            );
        }
      }		
    }
	
    return 1;
}

1;
