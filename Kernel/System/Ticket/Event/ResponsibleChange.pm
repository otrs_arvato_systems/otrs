# --
# Kernel/System/Ticket/Event/ResponsibleChange.pm - a event module for auto set of responible
# Copyright (C) 2001-2015 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::System::Ticket::Event::ResponsibleChange;

use strict;
use warnings;
use Kernel::System::ObjectManager;

our @ObjectDependencies = (
    'Kernel::Config',
    'Kernel::System::Log',
    'Kernel::System::Ticket',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;

    # check needed stuff
    for (qw(Data Event Config UserID)) {
        if ( !$Param{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "Need $_!"
            );
            return;
        }
    }
    for (qw(TicketID)) {
        if ( !$Param{Data}->{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "Need $_ in Data!"
            );
            return;
        }
    }

    # get config object
    my $ConfigObject = $Kernel::OM->Get('Kernel::Config');

    # get ticket object
    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
	
	#get db object
	my $DBObject = $Kernel::OM->Get('Kernel::System::DB');

    # get current ticket data
    my %Ticket = $TicketObject->TicketGet(
        TicketID      => $Param{Data}->{TicketID},
        UserID        => $Param{UserID},
        DynamicFields => 0,
    );
		$DBObject->Prepare(
        SQL    => "SELECT id_agent FROM responsible_queue WHERE id_queue = ? ",
        Bind   => [ \$Ticket{QueueID}],
    );
	
	my $NewResponsible;
	while (my @Row = $DBObject->FetchrowArray()) {
        $NewResponsible=$Row[0];
    }
	if($NewResponsible){
		$TicketObject->TicketResponsibleSet(
        TicketID  => $Param{Data}->{TicketID},
        NewUserID => $NewResponsible,
        UserID    => $Param{UserID},
		);
	}else
	{
		$TicketObject->TicketResponsibleSet(
        TicketID  => $Param{Data}->{TicketID},
        NewUserID => $Ticket{OwnerID},
        UserID    => $Param{UserID},
		);
	}

    return 1;
}

1;
