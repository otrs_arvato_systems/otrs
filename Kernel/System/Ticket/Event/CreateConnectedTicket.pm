# --
# Kernel/System/Ticket/Event/ResponsibleAutoSet.pm - a event module for auto set of responible
# Copyright (C) 2001-2014 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::System::Ticket::Event::CreateConnectedTicket;

use strict;
use warnings;

our @ObjectDependencies = (
    'Kernel::Config',
    'Kernel::System::Log',
    'Kernel::System::CustomerUser',
    'Kernel::System::Ticket',
    'Kernel::System::Time',
    'Kernel::System::DynamicField',
    'Kernel::System::DynamicField::Backend ',
    'Kernel::System::LinkObject',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;
    # check needed stuff
    for (qw(Data Event Config UserID)) {
        if ( !$Param{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[CreateConnectedTicket] Need $_!"
            );
            return;
        }
    }
    for (qw(TicketID)) {
        if ( !$Param{Data}->{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[CreateConnectedTicket] Need $_ in Data!"
            );
            return;
        }
    }

    # get ticket object
    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

    # get current ticket data
    my %Ticket = $TicketObject->TicketGet(
        TicketID      => $Param{Data}->{TicketID},
        UserID        => 1,
        DynamicFields => 1,
    );
	if (defined $Ticket{DynamicField_TypeConnectedTicket}) {
		#get customer object
		my $CustomerUserObject = $Kernel::OM->Get('Kernel::System::CustomerUser');
		
		# get customer user data
		my %User = $CustomerUserObject->CustomerUserDataGet(
			User => $Ticket{CustomerUser},
		);

		my $TicketID;
		
		if ($Ticket{DynamicField_TypeConnectedTicket} =~ /Change/ ) {
		   if ($Ticket{DynamicField_TypeConnectedTicket} eq 'normal Change' ) {	
		   	
				# create new ticket
				$TicketID = $TicketObject->TicketCreate(
					#%Ticket,
					Title				=> $Ticket{Title},
					Queue			=> $Ticket{Queue},
					Lock				=> 'unlock',
					Priority			=> $Ticket{Priority},
					State				=> 'requested',
					Type				=> 'Change',
					Service			=> $Ticket{Service},
					SLA				=> $Ticket{SLA},
					CustomerID		=> $Ticket{CustomerID},
					CustomerUser	=> $User{UserEmail},                                    # customer email is needed instead of customer login!!
					OwnerID			=> $Param{UserID},
					ResponsibleID  => $Ticket{ResponsibleID},
					UserID                  => 1,
			   );

		   } else {
				# create new ticket
				$TicketID = $TicketObject->TicketCreate(
					#%Ticket,
					Title				=> $Ticket{Title},
					Queue			=> $Ticket{Queue},
					Lock				=> 'unlock',
					Priority			=> $Ticket{Priority},
					State				=> 'build',
					Type				=> 'Change',
					Service			=> $Ticket{Service},
					SLA				=> $Ticket{SLA},
					CustomerID		=> $Ticket{CustomerID},
					CustomerUser	=> $User{UserEmail},                                    # customer email is needed instead of customer login!!
					OwnerID			=> $Param{UserID},
					ResponsibleID  => $Ticket{ResponsibleID},
					UserID                  => 1,
			   );

		   }

		} else {
		   # create new ticket
			$TicketID = $TicketObject->TicketCreate(
				#%Ticket,
				Title				=> $Ticket{Title},
				Queue			=> $Ticket{Queue},
				Lock				=> 'unlock',
				Priority			=> $Ticket{Priority},
				State				=> 'in progress',
				Type				=> $Ticket{DynamicField_TypeConnectedTicket},
				Service			=> $Ticket{Service},
				SLA				=> $Ticket{SLA},
				CustomerID		=> $Ticket{CustomerID},
				CustomerUser	=> $User{UserEmail},                                    # customer email is needed instead of customer login!!
				OwnerID			=> $Param{UserID},
				ResponsibleID  => $Ticket{ResponsibleID},
				UserID                  => 1,
		   );
				   
		}

		 
		 if (defined $TicketID) { 
			 # get config object
			my $ConfigObject = $Kernel::OM->Get('Kernel::Config');
		 
			 # get DynamicField Object
			 my $DynamicFieldObject = $Kernel::OM->Get('Kernel::System::DynamicField');
			 
			 # get Config of DynamicField ProcessManagementProcessID
			 my $ConfigProcess = $DynamicFieldObject->DynamicFieldGet(
				ID   => 1,
			 );
			 
			 # get Config of DynamicField ProcessManagementActivityID
			 my $ConfigActivity = $DynamicFieldObject->DynamicFieldGet(
				ID   => 2,
			 );

			 # get Config of DynamicField TypeConnectedTicket
			 my $ConfigConnectedTicketType = $DynamicFieldObject->DynamicFieldGet(
					ID   => 11,
			 );
			 
			 # get Backend Object
			 my $BackendObject = $Kernel::OM->Get('Kernel::System::DynamicField::Backend');
			 
			 # Set Process
			 my $Success = $BackendObject->ValueSet(
				DynamicFieldConfig	=> $ConfigProcess,      
				ObjectID           		=> $TicketID,                                           
				Value              		=> $ConfigObject->Get('CreateConnectedTicket::Type-Process Mapping')->{$Ticket{DynamicField_TypeConnectedTicket}},
				UserID             		=> 1,
			);
			
			# Set Activity
			$Success = $BackendObject->ValueSet(
				DynamicFieldConfig	=> $ConfigActivity,      
				ObjectID           		=> $TicketID,                                           
				Value              		=> $ConfigObject->Get('CreateConnectedTicket::Process-Activity Mapping')->{$Ticket{DynamicField_TypeConnectedTicket}},
				UserID             		=> 1,
			);
			
			
			if ( $Ticket{DynamicField_TypeConnectedTicket} =~ /Change/  ) {
				 # get Config of DynamicField ChangeType
				 my $ConfigChangeType= $DynamicFieldObject->DynamicFieldGet(
						ID   => 33,
				 );
			
				$Ticket{DynamicField_TypeConnectedTicket} =~ s/ Change//;
				# Set ChangeType
				 my $Success = $BackendObject->ValueSet(
					DynamicFieldConfig	=> $ConfigChangeType,      
					ObjectID           		=> $TicketID,                                           
					Value              		=> $Ticket{DynamicField_TypeConnectedTicket},
					UserID             		=> 1,
				);
				
			}

			$Success = $BackendObject->ValueDelete(
        			DynamicFieldConfig	=> $ConfigConnectedTicketType, 
        			ObjectID				=> $Ticket{TicketID},       
        			UserID					=> 1,
    			);
			 
			 # get link object
			 my $LinkObject = $Kernel::OM->Get('Kernel::System::LinkObject');
			 
			 # link old and new ticket
			 my $True = $LinkObject->LinkAdd(
				SourceObject	=> 'Ticket',
				SourceKey		=> $Ticket{TicketID},
				TargetObject	=> 'Ticket',
				TargetKey		=> $TicketID,
				Type				=> 'Normal',
				State				=> 'Valid',
				UserID			=> 1,
			);
			
			# Check article option
			#if (defined $Ticket{ArticleForConnectedTicket}) {
			
				foreach my $Option (@{$Ticket{DynamicField_ArticleForConnectedTicket}}) {
					my %Article;
					
					if ($Option eq "first article") {
						# get first article
						%Article = $TicketObject->ArticleFirstArticle(
							TicketID        => $Ticket{TicketID},
							DynamicFields => 0,
						);					
						
					}				
					 
					elsif ($Option eq "latest article") {						
						
						# get latest article
						my @ArticleIDs = $TicketObject->ArticleIndex(
							TicketID => $Ticket{TicketID},
							UserID	=> 1,
						);
						
						%Article = $TicketObject->ArticleGet(
							ArticleID	=> $ArticleIDs[$#ArticleIDs],
							#TicketID	=> $Ticket{TicketID},
							UserID	=> 1,
						);
					
					}
					
					else {
						# Log New Option that is not yet recoginzed by module CreateConnectedTicket
						$Kernel::OM->Get('Kernel::System::Log')->Log(
							Priority => 'error',
							Message  => "The module CreateConnectedTicket cannot handle the Option $Option"
						);
					}
					
					if (defined $Article{'ArticleID'}) {
						# get Attachments of first article
						my %Index = $TicketObject->ArticleAttachmentIndex(
							ArticleID => $Article{'ArticleID'},
							UserID    => 1,
						);

						my @Attachments = ();

						while ( (my $key, my $value) = each %Index )
						{
								my %Attachment = $TicketObject->ArticleAttachment(
										ArticleID => $Article{'ArticleID'},
										FileID    => $key,   # as returned by ArticleAttachmentIndex
										UserID    => 1,
								);

								push ( @Attachments, {%Attachment} );
						}						
						
						# add article + attachments to new ticket
						my $ArticleID = $TicketObject->ArticleCreate(
							%Article,
							TicketID         	=> $TicketID,
							HistoryType		=> 'AddNote',                          # EmailCustomer|Move|AddNote|PriorityUpdate|WebRequestCustomer|...
							HistoryComment   => 'Added Article via CreateConnectedTicket Module!',
							UserID           	=> 1,
							Attachment 		=> \@Attachments,
						);
					
					}
					
				}
				
			#}
		
		} else {
			$Kernel::OM->Get('Kernel::System::Log')->Log(
					Priority => 'error',
					Message  => 'No Ticket created! Error in CreateConnectedTicket.pm',
				);
			return;
		}
	}

    return 1;
}

1;
