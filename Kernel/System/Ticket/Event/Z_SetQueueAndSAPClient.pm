package Kernel::System::Ticket::Event::Z_SetQueueAndSAPClient;

use strict;
use warnings;

our @ObjectDependencies = (
    'Kernel::Config',
    'Kernel::System::Log',
    'Kernel::System::Ticket',
    'Kernel::System::DynamicField',
    'Kernel::System::DynamicField::Backend',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;
    # check needed stuff
    for (qw(Data Event Config)) {
        if ( !$Param{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[SetQueueAndSAPClient] Need $_!"
            );
            return;
        }
    }
    for (qw(TicketID)) {
        if ( !$Param{Data}->{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[SetQueueAndSAPClient] Need $_ in Data!"
            );
            return;
        }
    }    
    # get ticket object
    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

    my %Ticket = $TicketObject->TicketGet(
        TicketID => $Param{Data}->{TicketID},
        DynamicFields => 1,
	UserID => 1
    );

    my $Client;
    return 1 if $Ticket{DynamicField_Client};
    if ( $Ticket{Title} =~ /(\s|[A-Za-z][A-Za-z]6_|\(|^)(\d\d\d)(\,|\.|\;|\:|\/|\\|\_|\-|\)|$|\s)/  ) {
        $Client = $2;
    }
    
    return 1 if !$Client;

    # get dynamic field config
    my $DynamicFieldConfig = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
        ID => 9,
    );

    if (defined $DynamicFieldConfig) {
        # set the value
        my $Success = $Kernel::OM->Get('Kernel::System::DynamicField::Backend')->ValueSet(
            DynamicFieldConfig => $DynamicFieldConfig,
           ObjectID => $Param{Data}->{TicketID},
            Value => $Client,
            UserID => $Param{UserID},
        );
    }

    if ( $Ticket{Title} =~ /PYP Monitoring/ ) {
        return 1;
    }
    return 1 if (!($Ticket{CustomerID} eq 'Pharma' 
        || $Ticket{CustomerID} eq 'Auto/CIM' 
        || $Ticket{CustomerID} eq 'Telco' 
        || $Ticket{CustomerID} eq 'arvato_D10'
        || $Ticket{CustomerID} eq 'arvato_Rewards' 
 	|| $Ticket{CustomerID} eq 'arvato_Games'
 	|| $Ticket{CustomerID} eq 'JP6 - 260 - Olympus'
        || $Ticket{CustomerID} eq 'EP6 - 350 - LOreal'
    ));


    if ( !$Kernel::OM->Get('Kernel::Config')->Get('SAPClientMapping')->{$Client} ) {
        $Kernel::OM->Get('Kernel::System::Log')->Log(
            Priority => 'info',
            Message => "Für die Nummer: $Client wurde im Mapping keine Queue gefunden."
        );
        return 1
    }
    # set the value
    my $Success = $Kernel::OM->Get('Kernel::System::Ticket')->TicketQueueSet(
        Queue => $Kernel::OM->Get('Kernel::Config')->Get('SAPClientMapping')->{$Client},
        TicketID => $Param{Data}->{TicketID},
        UserID => $Param{UserID},
    );

    if ( !$Success ) {
        $Kernel::OM->Get('Kernel::System::Log')->Log(
            Priority => 'info',
            Message => "Queue could not be set via Event-Module SetQueueAndSAPClient.pm"
        );
    }

    return 1;
}

1;
