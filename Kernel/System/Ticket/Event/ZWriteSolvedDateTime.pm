package Kernel::System::Ticket::Event::ZWriteSolvedDateTime;

use strict;
use warnings;

our @ObjectDependencies = (
   'Kernel::Config',
   'Kernel::System::DB',
   'Kernel::System::Log',
   'Kernel::System::Time',
   'Kernel::System::Ticket',
   'Kernel::System::DynamicField',
   'Kernel::System::DynamicField::Backend',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;

    # check needed stuff
    for (qw(Data Event Config UserID)) {
        if ( !$Param{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[WriteSolvedDateTime..] Need $_!"
            );
            return;
        }
    }

    for (qw(TicketID)) {
        if ( !$Param{Data}->{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "Need $_ in Data!"
            );
            return;
        }
    }

    # get objects
    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
    my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
    my $DynamicFieldObject = $Kernel::OM->Get('Kernel::System::DynamicField');
    my $BackendObject = $Kernel::OM->Get('Kernel::System::DynamicField::Backend');

    # get current ticket data
    my %Ticket = $TicketObject->TicketGet(
        TicketID      => $Param{Data}->{TicketID},
        UserID        => $Param{UserID},
        DynamicFields => 1,
    );

    if ( !(($Ticket{State} eq 'solved') || ($Ticket{State} eq 'rejected'))) {
        return 1;
    }

    $DBObject->Prepare(
        SQL => 'SELECT create_time FROM ticket_history WHERE id = ( SELECT MAX(id) FROM ticket_history WHERE history_type_id = 27 AND state_id IN (3,18) AND ticket_id = ? )',
        Bind => [ \$Ticket{TicketID} ],
    );

    my $SolvedDateTime;
    while (my @Row = $DBObject->FetchrowArray()) {
        $SolvedDateTime = $Row[0];
    }
    return 1 if !$SolvedDateTime;

    #DynamicField_SolvedDateTime
    my $DynamicFieldConfig = $DynamicFieldObject->DynamicFieldGet(
        ID   => 58,
    );
 
    my $Success = $BackendObject->ValueSet(
        DynamicFieldConfig => $DynamicFieldConfig,
        ObjectID           => $Param{Data}->{TicketID},
        Value              => $SolvedDateTime,
        UserID             => 1,
    );

    return 1;
}

1;
