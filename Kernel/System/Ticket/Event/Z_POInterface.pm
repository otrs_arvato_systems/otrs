# This module listens for certain events configured at Kernel/Config/Files/Z_POInterface.xml
package Kernel::System::Ticket::Event::Z_POInterface;

use strict;
use warnings;
use Data::Dumper;

our @ObjectDependencies = (
  'Kernel::Config',
  'Kernel::System::Log',
  'Kernel::System::Ticket',
  'Kernel::System::DynamicField',
  'Kernel::System::DynamicField::Backend',
);

sub new {
  my ($Class, %Param) = @_;

  my $Self = {
  };
  bless $Self, $Class;
  return $Self;
}

sub Run {
  my ($Self, %Param) = @_;

  my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
  my $LogObject = $Kernel::OM->Get('Kernel::System::Log');
  my $ConfigObject = $Kernel::OM->Get('Kernel::Config');

  my $log_pref = "[POInterface] (Event = $Param{Event}, TicketID = $Param{Data}->{TicketID})";

  $LogObject->Log(Priority => 'info',
                  Message  => "$log_pref - Run() is called");

  my $check_result = $Self->precheck_settings(%Param);
  # $LogObject->Log(Priority => 'info', Message => Dumper($check_result));exit;

  return 1 if ($Param{UserID} eq '117'); # Ticket syncin should not propagate to HPSM

  if ($check_result != 0) {
    return $check_result;
  }
  # $LogObject->Log(Priority => 'info', Message => 'Until here');exit;

  # Get current ticket data
  my %Ticket = $TicketObject->TicketGet(
    TicketID => $Param{Data}->{TicketID},
    UserID => $Param{UserID},
    DynamicFields => 1,
  );

  # Return if the customer is not registered for this propagation service.
  if (!$ConfigObject->Get("Webservice::PO::ActiveCustomers")) {
    $LogObject->Log(Priority => 'error',
                    Message  => "$log_pref - Cannot load Webservice::PO::ActiveCustomers settings. Is it deactivated?");
    return 1;
  }
  if (!$ConfigObject->Get("Webservice::PO::ActiveCustomers")->{$Ticket{CustomerID}. '::Active'}) {
    $LogObject->Log(Priority => 'error',
                    Message  => "$log_pref - Unconfigured customer ID: $Ticket{CustomerID}");
    return 1;
  }

#Shaun - Skip if this ticket is not of type Incident
#Need to be removed when we support other types (e.g. Problem/Change/Request/etc)
  if ($Ticket{Type} ne 'Incident') {
    $LogObject->Log(Priority => 'info',
                    Message  => "$log_pref - Rejected: Not an Incident ticket as Type = $Ticket{Type}");
    return 1;
  }

  if ($Param{Event} eq 'ArticleCreate' && $Param{Data}->{ArticleID}) {
    my %Article = $TicketObject->ArticleGet(
      ArticleID => $Param{Data}->{ArticleID},
      UserID => $Param{UserID},
    );

    # Stop if the sender type is system
    if ($Article{SenderType} && $Article{SenderType} eq 'system') {
      $LogObject->Log(Priority => 'info',
                      Message  => "$log_pref - Rejected: SenderType = system");
      return 1;
    }
  }

  # Check if the ticket is created for queue we care about
  my $config_queue = $ConfigObject->Get("Webservice::PO::ActiveCustomers")->{$Ticket{CustomerID}. '::Queue'};
  if (index($Ticket{Queue}, $config_queue) != 0) { # If not started with the config_queue
    $LogObject->Log(Priority => 'info',
                    Message  => "$log_pref - Rejected: Unimportant queue = $Ticket{Queue}");
    return 1;
  }
  
  if ($Param{Event} eq 'TicketCreate') { # We don't care about it!
    $LogObject->Log(Priority => 'info',
                    Message  => "$log_pref - Rejected: TicketCreate event has caused an unexpected event."
    );
    return 1;
  }

  $LogObject->Log(Priority => 'info',
                  Message  => "$log_pref - Accepted because of queue = $Ticket{Queue}");

  my $sync = "/opt/otrs/Custom/Webservice/POInterface/syncout";

  # Running in background doesn't work this way:
  # my @result = `unset MOD_PERL && nohup $sync -w -d $Ticket{TicketID} > /tmp/otrs-po.nohup &`;

  system("unset MOD_PERL && sleep 5 && nohup $sync -w -d $Ticket{TicketID} >> /tmp/otrs-po.nohup &");
  # system("unset MOD_PERL && sleep 5 && nohup $sync -w -d -y -x http://localhost:3001/pi $Ticket{TicketID} > /tmp/otrs-po.nohup &");

  $LogObject->Log(Priority => 'info',
                  Message  => "$log_pref - Done calling the sync script");

  return 1;
}

sub precheck_settings {
  my ($Self, %Param) = @_;

  my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
  my $LogObject = $Kernel::OM->Get('Kernel::System::Log');
  my $ConfigObject = $Kernel::OM->Get('Kernel::Config');

  # Check for required keys in %Param
  for (qw(Data Event Config UserID)) {
    if (!$Param{$_}) {
      $LogObject->Log(Priority => 'error',
                      Message  => "[POInterface] Need $_!");
      return 1;
    }
  }

  # Check for required keys in $Param{Data}
  for (qw(TicketID)) {
    if (!$Param{Data}->{$_}) {
      $LogObject->Log(Priority => 'error',
                      Message  => "[POInterface] Need $_ in Data!");
      return 1;
    }
  }

  my $general_params = $ConfigObject->Get("Webservice::PO::General");
  $LogObject->Log(Priority => 'info', 
                  Message => "[POInterface] Webservice::PO::General = ". Dumper($general_params));

  for (qw(WSProxy)) {
    if (!$general_params->{$_}) {
      $LogObject->Log(Priority => 'error',
                      Message  => "[POInterface] Need $_ in Webservice::PO::General!");
      return 1;
    }
  }
  return 0;
}

1;

