package Kernel::System::Ticket::Event::ZWriteSLATimesToDynamicFields;

use strict;
use warnings;

our @ObjectDependencies = (
   'Kernel::Config',
   'Kernel::System::DB',
   'Kernel::System::Log',
   'Kernel::System::Time',
   'Kernel::System::Ticket',
   'Kernel::System::DynamicField',
   'Kernel::System::DynamicField::Backend',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;
    # check needed stuff
    for (qw(Data Event Config UserID)) {
        if ( !$Param{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[WriteSLA..] Need $_!"
            );
            return;
        }
    }
    for (qw(TicketID)) {
        if ( !$Param{Data}->{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[WriteSLA..] Need $_ in Data!"
            );
            return;
        }
    }
    # get objects
    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
    my $TimeObject = $Kernel::OM->Get('Kernel::System::Time');
    my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
    my $ConfigObject = $Kernel::OM->Get('Kernel::Config');
    my $DynamicFieldObject = $Kernel::OM->Get('Kernel::System::DynamicField');
    my $BackendObject = $Kernel::OM->Get('Kernel::System::DynamicField::Backend');

    # get current ticket data
    my %Ticket = $TicketObject->TicketGet(
        TicketID      => $Param{Data}->{TicketID},
        UserID        => $Param{UserID},
        DynamicFields => 1,
    );
    if (!($Ticket{State} eq 'closed' || $Ticket{State} eq 'in progress')) {
	return 1;
    }
    return 1 if !$Ticket{SLA};

    my %SLA = $Kernel::OM->Get('Kernel::System::SLA')->SLAGet(
        SLAID  => $Ticket{SLAID},
        UserID => 1,
    );

    if (!($SLA{FirstResponseTime} || $SLA{SolutionTime})){
        return 1;
    }

    $DBObject->Prepare(
        SQL => 'select ticket_state.name, ticket_history.create_time from ticket_history, ticket_state where ticket_id = ? and history_type_id = 27 and state_id = ticket_state.id order by ticket_history.create_time',
        Bind => [ \$Ticket{TicketID} ],
    );

    my $SecondsInPausedState = 0;
    my $PrevState;
    my $PrevTime;
    while (my @Row = $DBObject->FetchrowArray()) {
        if (defined $PrevState && defined $PrevTime) {
            if ( $ConfigObject->Get('SLATimeFreeze::PausedStates')->{$PrevState}) {
                $SecondsInPausedState = $SecondsInPausedState +
                ( $TimeObject->TimeStamp2SystemTime( String => $Row[1] ) -
                  $TimeObject->TimeStamp2SystemTime( String => $PrevTime ) )
            }
        }
        $PrevState =  $Row[0];
        $PrevTime  =  $Row[1];
    }

    #$Kernel::OM->Get('Kernel::System::Log')->Log(
    #    Priority => 'error',
    #    Message  => "DEBUG",
    #);

    my $StopTime = $TimeObject->SystemTime() - $SecondsInPausedState;

    my $WorkingTime = $TimeObject->WorkingTime(
        StartTime => $Ticket{CreateTimeUnix},
        StopTime  => $StopTime,
        Calendar  => $SLA{Calendar}, 
    );

    if ($Ticket{State} eq 'in progress') {

        return 1 if !$SLA{FirstResponseTime};

        my $DynamicFieldConfig = $DynamicFieldObject->DynamicFieldGet(
            ID   => 37,
        );

	my $DF_Name = "DynamicField_".$DynamicFieldConfig->{Name};
	return 1 if $Ticket{$DF_Name};

        my $Time = ($SLA{FirstResponseTime}*60)-$WorkingTime;

        #$Kernel::OM->Get('Kernel::System::Log')->Log(
        #    Priority => 'info',
        #    Message  => "DEBUG $SLA{FirstResponseTime}, $WorkingTime, $Time, $SecondsInPausedState",
        #);

        my $Minutes = $Time/60;
	$Minutes =~ s/\.\d+//g;
        my $Seconds = ($Time/60)%60;

        my $Success = $BackendObject->ValueSet(
            DynamicFieldConfig => $DynamicFieldConfig,      
            ObjectID           => $Param{Data}->{TicketID},
            Value              => $Minutes."m ".$Seconds."s",                   
            UserID             => 1,
        );

    }

    if ($Ticket{State} eq 'closed') {

        return 1 if !$SLA{SolutionTime};

        my $DynamicFieldConfig = $DynamicFieldObject->DynamicFieldGet(
            ID   => 38,
        );

        my $DF_Name = "DynamicField_".$DynamicFieldConfig->{Name};
        return 1 if $Ticket{$DF_Name};

        my $Time = ($SLA{SolutionTime}*60)-$WorkingTime;
	my $Minutes = $Time/60;
        $Minutes =~ s/\.\d+//g;
        my $Seconds = ($Time/60)%60;

        my $Success = $BackendObject->ValueSet(
            DynamicFieldConfig => $DynamicFieldConfig,
            ObjectID           => $Param{Data}->{TicketID},
            Value              => $Minutes."m ".$Seconds."s",
            UserID             => 1,
        );

    } 
    return 1;
}

1;
