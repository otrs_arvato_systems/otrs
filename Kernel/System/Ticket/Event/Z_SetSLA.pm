package Kernel::System::Ticket::Event::Z_SetSLA;

use strict;
use warnings;

our @ObjectDependencies = (
    'Kernel::System::Log',
    'Kernel::System::Ticket',
    'Kernel::Config',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;


    # check needed stuff
    for (qw(Data Event Config UserID)) {
        if ( !$Param{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[SetSLA] Need $_!"
            );
            return;
        }
    }
    for (qw(TicketID)) {
        if ( !$Param{Data}->{$_} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "[SetSLA] Need $_ in Data!"
            );
            return;
        }
    }

    # get ticket object
    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

    # get current ticket data
    my %Ticket = $TicketObject->TicketGet(
        TicketID      => $Param{Data}->{TicketID},
        UserID        => 1,
        DynamicFields => 1,
    );

    return 1 if !$Ticket{CustomerID};
    return 1 if !$Kernel::OM->Get('Kernel::Config')->Get('SLA::'.$Ticket{CustomerID});

    $Ticket{Type} = 'Unclassified' if !$Ticket{Type};
    $Ticket{Priority} = '3 - medium' if !$Ticket{Priority};

    if (!$Ticket{Service}) {

        my $Success = $TicketObject->TicketServiceSet(
            Service  => 'undefined',
            TicketID => $Param{Data}->{TicketID},
            UserID   => 1,
        );
	$Ticket{Service} = 'undefined';
    }

    my $Config = $Kernel::OM->Get('Kernel::Config')->Get('SLA::'.$Ticket{CustomerID});

    my %ConfigLine;
    foreach my $Key (keys %{$Config}) {

        %ConfigLine = map{split /\=/, $_}(split /\|/, $Key);

        my $SLA = $Config->{$Key};
        foreach my $Key2 (keys %ConfigLine) {
            next if $ConfigLine{$Key2} eq 'Any';;
            next if !$Ticket{$Key2};
            undef $SLA if ($ConfigLine{$Key2} ne $Ticket{$Key2});
        }
	undef %ConfigLine;

        my $Success = $TicketObject->TicketSLASet(
            SLA      => "$SLA",
            TicketID => $Param{Data}->{TicketID},
            UserID   => 1,
        ) if $SLA;
        last if $SLA;
    }
    return 1;
 
}

1;
