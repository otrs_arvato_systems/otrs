package Kernel::System::Stats::Static::MosolfReportingFrank;

use strict;
use warnings;


our @ObjectDependencies = (
    'Kernel::System::Queue',
    'Kernel::System::Ticket',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Param {
    my $Self = shift;

    my @Params;

    return @Params;
}


sub Run {
    my ( $Self, %Param ) = @_;

	my @Data;

    # set report title
    my $Title = 'Tickets nach Aufwand sortiert';

    # table headlines
    my @HeadData = (
        'Ticket Number',
        'Titel',
        'Erstellzeit',
        'Status',
        'Typ',
        'Externe Ticketnummer Mosolf',
        'Aktueller Aufwand in PT',
    );

	my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
	
    my @TicketIDs = $TicketObject->TicketSearch(
        UserID          => 1,
        Result          => 'ARRAY',
        StateType       => 'Open',
        Queues          => [ 'SME::Mosolf', 'SME::Mosolf::Basis' ],
        DynamicField_Ordernumber => {
			Like  => [ '44344005-200 Mosolf*', '44344005-300 Mosolf*' ],
        },
        OrderBy			=> 'Down',
        SortBy			=> 'DynamicField_IstAufwandCATS',
    );

    

    for my $TicketID ( @TicketIDs ) {
	
        my %Ticket = $TicketObject->TicketGet(
            UserID => 1,
            TicketID => $TicketID,
            DynamicFields => 1,
            Extended => 1,
        );

        if ( defined $Ticket{DynamicField_IstAufwandCATS} ) {
                        $Ticket{DynamicField_IstAufwandCATS} =~  s/\./\,/g;
        }

        push @Data, [
            $Ticket{TicketNumber},
            $Ticket{Title},
            $Ticket{Created},
            $Ticket{State},
            $Ticket{Type},
            "$Ticket{DynamicField_ExternalTicketnumber}"."\t",
            $Ticket{DynamicField_IstAufwandCATS},
         ];
    }

    return ( [$Title], [@HeadData], @Data );
}

1;
