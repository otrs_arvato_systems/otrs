package Kernel::System::Stats::Static::WeeklyReporting;

use strict;
use warnings;

our @ObjectDependencies = (
    'Kernel::System::Queue',	
    'Kernel::System::Ticket',
    'Kernel::System::Time',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Param {
    my $Self = shift;

    my @Params;

    return @Params;
}

sub Run {
    my ( $Self, %Param ) = @_;

    # check needed stuff
    for my $Needed (qw()) {
        if ( !$Param{$Needed} ) {
            $Kernel::OM->Get('Kernel::System::Log')->Log(
                Priority => 'error',
                Message  => "Need $Needed!",
            );
            return;
        }
    }

    # set report title
    my $Title = 'Datenbasis fuer QlickView';

    # table headlines
    my @HeadData = (
        'Ticket#',
        'Title',
        'Created',
	'SolvedDateTime',
        'Close Time',
        'Queue',
        'State',
        'Priority',
        'Customer User',
        'CustomerID',
	'SLA',
        'Service',
        'Type',
        'Agent/Owner',
        'Client',
        'Externe Ticketnummer Mosolf',
	'Incident Opened',
	'Change Opened',
	'Current effort',
	'Changed at',
	'FirstResponseTimeDestinationDate',
	'FirstResponseTimeEscalation',
	'FirstResponseTime',
	'FirstResponseDiffInMin',
	'SolutionTimeDestinationDate',
	'SolutionTimeEscalation',
	'SolutionTime',
	'SolutionDiffInMin',
	'EscalationDestinationDate',
    );
	
    # get ticket object
    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
    # get config object
    my $ConfigObject = $Kernel::OM->Get('Kernel::Config');
    #get ticket object
    my $TimeObject = $Kernel::OM->Get('Kernel::System::Time');

    my $DBObject = $Kernel::OM->Get('Kernel::System::DB');

    #returns format yyyy-mm-dd 23:59:59
    my $TimeStamp = $TimeObject->CurrentTimestamp();

    #PAEP001 03.04.2017 --> Added filter on creation date
    my $TicketIDs2DArrRef = $DBObject->SelectAll(
        SQL   => "SELECT id FROM ticket 
		  WHERE ticket_state_id <> 9 
		  AND queue_id <> 36 
		  AND create_time >= DATE_SUB('$TimeStamp',INTERVAL 18 MONTH)",
    );

    #my $TicketsInString = join(",", map { "$_->[0]" } @{$TicketIDs2DArrRef} );

#$Kernel::OM->Get('Kernel::System::Email')->Send(
#                From => 'otrs@arvato-systems.de',
#                To => 'khaishaun.ng@bertelsmann.de',
#                Subject => 'Test email from OTRS Prod - WeeklyReporting.pm',
#                Charset => 'iso-8859-15',
                #Charset => 'utf-8',
#                MimeType => 'text/plain',
#                Body => "List of tickets:\n$TicketsInString",
#);

    my @Data;

#    my %Tickets = $TicketObject->TicketGet(
#	UserID => 1,
#	TicketID => $TicketsInString,
#	DynamicFields => 1,
#	Extended => 1,
#    );

    foreach my $TicketIDRef ( @{$TicketIDs2DArrRef} ) {
        my $TicketID = $TicketIDRef->[0];
        my %Ticket = $TicketObject->TicketGet(
            UserID => 1,
            TicketID => $TicketID,
            DynamicFields => 1,
	    Extended => 1,
        );

#	next if $Ticket{State} eq "merged";
#	next if $Ticket{Queue} eq "Testtickets";

	if ($Ticket{Queue} && $Ticket{Queue} =~ /Bertelsmann::/) {
	    $Ticket{Queue} =~ s/Bertelsmann:://g;
	}
	
	if ($Ticket{CustomerID} && $ConfigObject->Get('WeeklyReporting::SME Customers')->{$Ticket{CustomerID}}) {
		$Ticket{Queue}="SME";
	}

	$Ticket{DynamicField_ExternalTicketnumber} .= "\t" if $Ticket{DynamicField_ExternalTicketnumber};
        $Ticket{DynamicField_IstAufwandCATS} =~ s/\./\,/g if $Ticket{DynamicField_IstAufwandCATS};

	push @Data, [
		$Ticket{TicketNumber},
		$Ticket{Title},
		$Ticket{Created},
		$Ticket{DynamicField_SolvedDateTime},
		$Ticket{Closed},
		$Ticket{Queue},
		$Ticket{State},
		$Ticket{Priority},
		$Ticket{CustomerUserID},
		$Ticket{CustomerID},
		$Ticket{SLA},
		$Ticket{Service},
		$Ticket{Type},
		$Ticket{Owner},
		$Ticket{DynamicField_Client},
		$Ticket{DynamicField_ExternalTicketnumber},
		" ", #Incident
		" ", #Change
		$Ticket{DynamicField_IstAufwandCATS},
		$Ticket{Changed},
		$Ticket{FirstResponseTimeDestinationDate},
		$Ticket{FirstResponseTimeEscalation},
		$Ticket{FirstResponseTime},
		$Ticket{FirstResponseDiffInMin},
		$Ticket{SolutionTimeDestinationDate},
		$Ticket{SolutionTimeEscalation},
		$Ticket{SolutionTime},
		$Ticket{SolutionDiffInMin},
		$Ticket{EscalationDestinationDate},
	];
	
    }

    return ( [$Title], [@HeadData], @Data );
}

1;
