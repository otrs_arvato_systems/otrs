# --
# Kernel/System/Stats/Static/StateAction.pm - static stat for ticket history
# Copyright (C) 2001-2015 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::System::Stats::Static::LLTicketAssignment;
## nofilter(TidyAll::Plugin::OTRS::Perl::Time)

use strict;
use warnings;
use Date::Pcalc qw(Days_in_Month);

our @ObjectDependencies = (
    'Kernel::Language',
    'Kernel::System::DB',
    'Kernel::System::Time',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Param {
    my $Self = shift;

    my %Queues = $Kernel::OM->Get('Kernel::System::Queue')->GetAllQueues();

    my @Params = (
        {
            Frontend  => 'Queue',
            Name      => 'QueueIDs',
            Multiple  => 1,
            Size      => 3,
            Data      => \%Queues,
        },
    );

    # get current time
    my ( $s, $m, $h, $D, $M, $Y ) = $Kernel::OM->Get('Kernel::System::Time')->SystemTime2Date(
        SystemTime => $Kernel::OM->Get('Kernel::System::Time')->SystemTime(),
    );
    $D = sprintf( "%02d", $D );
    $M = sprintf( "%02d", $M );
    $Y = sprintf( "%02d", $Y );

    # create possible time selections
    my %Year = map { $_, $_ } ( $Y - 10 .. $Y + 1 );
    my %Month = map { sprintf( "%02d", $_ ), sprintf( "%02d", $_ ) } ( 1 .. 12 );
    my %Day   = map { sprintf( "%02d", $_ ), sprintf( "%02d", $_ ) } ( 1 .. 31 );

    push @Params, {
        Frontend   => 'Start day',
        Name       => 'StartDay',
        Multiple   => 0,
        Size       => 0,
        SelectedID => '01',
        Data       => {
            %Day,
        },
    };
    push @Params, {
        Frontend   => 'Start month',
        Name       => 'StartMonth',
        Multiple   => 0,
        Size       => 0,
        SelectedID => $M,
        Data       => {
            %Month,
        },
    };
    push @Params, {
        Frontend   => 'Start year',
        Name       => 'StartYear',
        Multiple   => 0,
        Size       => 0,
        SelectedID => $Y,
        Data       => {
            %Year,
        },
    };

    push @Params, {
        Frontend   => 'End day',
        Name       => 'EndDay',
        Multiple   => 0,
        Size       => 0,
        SelectedID => $D,
        Data       => {
            %Day,
        },
    };
    push @Params, {
        Frontend   => 'End month',
        Name       => 'EndMonth',
        Multiple   => 0,
        Size       => 0,
        SelectedID => $M,
        Data       => {
            %Month,
        },
    };
    push @Params, {
        Frontend   => 'End year',
        Name       => 'EndYear',
        Multiple   => 0,
        Size       => 0,
        SelectedID => $Y,
        Data       => {
            %Year,
        },
    };

    return @Params;
}

sub Run {
    my ( $Self, %Param ) = @_;

    # get language object
    my @Data;

    # set report title
    my $Title = "LLTicketAssignment $Kernel::OM->Get('Kernel::System::Time')->CurrentTimestamp()";

    # table headlines
    my @HeadData = (
        'TicketNummer arvato Systems',
        'TicketNummer Bechtle',
	'Erstelldatum',
        'Erstellt in Queue',
	'Aktuelle Queue',
	'Anzahl Verschoben',
	'Verschoben nach',
    );

    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
    my $DBObject = $Kernel::OM->Get('Kernel::System::DB');

    my $StartDay = sprintf( "%02d", Days_in_Month( $Param{StartYear}, $Param{StartMonth} ) );
    if ( $Param{StartDay} < $StartDay ) {
        $StartDay = $Param{StartDay};
    }

    # correct end day of month if entered wrong by user
    my $EndDay = sprintf( "%02d", Days_in_Month( $Param{EndYear}, $Param{EndMonth} ) );
    if ( $Param{EndDay} < $EndDay ) {
        $EndDay = $Param{EndDay};
    }

    # set start and end date
    my $StartDate = "$Param{StartYear}-$Param{StartMonth}-$StartDay 00:00:00";
    my $EndDate   = "$Param{EndYear}-$Param{EndMonth}-$EndDay 23:59:59";

    my @TicketIDs = $TicketObject->TicketSearch(
        Result => 'ARRAY',
        UserID => 1,
	QueueIDs => $Param{QueueIDs},
	TicketCreateTimeNewerDate => $StartDate,
	TicketCreateTimeOlderDate => $EndDate,
        # %Param,
    ); 

    foreach my $TicketID (@TicketIDs){

        my %Ticket = $TicketObject->TicketGet(
            TicketID      => $TicketID,
            DynamicFields => 1,         
            UserID        => 1,
        );

	my $ErstelltInQueue;
	$DBObject->Prepare(
            SQL   => "SELECT queue.name FROM queue, ticket_history where queue.id = ticket_history.queue_id and history_type_id = 1 and ticket_id = $TicketID",
        );

	while (my @Row = $DBObject->FetchrowArray()) {
		$ErstelltInQueue = $Row[0];
        }

        $DBObject->Prepare(
            SQL   => "SELECT queue.name FROM queue, ticket_history where queue.id = ticket_history.queue_id and history_type_id = 16 and ticket_id = $TicketID",
        );

        my $VerschobenNach;
	my $Counter = 0;

        while (my @Row2 = $DBObject->FetchrowArray()) {
            $VerschobenNach .= "$Row2[0], ";
	    $Counter += 1;
        }

        chomp($VerschobenNach);
        chomp($VerschobenNach);

	push @Data, [
            $Ticket{TicketNumber},
            $Ticket{DynamicField_ExternalTicketnumber},
	    $Ticket{Created},
	    $ErstelltInQueue,
            $Ticket{Queue},
	    $Counter,
            $VerschobenNach  || " ",
	]; 
    
    }

    my @Result = ( [$Title], [@HeadData], @Data );

    return @Result;

}

1;
