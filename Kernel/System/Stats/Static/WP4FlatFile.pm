# --
# Kernel/System/Stats/Static/StateAction.pm - static stat for ticket history
# Copyright (C) 2001-2015 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::System::Stats::Static::WP4FlatFile;
## nofilter(TidyAll::Plugin::OTRS::Perl::Time)

use strict;
use warnings;

our @ObjectDependencies = (
    'Kernel::System::Queue',
    'Kernel::System::Ticket',
    'Kernel::System::DB',
    'Kernel::Config',
    'Kernel::System::Time',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Param {
    my $Self = shift;

    # my %Queues = $Kernel::OM->Get('Kernel::System::Queue')->GetAllQueues();

    my @Params = (
        # {
        #    Frontend  => 'Queue',
        #    Name      => 'QueueIDs',
        #    Multiple  => 1,
        #    Size      => 3,
        #    Data      => \%Queues,
        # },
    );

    return @Params;
}

sub Run {
    my ( $Self, %Param ) = @_;

    my @Data;

    # set report title
    my $Title = 'Datenfile für das WP4';

    # table headlines
    my @HeadData = (
	'Auftragsnummer',
        'Ticket#',
        'Externe Ticketnummer',
	'Titel',
        'Angelegt am',
	'Angelegt von',
        'Geschlossen am',
        'Queue',
        'Status',
        'Priorität',
        'Kundenbenutzer',
        'Kundenfirma',
        'Service',
        'Typ',
        'Agent/Owner',
	'Mandant',
	'Lösung',
	'Aufwandsschätzung',
	'Geändert am',
	'Reaktionszeit',
	'Reaktionszeit eskalliert',
	'Lösungszeit',
	'Lösungszeit eskalliert',
        'Pending Zeit',
    );

    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
    my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
    my $ConfigObject = $Kernel::OM->Get('Kernel::Config');
    my $TimeObject = $Kernel::OM->Get('Kernel::System::Time');

    my @TicketIDs = $TicketObject->TicketSearch(
        Result => 'ARRAY',
        UserID => 1,
	ArchiveFlags => ['n', 'y'],
	%Param,
    ); 

    foreach my $TicketID (@TicketIDs){

        my %Ticket = $TicketObject->TicketGet(
            UserID => 1,
            TicketID => $TicketID,
            DynamicFields => 1,
	    Extended => 1,
        );
		
	my %Article = $TicketObject->ArticleFirstArticle(
		TicketID      => $TicketID,
	);
		
	my $ResEsc;
	my $SolEsc;
	my $PendingTime;
	
	if (defined $Ticket{FirstResponseDiffInMin} < 0) {
		$ResEsc = "X";
	}
		
	if (defined $Ticket{SolutionDiffInMin} < 0) {
		$SolEsc = "X";
	}
		
	$DBObject->Prepare(
		SQL => 'select ticket_state.name, ticket_history.create_time from ticket_history, ticket_state where ticket_id = ? and history_type_id = 27 and state_id = ticket_state.id order by ticket_history.create_time',
		Bind => [ \$Ticket{TicketID} ],
	);
		
	my $SecondsInPausedState;
        my $PrevState;
        my $PrevTime;
        while (my @Row = $DBObject->FetchrowArray()) {
                if (defined $PrevState && defined $PrevTime) {
                        if ( $ConfigObject->Get('SLATimeFreeze::PausedStates')->{$PrevState}) {
                            $SecondsInPausedState +=
                                (   $TimeObject->TimeStamp2SystemTime( String => $Row[1] ) -
                                    $TimeObject->TimeStamp2SystemTime( String => $PrevTime ) )
                        }
                }
                $PrevState =  $Row[0];
                $PrevTime  =  $Row[1];
        }
	
	if (defined $SecondsInPausedState) {
	
		my $days  = int($SecondsInPausedState/(24*60*60));
		my $hours = ($SecondsInPausedState/(60*60))%24;
		my $mins  = ($SecondsInPausedState/60)%60;         
		my $secs  = $SecondsInPausedState%60;
		$PendingTime = $days." d ".$hours." h ".$mins." m ".$secs." s";
	
	}

	$Ticket{DynamicField_ExternalTicketnumber} .= "\n" if $Ticket{DynamicField_ExternalTicketnumber};

	foreach my $On ( @{$Ticket{DynamicField_Ordernumber}} ) {

	    next if $On eq 'no transfer to CATS';

	    $On = substr $On, 0, 8;

            $Ticket{DynamicField_ExternalTicketnumber} =~ s/\n//g if $Ticket{DynamicField_ExternalTicketnumber};

	    push @Data, [
		$On,
                $Ticket{TicketNumber},
                $Ticket{DynamicField_ExternalTicketnumber},
                $Ticket{Title},
		$Ticket{Created},
		$Article{From},
		$Ticket{Closed},
		$Ticket{Queue},
		$Ticket{State},
		$Ticket{Priority},
		$Ticket{CustomerUserID},
		$Ticket{CustomerID},
		$Ticket{Service},
		$Ticket{Type},
		$Ticket{Owner},
		$Ticket{DynamicField_Client},
		"\t",
		$Ticket{DynamicField_CostEstimation}, #Aufwandsschätzung
		$Ticket{Changed},
		$Ticket{FirstResponse},
		$ResEsc,
		$Ticket{SolutionTime},
		$SolEsc,
		$PendingTime,
            ];
	}
    } 
    
    my @Result = ( [$Title], [@HeadData], @Data );

    return @Result;

}

1;
