# --
# Kernel/System/Stats/Static/StateAction.pm - static stat for ticket history
# Copyright (C) 2001-2015 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::System::Stats::Static::ReportBMG;
## nofilter(TidyAll::Plugin::OTRS::Perl::Time)

use strict;
use warnings;

our @ObjectDependencies = (
    'Kernel::System::Queue',
    'Kernel::System::Ticket',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    return $Self;
}

sub Param {
    my $Self = shift;

    my %Queues = $Kernel::OM->Get('Kernel::System::Queue')->GetAllQueues();

    my @Params = (
        {
            Frontend  => 'Queue',
            Name      => 'QueueIDs',
            Multiple  => 1,
            Size      => 3,
            Data      => \%Queues,
        },
    );

    return @Params;
}

sub Run {
    my ( $Self, %Param ) = @_;

    # get language object
    my @Data;

    # set report title
    my $Title = 'Tickets per Queue';

    # table headlines
    my @HeadData = (
        'Ticket Number',
        'ExterneTicketnummer',
        'Typ',
        'Titel',
        'Erstellzeit',
        'Mandant',
        'Kunde',
        'Kundenuser',
        'Queue',
        'Besitzer',
        'State',
        'Service',
        'SLA',
        'FRTEscalation',
        'UTEscalation',
        'STEscalation',
        'Auftragsnummer',
        'IstAufwandCATS',
        'Kommentar zum Ticketstatus',
    );

    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

    my @TicketIDs = $TicketObject->TicketSearch(
        Result => 'ARRAY',
        UserID => 1,
	StateType => ['new', 'open', 'pending reminder' ],
	%Param,
    ); 

    foreach my $TicketID (@TicketIDs){

        my %Ticket = $TicketObject->TicketGet(
            TicketID      => $TicketID,
            DynamicFields => 1,         
            UserID        => 1,
        );

	 push @Data, [
                 $Ticket{TicketNumber},
                 "$Ticket{DynamicField_ExternalTicketnumber}\t",
                 $Ticket{Type},
                 $Ticket{Title},
                 $Ticket{Created},
                 $Ticket{DynamicField_Client},
                 $Ticket{CustomerID},
                 $Ticket{CustomerUserID},
                 $Ticket{Queue},
                 $Ticket{Owner},
                 $Ticket{State},
                 $Ticket{Service},
                 $Ticket{SLA},
                 $Ticket{FirstResponseTimeEscalation},
                 $Ticket{UpdateTimeEscalation},
                 $Ticket{SolutionTimeEscalation},
                 $Ticket{DynamicField_Ordernumber}->[0],
                 $Ticket{DynamicField_IstAufwandCATS},
                 $Ticket{DynamicField_StatusComment}
        ];
    } 
    
    my @Result = ( [$Title], [@HeadData], @Data );

    return @Result;

}

1;
