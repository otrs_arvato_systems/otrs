# --
# Kernel/Output/HTML/ToolBarTicketInvolved.pm
#

package Kernel::Output::HTML::ToolBar::TicketInvolved;

use strict;
use warnings;
no warnings qw(uninitialized);

our @ObjectDependencies = (
    'Kernel::System::Cache',
    'Kernel::System::DB',
    'Kernel::System::Log',
);

#sub new {
#    my ( $Type, %Param ) = @_;

    # allocate new hash for object
#    my $Self = {};
#    bless( $Self, $Type );

    # get needed objects
#    for (qw(ConfigObject LogObject DBObject TicketObject LayoutObject UserID)) {
#        $Self->{$_} = $Param{$_} || die "Got no $_!";
#    }
#    return $Self;
#}

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    $Self->{CacheType} = 'Valid';
    $Self->{CacheTTL}  = 60 * 60 * 24 * 20;

    $Self->{ConfigObject} = $Kernel::OM->Get('Kernel::Config');
    $Self->{LogObject} = $Kernel::OM->Get('Kernel::System::Log');
    $Self->{DBObject} = $Kernel::OM->Get('Kernel::System::DB');
    $Self->{TicketObject} = $Kernel::OM->Get('Kernel::System::Ticket');
    $Self->{LayoutObject} = $Kernel::OM->Get('Kernel::Output::HTML::Layout');
    $Self->{UserID} = $Param{UserID};
    
    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;

    # check responsible feature
    return if !$Self->{ConfigObject}->Get('Ticket::Responsible');

    # check needed stuff
    for (qw(Config)) {
        if ( !$Param{$_} ) {
            $Self->{LogObject}->Log( Priority => 'error', Message => "Need $_!" );
            return;
        }
    }

    my @TicketsO = $Self->{TicketObject}->TicketSearch(
        Result         => 'ARRAY',
        StateType      => 'Open',
        OwnerIDs       => [ $Self->{UserID} ],
        UserID         => 1,
        Permission     => 'ro',
    );

    my @TicketsR = $Self->{TicketObject}->TicketSearch(
        Result         => 'ARRAY',
        StateType      => 'Open',
        ResponsibleIDs => [ $Self->{UserID} ],
        UserID         => 1,
        Permission     => 'ro',
    );
    push (@TicketsO, @TicketsR);
    my %Tickets = map { $_ => 1 } @TicketsO;
    my @Tickets = keys %Tickets;
    my $Count = $#Tickets + 1;

    my @TicketsNewO = $Self->{TicketObject}->TicketSearch(
        Result         => 'ARRAY',
        StateType      => 'Open',
        OwnerIDs       => [ $Self->{UserID} ],
        TicketFlag     => {
            Seen => 1,
        },
        TicketFlagUserID => $Self->{UserID},
        UserID         => 1,
        Permission     => 'ro',
    );

    my @TicketsNewR = $Self->{TicketObject}->TicketSearch(
        Result         => 'ARRAY',
        StateType      => 'Open',
        ResponsibleIDs => [ $Self->{UserID} ],
        TicketFlag     => {
            Seen => 1,
        },
        TicketFlagUserID => $Self->{UserID},
        UserID         => 1,
        Permission     => 'ro',
    );
    push (@TicketsNewO, @TicketsNewR);
    my %TicketsNew = map { $_ => 1 } @TicketsNewO;
    my @TicketsNew = keys %TicketsNew;

    my $CountNew = $#TicketsNew +1;
    $CountNew = $Count - $CountNew;

    my @TicketsReachedO = $Self->{TicketObject}->TicketSearch(
        Result         => 'ARRAY',
        StateType      => 'pending reminder',
        OwnerIDs       => [ $Self->{UserID} ],
        TicketPendingTimeOlderMinutes => 1,
        UserID         => 1,
        Permission     => 'ro',
    );

    my @TicketsReachedR = $Self->{TicketObject}->TicketSearch(
        Result         => 'ARRAY',
        StateType      => 'pending reminder',
        ResponsibleIDs => [ $Self->{UserID} ],
        TicketPendingTimeOlderMinutes => 1,
        UserID         => 1,
        Permission     => 'ro',
    );
    push (@TicketsReachedO, @TicketsReachedR);
    my %TicketsReached = map { $_ => 1 } @TicketsReachedO;
    my @TicketsReached = keys %TicketsReached;
    my $CountReached = $#TicketsReached +1;

    my $Class        = $Param{Config}->{CssClass};
    my $ClassNew     = $Param{Config}->{CssClassNew};
    my $ClassReached = $Param{Config}->{CssClassReached};

    my $Icon        = $Param{Config}->{Icon};
    my $IconNew     = $Param{Config}->{IconNew};
    my $IconReached = $Param{Config}->{IconReached};

    my $URL = $Self->{LayoutObject}->{Baselink};
    my %Return;
    my $Priority = $Param{Config}->{Priority};
    if ($CountNew) {
        $Return{ $Priority++ } = {
            Block       => 'ToolBarItem',
            Description => 'Invovled Tickets New',
            Count       => $CountNew,
            Class       => $ClassNew,
            Icon        => $IconNew,
            Link        => $URL . 'Action=AgentTicketInvolvedView;Filter=New',
            AccessKey   => '',
        };
    }
    if ($CountReached) {
        $Return{ $Priority++ } = {
            Block       => 'ToolBarItem',
            Description => 'Involved Tickets Reminder Reached',
            Count       => $CountReached,
            Class       => $ClassReached,
            Icon        => $IconReached,
            Link        => $URL . 'Action=AgentTicketInvolvedView;Filter=ReminderReached',
            AccessKey   => '',
        };
    }
    if ($Count) {
        $Return{ $Priority++ } = {
            Block       => 'ToolBarItem',
            Description => 'Involved Tickets Total',
            Count       => $Count,
            Class       => $Class,
            Icon        => $Icon,
            Link        => $URL . 'Action=AgentTicketInvolvedView',
            AccessKey   => 'i',
        };
    }
    return %Return;
}

1;
