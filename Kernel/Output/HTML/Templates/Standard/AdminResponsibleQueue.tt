# --
# AdminResponsibleQueue.tt - provides HTML form for AdminInterface
# Copyright (C) 2001-2014 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

[% RenderBlockStart("Overview") %]
<div class="MainBox ARIARoleMain LayoutFixedSidebar SidebarFirst">
    <h1>[% Translate("Manage Responsible-Queue Relations") | html %]</h1>

    <div class="SidebarColumn">

[% RenderBlockStart("ActionList") %]
        <div class="WidgetSimple">
            <div class="Header">
                <h2>[% Translate("Actions") | html %]</h2>
            </div>
            <div class="Content">
                <ul class="ActionList">
[% RenderBlockStart("ActionOverview") %]
                    <li>
                        <a href="[% Env("Baselink") %]Action=[% Env("Action") %]" class="CallForAction Fullsize Center"><span><i class="fa fa-caret-left"></i>[% Translate("Go to overview") | html %]</span></a>
                    </li>
[% RenderBlockEnd("ActionOverview") %]
                </ul>
            </div>
        </div>
[% RenderBlockEnd("ActionList") %]

[% RenderBlockStart("UserFilter") %]
        <div class="WidgetSimple">
            <div class="Header">
                <h2><label for="FilterUsers">[% Translate("Filter for Agents") | html %]</label></h2>
            </div>
            <div class="Content">
                <input type="text" id="FilterUsers" class="FilterBox" placeholder="[% Translate("Just start typing to filter...") | html %]" name="FilterUser" value="" title="[% Translate("Filter for Agents") | html %]" />
            </div>
        </div>
[% RenderBlockEnd("UserFilter") %]
[% RenderBlockStart("QueueFilter") %]
        <div class="WidgetSimple">
            <div class="Header">
                <h2><label for="FilterQueues">[% Translate("Filter for Queues") | html %]</label></h2>
            </div>
            <div class="Content">
				<input type="text" id="FilterQueues" class="FilterBox" placeholder="[% Translate("Just start typing to filter...") | html %]" name="FilterQueues" value=""  title="[% Translate("Filter for Queues") | html %]"/>
			</div>
        </div>
[% RenderBlockEnd("QueueFilter") %]
    </div>

    <div class="ContentColumn">
        <div class="WidgetSimple">

[% RenderBlockStart("OverviewResult") %]
            <div class="Header">
                <h2>[% Translate("Overview") | html %]</h2>
            </div>
            <div class="Content LayoutGrid ColumnsWithSpacing">
                <div class="Size1of2">
                    <ul class="Tablelike" id="Users">
                        <li class="Header">[% Translate("Agents") | html %]</li>
                        <li class="FilterMessage Hidden">[% Translate("No matches found.") | html %]</li>
[% RenderBlockStart("List1n") %]
                        <li><a href="[% Env("Baselink") %]Action=[% Env("Action") %];Subaction=[% Data.Subaction | uri %];ID=[% Data.ID | uri %]" class="AsBlock">[% Data.Name | html %]</a></li>
[% RenderBlockEnd("List1n") %]
                    </ul>
                </div>
                <div class="Size1of2">
                    <ul class="Tablelike" id="Queues">
                        <li class="Header">[% Translate("Queues") | html %]</li>
                        <li class="FilterMessage Hidden">[% Translate("No matches found.") | html %]</li>
[% RenderBlockStart("Listn1") %]
                        <li><a href="[% Env("Baselink") %]Action=[% Env("Action") %];Subaction=[% Data.Subaction | uri %];ID=[% Data.ID | uri %]" class="AsBlock">[% Data.Name | html %]</a></li>
[% RenderBlockEnd("Listn1") %]
                    </ul>
                </div>
                <div class="Clear"></div>
            </div>
[% RenderBlockEnd("OverviewResult") %]


[% WRAPPER JSOnDocumentComplete %]
<script type="text/javascript">//<![CDATA[
    Core.UI.Table.InitTableFilter($('#FilterUsers'), $('#Users'));
    Core.UI.Table.InitTableFilter($('#FilterQueues'), $('#Queues'));
//]]></script>
[% END %]

[% RenderBlockStart("Change") %]

            <div class="Header">
                <h2>
[% RenderBlockStart("ChangeHeaderQueue") %]
                    [% Translate("Change Queues Relations for Agent") | html %]
[% RenderBlockEnd("ChangeHeaderQueue") %]
[% RenderBlockStart("ChangeHeaderAgent") %]
                    [% Translate("Change Agent Relations for Queues") | html %]
[% RenderBlockEnd("ChangeHeaderAgent") %]
                    <a href="[% Env("Baselink") %]Action=[% Data.ActionHome | uri %];Subaction=Change;ID=[% Data.ID | uri %]">[% Data.Name | html %]</a>
                </h2>
            </div>
            <div class="Content ">
                <form action="[% Env("CGIHandle") %]" method="post" name="matrix">
                    <input type="hidden" name="Action" value="[% Env("Action") %]"/>
                    <input type="hidden" name="Subaction" value="Change[% Data.Type | html %]"/>
                    <input type="hidden" name="ID" value="[% Data.ID | html %]"/>
                    <table class="DataTable VariableWidth" id="UserQueues">
                        <thead>
                            <tr>
                                <th>[% Translate(Data.VisibleNeType) | html %]</th>
[% RenderBlockStart("ChangeHeader") %]
                                <th class="Header">
								   <input type="checkbox" name="Responsiblehead" id="resp_id" title="[% Translate("Toggle permission for all Queues") | html %]" value="" />
                                    [% Translate("Responsible") | html %]
                                </th>
[% WRAPPER JSOnDocumentComplete %]
<script type="text/javascript">//<![CDATA[

   
	
//]]></script>
[% END %]
[% RenderBlockEnd("ChangeHeader") %]
                            </tr>
                        </thead>
                        <tbody>
[% RenderBlockStart("ChangeRow") %]
                            <tr>
                                <td><a href="[% Env("Baselink") %]Action=Admin[% Data.NeType | uri %];Subaction=Change;ID=[% Data.ID | uri %]">[% Data.Name | html %]</a></td>
[% RenderBlockStart("ChangeRowItem") %]
                                <td class="Row">
									<input type="checkbox" name="Responsible" id="[% Data.ID | html %]" title="[% Translate("Toggle permission for %s", Data.Name) | html %]" value="[% Data.ID | html %]" [% Data.Selected %]/>
                             		
[% WRAPPER JSOnDocumentComplete %]
<script type="text/javascript">//<![CDATA[

	$(document).ready(function() {
		$('#resp_id').click(function(event) { 
			if(this.checked) {
				$('#[% Data.ID | html %]').each(function() { 
						if(this.disabled){
							this.checked = false; 	
						}else{
							this.checked=true;
						}			
			});	
			}else{
				$('#[% Data.ID | html %]').each(function() { 
					this.checked = false;   				
				});  
				
			}
		});
	});
		if ('[% Data.Disable | html %]'=='true') {
			$('#[% Data.ID | html %]').prop('disabled', true);
			$('#[% Data.ID | html %]').prop('title','Responsible for [% Data.Name | html %] is user [% Data.ResponsibleUser | html %] ');
		}
            
		if('[% Data.Subaction | html %]'=='Queue'){
            $('input[type="checkbox"]').click(function() {
				if ($(this).is(':checked')) {
					$('input[type="checkbox"]').prop('checked', false);
					$(this).prop('checked', true);
				}
            });    
			$('#resp_id').hide();
		}		
			
            
//]]></script>
[% END %]		
							  </td>	
						</tr>
[% RenderBlockEnd("ChangeRowItem") %]
                            
[% RenderBlockEnd("ChangeRow") %]
                        </tbody>
                    </table>
                    <div class="Field SpacingTop">
                        <button class="Primary CallForAction" type="submit" value="[% Translate("Submit") | html %]"><span>[% Translate("Submit") | html %]</span></button>
                        [% Translate("or") | html %]
                        <a href="[% Env("Baselink") %]Action=[% Env("Action") %]">[% Translate("Cancel") | html %]</a>
                    </div>
                    <div class="Clear"></div>
                </form>
            </div>
[% RenderBlockEnd("Change") %]

        </div>

[% WRAPPER JSOnDocumentComplete %]
<script type="text/javascript">//<![CDATA[
    Core.UI.Table.InitTableFilter($('#Filter'), $('#UserQueues'));
//]]></script>
[% END %]

    </div>
    <div class="Clear"></div>
</div>
[% RenderBlockEnd("Overview") %]
