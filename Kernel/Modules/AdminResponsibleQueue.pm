# --
# Copyright (C) 2001-2014 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::Modules::AdminResponsibleQueue;

use strict;
use warnings;

our $ObjectManagerDisabled = 1;

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {%Param};
    bless( $Self, $Type );

    return $Self;
}

sub Run {
    my ( $Self, %Param ) = @_;
    
    my $ParamObject = $Kernel::OM->Get('Kernel::System::Web::Request');
    my $QueueObject = $Kernel::OM->Get('Kernel::System::Queue');
    my $UserObject = $Kernel::OM->Get('Kernel::System::User');
    my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
    my $LayoutObject = $Kernel::OM->Get('Kernel::Output::HTML::Layout');

    # ------------------------------------------------------------ #
    # user <-> queue 1:n
    # ------------------------------------------------------------ #
    if ( $Self->{Subaction} eq 'User' ) {

        # get user data
        my $UserID = $ParamObject->GetParam( Param => 'ID' );
        my %UserData = $UserObject->GetUserData( UserID => $UserID );
		#get queue data.
		my %QueueData = $QueueObject->GetAllQueues();
	
        my $Output = $LayoutObject->Header();
        $Output .= $LayoutObject->NavigationBar();
        $Output .= $Self->_Change(
            Data => \%QueueData,
            ID   => $UserData{UserID},
            Name => "$UserData{UserFirstname} $UserData{UserLastname} ($UserData{UserLogin})",
            Type => 'User',
        );
        $Output .= $LayoutObject->Footer();
        return $Output;
    }
	
    # ------------------------------------------------------------ #
    # queue <-> user n:1
    # ------------------------------------------------------------ #
    elsif ( $Self->{Subaction} eq 'Queue' ) {

	
		#get queue data
		my $IDQueue = $ParamObject->GetParam( Param => 'ID' );
		my %QueueData = $QueueObject->GetAllQueues();
		
        # get user list
        my %UserData = $UserObject->UserList( Valid => 1 );
		my $Disable=' ';
        # get user name
        for my $UserID ( sort keys %UserData ) {
            my $Name = $UserObject->UserName( UserID => $UserID );
            next if !$Name;
            $UserData{$UserID} .= " ($Name)";
        }
		
        my $QueueName = $QueueObject->QueueLookup( QueueID => $IDQueue );
            
        my $Output = $LayoutObject->Header();
        $Output .= $LayoutObject->NavigationBar();
        $Output .= $Self->_Change(
            Data => \%UserData,
            ID   => $IDQueue,
			Name  => $QueueName,
            Type => 'Queue',
        );
        $Output .= $LayoutObject->Footer();
        return $Output;
    }
	
    # ------------------------------------------------------------ #
    # add user to queues
    # ------------------------------------------------------------ #
    elsif ( $Self->{Subaction} eq 'ChangeQueue' ) {

        # challenge token check for write action
        $LayoutObject->ChallengeTokenCheck();
		
		my $Responsible = 'Responsible';
		

        my $QueueID = $ParamObject->GetParam( Param => 'ID' );
		
        my @IDs = $ParamObject->GetArray( Param => $Responsible );
         
		$DBObject->Do( 	SQL => "delete from responsible_queue where id_queue='".$QueueID."' "
								);
		if($IDs[0]!=0)
		{		
		$DBObject->Do( 	SQL => "INSERT INTO responsible_queue (id_agent,id_queue) 
											VALUES ('".$IDs[0]."',
													'".$QueueID."')",			
								);
		}
        return $LayoutObject->Redirect( OP => "Action=$Self->{Action}" );
    }

    # ------------------------------------------------------------ #
    # queues to user
    # ------------------------------------------------------------ #
    elsif ( $Self->{Subaction} eq 'ChangeUser' ) {

        # challenge token check for write action
        $LayoutObject->ChallengeTokenCheck();

        my $IDUser = $ParamObject->GetParam( Param => 'ID' );
		
		my $Responsible = 'Responsible';
		
        my @IDs = $ParamObject->GetArray( Param => $Responsible );
		
		$DBObject->Do( 	SQL => "Delete from responsible_queue where id_agent='".$IDUser."' "		
								);
		foreach my $IDKol (@IDs)
		{
			$DBObject->Do( 	SQL => "INSERT INTO responsible_queue (id_agent,id_queue) 
												VALUES ('".$IDUser."',
														'".$IDKol."')",			
									);
		}					
		
        return $LayoutObject->Redirect( OP => "Action=$Self->{Action}" );
    }

    # ------------------------------------------------------------ #
    # overview
    # ------------------------------------------------------------ #
    my $Output = $LayoutObject->Header();
    $Output .= $LayoutObject->NavigationBar();
    $Output .= $Self->_Overview();
    $Output .= $LayoutObject->Footer();
    return $Output;
}

sub _Change {
    my ( $Self, %Param ) = @_;

    my $ParamObject = $Kernel::OM->Get('Kernel::System::Web::Request');
    my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
    my $LayoutObject = $Kernel::OM->Get('Kernel::Output::HTML::Layout');
    
    my %Data   = %{ $Param{Data} };
    my $Type   = $Param{Type} || 'User';
	my $QueueID = $Param{IDQueue};
    my $NeType = $Type eq 'Queue' ? 'User' : 'Queue';
	my $Responsible = 'Responsible';
	my $Disable = ' ';
	my $ResponsibleUser;
	my %QueueData = $Kernel::OM->Get('Kernel::System::Queue')->GetAllQueues();
	
    my %VisibleType = ( Queue => 'Queue', User => 'Agent', );

    $LayoutObject->Block( Name => 'Overview' );
    $LayoutObject->Block( Name => 'ActionList' );
    $LayoutObject->Block( Name => 'ActionOverview' );
    $LayoutObject->Block(
        Name => 'Change',
        Data => {
            %Param,
            ActionHome    => 'Admin'.$Type,
            NeType        => $NeType,
            VisibleType   => $VisibleType{$Type},
            VisibleNeType => $VisibleType{$NeType},
        },
    );

    $LayoutObject->Block( Name => "ChangeHeader$VisibleType{$NeType}" );
	
       
        $LayoutObject->Block(
            Name => 'ChangeHeader',
            Data => {
                %Param,
                Type => $Type,
            },
        );
    

			my %ListaKolejek;
		my $Queue;
		my $User;
		for my $IDQueue (sort keys %QueueData) {
			my $ResultAsArrayUser = $DBObject->SelectAll(
			SQL   => "SELECT id_agent, id_queue FROM responsible_queue WHERE id_queue = ? ",
			Bind   => [ \$IDQueue],
			);
			foreach my $TempID (@{$ResultAsArrayUser}){
						$ListaKolejek{@{$TempID}[1]}=@{$TempID}[0];
			}
		 }

    for my $ID ( sort { uc( $Data{$a} ) cmp uc( $Data{$b} ) } keys %Data ) {
	
        # set output class
        $LayoutObject->Block(
            Name => 'ChangeRow',
            Data => {
                %Param,
                Name   => $Param{Data}->{$ID},
                ID     => $ID,
                NeType => $NeType,
            },
        );
		#ustawienie ID checkbox
		# $Responsible='nr'.$ID;
		# zaznaczenie checkbox dla odpowiednich kolejek
		my $Subaction = $ParamObject->GetParam( Param => 'Subaction' );
		my $UserID = $ParamObject->GetParam( Param => 'ID' );	
		my $QueueID = $ParamObject->GetParam( Param => 'ID' );
		my $Selected = ' ';
		my $Disable = 'false';
		
		if($Subaction eq 'User'){
			for my $TempID ( sort keys %ListaKolejek){
				if($TempID == $ID &&  $ListaKolejek{$TempID}!=$UserID){
						$Disable = 'true';	
						 my %UserData = $Kernel::OM->Get('Kernel::System::User')->GetUserData( UserID => $ListaKolejek{$TempID} );
						$ResponsibleUser=$UserData{UserLogin};		
				}
				elsif($TempID == $ID &&  $ListaKolejek{$TempID}==$UserID){
					$Selected = ' checked="checked"';
				}	
			}
		}
						
		#zaznaczenie checkbox dla odpowienich agentów
		if($Subaction eq 'Queue'){
			my $ResultAsArrayUser = $DBObject->SelectAll(
			SQL   => "SELECT id_agent FROM responsible_queue WHERE id_queue = ? ",
			Bind   => [ \$QueueID],
			);
			foreach my $TempID(@{$ResultAsArrayUser}){
				foreach my $UserID (@{$TempID}){
					if($UserID==$ID){
						$Selected = ' checked="checked"';
					}
				}
			}
		}		
        $LayoutObject->Block(
        Name => 'ChangeRowItem',
        Data => {
                %Param,
                Type => $Type,
				Responsible     => $Responsible,
				ID       => $ID,
				Subaction => $Subaction,
                Selected => $Selected,
				Disable =>  $Disable,
				ResponsibleUser => $ResponsibleUser,
                Name     => $Param{Data}->{$ID},
                },
            );
    }

    return $LayoutObject->Output(
        TemplateFile => 'AdminResponsibleQueue',
        Data         => \%Param,
    );
}

sub _Overview {
    my ( $Self, %Param ) = @_;

    my $UserObject = $Kernel::OM->Get('Kernel::System::User');
    my $LayoutObject = $Kernel::OM->Get('Kernel::Output::HTML::Layout');
    
    $LayoutObject->Block( Name => 'Overview' );

    # no "actions list" block because no actions now
    $LayoutObject->Block( Name => 'ActionList' );

    $LayoutObject->Block( Name => 'UserFilter' );
    $LayoutObject->Block( Name => 'QueueFilter' );
    $LayoutObject->Block( Name => 'OverviewResult' );

    # get user list
    my %UserData = $UserObject->UserList( Valid => 1 );

    # get user name
    for my $UserID ( sort keys %UserData ) {
        my $Name = $UserObject->UserName( UserID => $UserID );
        next if !$Name;
        $UserData{$UserID} .= " ($Name)";
    }
    for my $UserID ( sort { uc( $UserData{$a} ) cmp uc( $UserData{$b} ) } keys %UserData ) {

        # set output class
        $LayoutObject->Block(
            Name => 'List1n',
            Data => {
                Name      => $UserData{$UserID},
                Subaction => 'User',
                ID        => $UserID,
            },
        );
    }

	#get group data
	my %QueueData = $Kernel::OM->Get('Kernel::System::Queue')->GetAllQueues();

	#get queue data
    for my $QueueID ( sort { uc( $QueueData{$a} ) cmp uc( $QueueData{$b} ) } keys %QueueData ) {

        # set output class
        $LayoutObject->Block(
            Name => 'Listn1',
            Data => {
                Name      => $QueueData{$QueueID},
                Subaction => 'Queue',
                ID        => $QueueID,
            },
        );
    }
	
    # return output
    return $LayoutObject->Output(
        TemplateFile => 'AdminResponsibleQueue',
        Data         => \%Param,
    );
}

1;
