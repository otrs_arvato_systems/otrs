package Kernel::GenericInterface::Invoker::SCM::TicketUpdate;

use strict;
use warnings;

use Kernel::System::VariableCheck qw(IsString IsStringWithData);

our @ObjectDependencies = (
    'Kernel::Config',
    'Kernel::System::Ticket',
	'Kernel::System::Time',
	'Kernel::System::Email',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    # check needed params
    if ( !$Param{DebuggerObject} ) {
        return {
            Success      => 0,
            ErrorMessage => "Got no DebuggerObject!"
        };
    }

    $Self->{DebuggerObject} = $Param{DebuggerObject};

    return $Self;
}


sub PrepareRequest {
    my ( $Self, %Param ) = @_;
	
	# Get Ticketdata
	my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
		TicketID => $Param{Data}->{TicketID},
		DynamicFields => 1,
		UserID => 1,
	);

        return {
            Success => 1,
            StopCommunication => 1,
        } if !$Ticket{DynamicField_ExternalTicketnumber};

	return {
            Success => 1,
            StopCommunication => 1,
        } if (!($Ticket{DynamicField_ExternalTicketnumber} =~ /^201/));

	return {
            Success => 1,
            StopCommunication => 1
        } if ($Ticket{ChangeBy} eq 350);	

	return {
            Success => 1,
            StopCommunication => 1
        } if (!($Ticket{CustomerID} eq 'arvato_D10' || $Ticket{CustomerID} eq 'Pharma'));
	
	# Set up data for SOAP call
	my %Data = (
		UserLogin => 'webservice',
		Password => 'webservice',
		TicketNumber => $Ticket{DynamicField_ExternalTicketnumber},
		Ticket => {
			Title => $Ticket{Title},
			#Queue => $Ticket{Queue},
			Type => $Ticket{Type},
			State => $Ticket{State},
			Priority => $Ticket{Priority},
		},
	);
		
		
	# ---
	# Pending Status
	# ---
	
	if ($Ticket{RealTillTimeNotUsed}) {
	
	    my ($Sec, $Min, $Hour, $Day, $Month, $Year, $WeekDay) = $Kernel::OM->Get('Kernel::System::Time')->SystemTime2Date(
        	SystemTime => $Ticket{RealTillTimeNotUsed},
	    );
	
	    my %PendingTime = (
   	    	Year => $Year,
   	    	Month => $Month,
        	Day => $Day,
        	Hour => $Hour,
        	Minute => $Sec,
    	);
		
	    $Data{Ticket}{PendingTime} = \%PendingTime;
	
	}
	
	# ---
	# Add DynamicFields
	# ---

	my @DynamicFields;
	
	push @DynamicFields, {
		Name => 'ExternalTicketnumber',
		Value => $Ticket{TicketNumber},
	},
	
	$Data{DynamicField} = \@DynamicFields if @DynamicFields;
	
	
	# ---
	# Article
	# ---
	
	if ($Param{Data}->{ArticleID}) {
		my %Article = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleGet(
			ArticleID => $Param{Data}->{ArticleID},
			TicketID => $Param{Data}->{TicketID},
			UserID => 1,
		);

		if (!($Article{ArticleType} =~ /internal/)) {

			$Article{ArticleType} = "note-external" if $Article{ArticleType} =~ /note/;

			my $MimeType = $Article{MimeType} || "text/html";
			my $Charset = $Article{Charset} || "ISO-8859-15";

			$Charset = "utf8" if  $Charset eq "utf-8";
	
			$Data{Article} = {
				ArticleType => $Article{ArticleType},
				SenderType => $Article{SenderType},
				From => $Article{From},
				Subject => $Article{Subject},
				Body => $Article{Body} || 'empty',
				MimeType => $MimeType,
				Charset => $Charset,
			};

			# ---
			# Add Attachments
			# ---

			my %Index = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleAttachmentIndex(
				ArticleID => $Param{Data}->{ArticleID},
				UserID => 1,
			);
			
			my @Attachments;
	
			foreach my $AttachmentID (keys %Index) {

				my %Attachment = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleAttachment(
					ArticleID => $Param{Data}->{ArticleID},
					FileID => $AttachmentID, 
					UserID => 1,
				);

				$Attachment{Content} = MIME::Base64::encode_base64($Attachment{Content});
	
				push @Attachments, {
					Content => $Attachment{Content},
					ContentType => 'application/octet-stream', # $Attachment{ContentType},
					Filename => $Attachment{Filename},
					ContentAlternative => $Attachment{ContentAlternative},
					Disposition => $Attachment{Disposition},
				};
			
			}
		
			$Data{Attachment} = \@Attachments if @Attachments;
		}
	}

    return {
        Success => 1,
        Data    => \%Data,
    };
}


sub HandleResponse {
    my ( $Self, %Param ) = @_;
	
    # if there was an error in the response, forward it
    if ( !$Param{ResponseSuccess} ) {
	
        return {
            Success      => 0,
            ErrorMessage => $Param{ResponseErrorMessage},
        };
    }

    my $Data = $Param{Data};

    if ( defined $Data->{Error}->{ErrorMessage} ) {

        return {
            Success => 0,
            ErrorMessage => $Param{Data},
        };

    }

    return {
        Success => 1,
        Data    => $Param{Data},
    };
}

1;
