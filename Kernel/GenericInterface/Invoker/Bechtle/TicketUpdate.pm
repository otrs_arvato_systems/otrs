package Kernel::GenericInterface::Invoker::Bechtle::TicketUpdate;

use strict;
use warnings;

use Kernel::System::VariableCheck qw(IsString IsStringWithData);

our @ObjectDependencies = (
    'Kernel::Config',
    'Kernel::System::Ticket',
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    # check needed params
    if ( !$Param{DebuggerObject} ) {
        return {
            Success      => 0,
            ErrorMessage => "Got no DebuggerObject!"
        };
    }

    $Self->{DebuggerObject} = $Param{DebuggerObject};

    return $Self;
}


sub PrepareRequest {
    my ( $Self, %Param ) = @_;
	
	# Get Ticketdata
	my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
		TicketID => $Param{Data}->{TicketID},
		DynamicFields => 1,
		UserID => 1,
	);

        return {
            Success => 1,
            StopCommunication => 1,
        } if !$Ticket{DynamicField_ExternalTicketnumber};

	return {
            Success => 1,
            StopCommunication => 1
        } if $Ticket{CustomerID} ne "105095";

# Temporary - Only send if customer user is BechtleInterfaceUser
	return {
            Success => 1,
            StopCommunication => 1
	} if $Ticket{CustomerUserID} ne "BechtleInterfaceUser";

	return {
            Success => 1,
            StopCommunication => 1
        } if $Ticket{ChangeBy} eq 1;

        my %MasterTicket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
                TicketID => $Param{Data}->{MainTicketID},
                DynamicFields => 1,
                UserID => 1,
        ) if $Param{Data}->{MainTicketID};
	
	# Loading Config
	my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Interface::Bechtle");
	
	# Set up data for SOAP call
	my %Data = (
		CustomerUserLogin => $Config->{User},
		Password => $Config->{Password},
		TicketNumber => $Ticket{DynamicField_ExternalTicketnumber},
		Ticket => {
			Title => $Ticket{Title},
			Queue => $Ticket{Queue},
			Type => $Ticket{Type},
			State => $Ticket{State},
			Priority => $Ticket{Priority},
		},
	);
		
		
	# ---
	# Pending Status
	# ---
	
	
	# ---
	# Add DynamicFields
	# ---

	my @DynamicFields;
	
	push @DynamicFields, {
		Name => 'ExternalTicketnumber',
		Value => $Ticket{TicketNumber},
	};
	
	push @DynamicFields, {
		Name => 'CI',
		Value => $Ticket{DynamicField_CI},
	} if $Ticket{DynamicField_CI};
	
	push @DynamicFields, {
		Name => 'Category',
		Value => $Ticket{DynamicField_Category},
	} if $Ticket{DynamicField_Category};
	
	$Data{DynamicField} = \@DynamicFields if @DynamicFields;
	
	
	# ---
	# Article
	# ---
	
	if ($Param{Data}->{ArticleID}) {
		my %Article = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleGet(
			ArticleID => $Param{Data}->{ArticleID},
			TicketID => $Param{Data}->{TicketID},
			UserID => 1,
		);

		if (!($Article{ArticleType} =~ /internal/)) { 

			my $MimeType = $Article{MimeType} || "text/html";
			my $Charset = $Article{Charset} || "ISO-8859-15";

			$Charset = "utf8" if  $Charset eq "utf-8";
	
			$Data{Article} = {
				From => $Article{From},
				Subject => $Article{Subject},
				Body => $Article{Body},
				MimeType => $MimeType,
				Charset => $Charset,
			};

			# ---
			# Add Attachments
			# ---

			my %Index = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleAttachmentIndex(
				ArticleID => $Param{Data}->{ArticleID},
				UserID => 1,
			);
		
			my @Attachments;

			foreach my $AttachmentID (keys %Index) {

				my %Attachment = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleAttachment(
					ArticleID => $Param{Data}->{ArticleID},
					FileID => $AttachmentID,   # as returned by ArticleAttachmentIndex
					UserID => 1,
				);
	
				$Attachment{Content} = MIME::Base64::encode_base64($Attachment{Content});
				# $Attachment{Filename} =~ s/\n//g;
				# $Attachment{ContentType} .= '; name="'.$Attachment{Filename}.'"; charset="iso-8859-15"';
	
				push @Attachments, {
					Content => $Attachment{Content},
					ContentType => $Attachment{ContentType},
					Filename => $Attachment{Filename},
				};
			
			}
		
			$Data{Attachment} = \@Attachments if @Attachments;
		}
	}

	$Data{Article} = {
            From => 'otrs@arvato-systems.de',
            Subject => 'Ticket merged',
            Body => "Master Ticket: $MasterTicket{DynamicField_ExternalTicketnumber} (OTRS Ticketnumber $MasterTicket{TicketNumber})",
            MimeType => 'text/html',
            Charset => 'utf8',
        } if $Param{Data}->{MainTicketID};


	if ($Param{Data}->{ArticleID} && !(exists $Data{Article})) {
            return {
                Success => 1,
                StopCommunication => 1
            };
	}

    return {
        Success => 1,
        Data    => \%Data,
    };
}

sub HandleResponse {
    my ( $Self, %Param ) = @_;

    # if there was an error in the response, forward it
    if ( !$Param{ResponseSuccess} ) {
        return {
            Success      => 0,
            ErrorMessage => $Param{ResponseErrorMessage},
        };
    }

    return {
        Success => 1,
        Data    => $Param{Data},
    };
}

1;
