# --
# Kernel/GenericInterface/Mapping/Simple.pm - GenericInterface simple data mapping backend
# Copyright (C) 2001-2015 OTRS AG, http://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (AGPL). If you
# did not receive this file, see http://www.gnu.org/licenses/agpl.txt.
# --

package Kernel::GenericInterface::Mapping::Z_OTRSValueMapping;

use strict;
use warnings;

use Kernel::System::VariableCheck qw(IsHashRefWithData IsString IsStringWithData);

our $ObjectManagerDisabled = 1;

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    # check needed params
    for my $Needed (qw(DebuggerObject MappingConfig)) {
        if ( !$Param{$Needed} ) {
            return {
                Success      => 0,
                ErrorMessage => "Got no $Needed!"
            };
        }
        $Self->{$Needed} = $Param{$Needed};
    }

    # check mapping config
    if ( !IsHashRefWithData( $Param{MappingConfig} ) ) {
        return $Self->{DebuggerObject}->Error(
            Summary => 'Got no MappingConfig as hash ref with content!',
        );
    }

    # check config - if we have a map config, it has to be a non-empty hash ref
    if (
        defined $Param{MappingConfig}->{Config}
        && !IsHashRefWithData( $Param{MappingConfig}->{Config} )
        )
    {
        return $Self->{DebuggerObject}->Error(
            Summary => 'Got MappingConfig with Data, but Data is no hash ref with content!',
        );
    }

    # check configuration
    my $ConfigCheck = $Self->_ConfigCheck( Config => $Self->{MappingConfig}->{Config} );
    return $ConfigCheck if !$ConfigCheck->{Success};

    return $Self;
}

sub Map {
    my ( $Self, %Param ) = @_;

    # check data - only accept undef or hash ref
    if ( defined $Param{Data} && ref $Param{Data} ne 'HASH' ) {
        return $Self->{DebuggerObject}->Error(
            Summary => 'Got Data but it is not a hash ref in Mapping Simple backend!'
        );
    }

    # return if data is empty
    if ( !defined $Param{Data} || !%{ $Param{Data} } ) {
        return {
            Success => 1,
            Data    => {},
        };
    }

    # prepare short config variable
    my $Config = $Self->{MappingConfig}->{Config};

    # no config means we just return input data
    if ( !$Config ) {
        return {
            Success => 1,
            Data    => $Param{Data},
        };
    }
	
	my %ReturnData = %{$Param{Data}};
	
	foreach my $ConfigKey ( sort keys %{ $Config->{KeyMapExact} } ) {
	
		my $ValueMapping = $Config->{ValueMap}->{$ConfigKey}->{ValueMapExact};	

		if ($Param{Data}->{$ConfigKey}) {
			if ( $ValueMapping->{ $Param{Data}->{$ConfigKey} } ) {
				$ReturnData{$ConfigKey} = $ValueMapping->{$Param{Data}->{$ConfigKey}};			
			}
		}
		
		if ($Param{Data}->{Ticket}->{$ConfigKey}) {
			if ( $ValueMapping->{ $Param{Data}->{Ticket}->{$ConfigKey} } ) {
				$ReturnData{Ticket}{$ConfigKey} = $ValueMapping->{$Param{Data}->{Ticket}->{$ConfigKey}};		
			}
		}
		
		if ($Param{Data}->{Article}->{$ConfigKey}) {
			if ( $ValueMapping->{ $Param{Data}->{Article}->{$ConfigKey} } ) {
				$ReturnData{Article}{$ConfigKey} = $ValueMapping->{$Param{Data}->{Article}->{$ConfigKey}};
			}
		}
	
	}

    return {
        Success => 1,
        Data    => \%ReturnData,
    };
}



sub _ConfigCheck {
    my ( $Self, %Param ) = @_;

    # just return success if config is undefined or empty hashref
    my $Config = $Param{Config};
    if ( !defined $Config ) {
        return {
            Success => 1,
        };
    }
    if ( ref $Config ne 'HASH' ) {
        return $Self->{DebuggerObject}->Error(
            Summary => 'Config is defined but not a hash reference!',
        );
    }
    if ( !IsHashRefWithData($Config) ) {
        return {
            Success => 1,
        };
    }

    # parse config options for validity
    my %OnlyStringConfigTypes = (
        KeyMapExact     => 1,
        KeyMapRegEx     => 0,
        KeyMapDefault   => 0,
        ValueMapDefault => 0,
    );
    my %RequiredConfigTypes = (
        KeyMapDefault   => 0,
        ValueMapDefault => 0,
    );
    CONFIGTYPE:
    for my $ConfigType (qw(KeyMapExact KeyMapRegEx KeyMapDefault ValueMap ValueMapDefault)) {

        # require some types
        if ( !defined $Config->{$ConfigType} ) {
            next CONFIGTYPE if !$RequiredConfigTypes{$ConfigType};
            return $Self->{DebuggerObject}->Error(
                Summary => "Got no $ConfigType, but it is required!",
            );
        }

        # check type definition
        if ( !IsHashRefWithData( $Config->{$ConfigType} ) ) {
           return $Self->{DebuggerObject}->Error(
               Summary => "Got $ConfigType with Data, but Data is no hash ref with content!",
           );
        }

        # check keys and values of these config types
        next CONFIGTYPE if !$OnlyStringConfigTypes{$ConfigType};
        for my $ConfigKey ( sort keys %{ $Config->{$ConfigType} } ) {
            if ( !IsString($ConfigKey) ) {
                return $Self->{DebuggerObject}->Error(
                    Summary => "Got key in $ConfigType which is not a string!",
                );
            }
            if ( !IsString( $Config->{$ConfigType}->{$ConfigKey} ) ) {
                return $Self->{DebuggerObject}->Error(
                    Summary => "Got value for $ConfigKey in $ConfigType which is not a string!",
                );
            }
        }
    }

    # check default configuration in KeyMapDefault and ValueMapDefault
    my %ValidMapTypes = (
        Keep   => 1,
        Ignore => 0,
        MapTo  => 0,
    );
	
    CONFIGTYPE:
    for my $ConfigType (qw(KeyMapDefault ValueMapDefault)) {

        # require MapType as a string with a valid value
        if (
            !IsStringWithData( $Config->{$ConfigType}->{MapType} )
            || !$ValidMapTypes{ $Config->{$ConfigType}->{MapType} }
            )
        {
            return $Self->{DebuggerObject}->Error(
                Summary => "Got no valid MapType in $ConfigType!",
            );
        }

        # check MapTo if MapType is set to 'MapTo'
        if (
            $Config->{$ConfigType}->{MapType} eq 'MapTo'
            && !IsStringWithData( $Config->{$ConfigType}->{MapTo} )
            )
        {
            return $Self->{DebuggerObject}->Error(
                Summary => "Got MapType 'MapTo', but MapTo value is not valid in $ConfigType!",
            );
        }
    }

    # check ValueMap
    for my $KeyName ( sort keys %{ $Config->{ValueMap} } ) {

        # require values to be hash ref
        if ( !IsHashRefWithData( $Config->{ValueMap}->{$KeyName} ) ) {
            return $Self->{DebuggerObject}->Error(
                Summary => "Got $KeyName in ValueMap, but it is no hash ref with content!",
            );
        }

        # possible sub-values are ValueMapExact or ValueMapRegEx and need to be hash ref if defined
        SUBKEY:
        for my $SubKeyName (qw(ValueMapExact ValueMapRegEx)) {
            my $ValueMapType = $Config->{ValueMap}->{$KeyName}->{$SubKeyName};
            next SUBKEY if !defined $ValueMapType;
            if ( !IsHashRefWithData($ValueMapType) ) {
                return $Self->{DebuggerObject}->Error(
                    Summary =>
                        "Got $SubKeyName in $KeyName in ValueMap,"
                        . ' but it is no hash ref with content!',
                );
            }

            # key/value pairs of ValueMapExact and ValueMapRegEx must be strings
            for my $ValueMapTypeKey ( sort keys %{$ValueMapType} ) {
                if ( !IsString($ValueMapTypeKey) ) {
                    return $Self->{DebuggerObject}->Error(
                        Summary =>
                            "Got key in $SubKeyName in $KeyName in ValueMap which is not a string!",
                    );
                }
                if ( !IsString( $ValueMapType->{$ValueMapTypeKey} ) ) {
                    return $Self->{DebuggerObject}->Error(
                        Summary =>
                            "Got value for $ValueMapTypeKey in $SubKeyName in $KeyName in ValueMap"
                            . ' which is not a string!',
                    );
                }
            }
        }
    }

    # if we arrive here, all checks were OK
    return {
        Success => 1,
    };
}

1;