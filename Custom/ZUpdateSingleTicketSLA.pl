#!/usr/bin/perl -w

use strict;
use warnings;
use Data::Dumper;

use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";

use Kernel::System::ObjectManager;
local $Kernel::OM = Kernel::System::ObjectManager->new();

# get objects
my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
my $DynamicFieldObject = $Kernel::OM->Get('Kernel::System::DynamicField');
my $BackendObject = $Kernel::OM->Get('Kernel::System::DynamicField::Backend');

if(!$ARGV[0]) {
        print "TicketID not found";exit;
}

my $TicketID = $ARGV[0];

my %Ticket = $TicketObject->TicketGet(
	TicketID      => $TicketID,
	UserID        => 1,
	DynamicFields => 1,
);

my $CustomerID = $Ticket{CustomerID};

if (!$Kernel::OM->Get('Kernel::Config')->Get('SLA::'.$CustomerID)) {
        print "CustomerID has no SLAs configured\n";exit;
}
my $Config = $Kernel::OM->Get('Kernel::Config')->Get('SLA::'.$CustomerID);

if ($Config) {
    $Ticket{Type} = 'Unclassified' if !$Ticket{Type};
    $Ticket{Priority} = '3 - medium' if !$Ticket{Priority};

    #if (!$Ticket{Service}) {
    if (!$Ticket{Service} || $Ticket{Service} eq 'undefined') {
        #my $Success = $TicketObject->TicketServiceSet(
        #Service  => 'undefined',
        #TicketID => $TicketID,
        #UserID   => 1,
        #);
        #$Ticket{Service} = 'undefined';
	print "No service defined";
        exit;
    }

    my %ConfigLine;
    my $SLA;
    foreach my $Key (keys %{$Config}) {
        %ConfigLine = map{split /\=/, $_}(split /\|/, $Key);

        $SLA = $Config->{$Key};
        #print "Current SLA: $SLA\n";
        foreach my $Key2 (keys %ConfigLine) {
	    next if !$ConfigLine{$Key2};
            next if $ConfigLine{$Key2} eq 'Any';;
            next if !$Ticket{$Key2};
            undef $SLA if ($ConfigLine{$Key2} ne $Ticket{$Key2});
        }
        undef %ConfigLine;
        my $Success = $TicketObject->TicketSLASet(
            SLA      => "$SLA",
            TicketID => $TicketID,
            UserID   => 1,
        ) if defined $SLA;
        print "Ticket $TicketID updated with SLA: $SLA\n" if defined $SLA;
        last if defined $SLA;
    }

    print "No SLA found for ticket $TicketID\n" if !$SLA;

    local @ARGV = ("$TicketID");
    do "/opt/otrs/Custom/ZSetSLATimesToDynamicField.pl" if $Ticket{State} eq "closed";
}
1;

