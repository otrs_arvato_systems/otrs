#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);
my $P_TicketID = $ARGV[0];
my $P_ArticleID = $ARGV[1];

if(!$P_TicketID || !$P_ArticleID)
{
    print "ArticleID or TicketID not found";
    exit;
}

# Get email object
my $EmailObject = $Kernel::OM->Get('Kernel::System::Email');
my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
#$EmailObject->Send(
#		#From => 'otrs@arvato-systems.de',
#		From => 'marc.blomberg@bertelsmann.de',
#		To => 'service-cc@arvato-systems.de, khaishaun.ng@bertelsmann.de',
#		Subject => 'Test email from OTRS Prod',
#		Charset => 'iso-8859-15',
#		#Charset => 'utf-8',
#		MimeType => 'text/plain',
#		Body => 'Test content',
#);

print "Deleting article $P_ArticleID from ticket $P_TicketID...\n";

my $Success = $TicketObject->ArticleDelete(
	TicketID  => $P_TicketID,
	ArticleID => $P_ArticleID,
	UserID    => 1,
);

if($Success)
{
	print "Deleted successfully. Ticket: $P_TicketID, Article: $P_ArticleID\n";
}
