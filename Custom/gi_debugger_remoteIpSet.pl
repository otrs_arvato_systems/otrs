#!/usr/bin/perl

use strict;
use warnings;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";

use Kernel::System::ObjectManager;
local $Kernel::OM = Kernel::System::ObjectManager->new();

use Data::Dumper;

my $tmp = $Kernel::OM->Get('Kernel::System::DB')->Do(
    SQL     => 	'UPDATE gi_debugger_entry JOIN gi_debugger_entry_content ON gi_debugger_entry.id = gi_debugger_entry_content.gi_debugger_entry_id
		SET gi_debugger_entry.remote_ip="outgoing message"
	       	WHERE gi_debugger_entry_content.subject = "XML data sent to remote system"'
);



my $IDs;

$tmp = $Kernel::OM->Get('Kernel::System::DB')->Prepare(
    SQL   => 'SELECT DISTINCT gi_debugger_entry_id
              FROM gi_debugger_entry_content
              WHERE subject = "XML data sent to remote system"'
);

while (my @Row = $Kernel::OM->Get('Kernel::System::DB')->FetchrowArray()) {
    $IDs .= "$Row[0], ";
}


$tmp = $Kernel::OM->Get('Kernel::System::DB')->Prepare(
    SQL   => 'SELECT id
              FROM gi_debugger_entry
              WHERE communication_type <> "Requester"'
);

while (my @Row = $Kernel::OM->Get('Kernel::System::DB')->FetchrowArray()) {
    $IDs .= "$Row[0], ";
}


chop($IDs);
chop($IDs);

print "$IDs\n";

$tmp = $Kernel::OM->Get('Kernel::System::DB')->Do(
    SQL => "DELETE FROM gi_debugger_entry_content
            WHERE gi_debugger_entry_id NOT IN ($IDs)"
);

$tmp = $Kernel::OM->Get('Kernel::System::DB')->Do(
    SQL => 'DELETE FROM gi_debugger_entry
            WHERE id NOT IN (
                SELECT DISTINCT gi_debugger_entry_id
                FROM gi_debugger_entry_content
            )'
);
