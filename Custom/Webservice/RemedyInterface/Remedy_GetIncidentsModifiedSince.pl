#!/usr/bin/perl -w

use strict;
use warnings;

no warnings 'once';

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
my $ConfigObject = $Kernel::OM->Get('Kernel::Config');

my $CustomerID;
if (defined $ARGV[0] ) {
   $CustomerID = $ARGV[0];
} else {
   my $tmp = $Webservice->ErrorHandling(
        ErrorMessage => "Remedy_GetIncidentsModifiedSince called without CustomerID",
   );
   exit;
}

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$CustomerID."::General");

foreach my $Key (keys %$ConfigCustomer) {
	$Config->{$Key} = $ConfigCustomer->{$Key}
}

# Check if the required Config settings are set
if ( !$Config->{User}
	|| !$Config->{Password}
	|| !$Config->{RemedyQueue}
	|| !$Config->{DispatchingQueue}
	|| !$Config->{WebserviceUserID}
	) {

	$Webservice->ErrorHandling(
		ErrorMessage => "Config for Customer $CustomerID is incomplete",
	);
	exit;
}

# prepare the XMLData for the SOAP Request
my $XMLData =
"<login><![CDATA[$Config->{User}]]></login>
<password><![CDATA[$Config->{Password}]]></password>
";

my $Operation = 'GetIncidentsModifiedSinceRequest';
# my $Operation = 'GetIncidentsNotClosedRequest';
# my $Operation = 'GetAllIncidents';
$XMLData .= "<seconds>500</seconds>"; 

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{Proxy},
	URI => $Config->{URI},
	Operation => $Operation,
	XMLData => $XMLData,
);

# In case of a faulty response, call errorhandling
if ($Result->{Fault}) {

	my $Dump = Dumper($Result);
	$Dump  =~ s/\$VAR1 = \'\n\s*//g;
	$Dump =~ s/\n\s*';\n//g;

	my $tmp = $Webservice->ErrorHandling(
		ErrorMessage => "Error:\n Code: $Result->{Fault}->{faultcode}\n String: $Result->{Fault}->{faultstring}\n$Dump",
	);

} else {

	my $RemedyTickets = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
		Result => $Result,
		Operation => $Operation
	);

 # If there is only one Ticket that was changed, the format of TicketList has to be changed to an Arrayref
	if (ref $RemedyTickets eq 'HASH') {
		print "one Ticket only\n";
		my $tmp = $RemedyTickets;
		undef $RemedyTickets;
		$RemedyTickets->[0] = $tmp;
	}

	foreach my $RemedyTicket (@{$RemedyTickets}) {

		print "___\nRemedy Ticket: $RemedyTicket->{incidentID}\n";

		next if $RemedyTicket->{customerName} eq 'Tivoli';

		next if $RemedyTicket->{debitorNo} ne $CustomerID;
		
		next if $RemedyTicket->{incidentID} =~ /(IM\d\d\d\d\d\d\d\d\d+)/;

		my $OTRSTicketID = $Remedy_Webservice_Subs->GetOTRSTicketID(
			RemedyTicket => $RemedyTicket,
			Config => $Config,
		);	

		if (!$OTRSTicketID) {

			my $CustomerUser = $Remedy_Webservice_Subs->GetCustomerUser(
			    RemedyTicket => $RemedyTicket,
			    Config => $Config,
			);

			# Shaun - 2017/2/13 - Update queue to Monitoring whenever receives Remedy ticket from X_I_OTRS-AMS_Monitoring-Only group.
			my $Queue = $Config->{DispatchingQueue};
			my $OwnerID = 1;
			my $ResponsibleID = 1;
			my $Priority = $ConfigObject->Get("Webservice::Remedy::PriorityMapping")->{$RemedyTicket->{priority}};
			my $State = $ConfigObject->Get("Webservice::Remedy::StateMapping")->{$RemedyTicket->{status}};
			
			# Shaun - 2017/2/27 - Get Ticket type and create OTRS ticket with the type
			my $Type = $ConfigObject->Get("Webservice::Remedy::TypeMapping")->{$RemedyTicket->{ticketType}};
			
			if( $RemedyTicket->{currentGroup} eq 'X_I_OTRS-AMS_Monitoring-Only' ) {
				$Queue = 'Monitoring';
				$OwnerID = 420; # ID 420 = MATE028
				$ResponsibleID = 420; # ID 420 = MATE028
				#$Type = 'Incident';
			}

			my $TicketID = $TicketObject->TicketCreate(
			    Title        => $RemedyTicket->{shortDescription},
			    Queue	 => $Queue,	    
			    Lock         => 'unlock',
			    Priority     => $Priority,
			    State        => $State,
			    Type	 => $Type || 'Unclassified',
			    CustomerID   => $RemedyTicket->{debitorNo} || 'internal',
			    CustomerUser => $CustomerUser || 'Internal_arvato',
			    OwnerID      => $OwnerID,
			    ResponsibleID=> $ResponsibleID,
			    UserID       => $Config->{WebserviceUserID},
			);
			print "DEBUG: Ticket created: $TicketID\n";
			$OTRSTicketID = $TicketID;

			my $Body;
			my $value;
			foreach my $name (sort keys %{$RemedyTicket}) {
				if (defined $RemedyTicket->{$name}) {
						$value = $RemedyTicket->{$name};
				} else {
						$value = " ";
				}
				$Body .= sprintf("%-30s %s\n", $name, $value);
			}
			
			my $ArticleID = $TicketObject->ArticleCreate(
			    TicketID => $OTRSTicketID,
			    ArticleType => 'note-initial',                        
			    SenderType => 'agent',                                
			    From => 'ticket@arvato-systems.de',       
			    To => 'otrs@arvato-systems.de', 
			    Subject => $RemedyTicket->{shortDescription},               
			    Body => $Body,                  
			    ContentType => 'text/plain; charset=ISO-8859-15',      
			    HistoryType => 'AddNote',
			    HistoryComment => 'Initial note added via Interface',
			    UserID => $Config->{WebserviceUserID},
			);
			print "DEBUG: Article created: $ArticleID\n";
		

		    my %OTRSTicket = $TicketObject->TicketGet(
			TicketID      => $OTRSTicketID,
			DynamicFields => 1,
			UserID        => 1,
		    );

		    $Remedy_Webservice_Subs->UpdateRemedyTicketNumber(
			Config => $Config,
			RemedyTicket => $RemedyTicket,
			OTRSTicket => \%OTRSTicket
		    );
		
		    local @ARGV = ("$OTRSTicketID");
		    do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_SetExternalKey.pl" if !$RemedyTicket->{externalKey};			

                    $Remedy_Webservice_Subs->UpdateRemedyExternalKey(
                        Config => $Config,
                        RemedyTicket => $RemedyTicket,
                        OTRSTicket => \%OTRSTicket
                    ) if $RemedyTicket->{externalKey};

		    local @ARGV = ("$OTRSTicketID");
		    do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_GetDetails.pl";

                } else {
	
		    #-------------------------------------------#
		    # If there is a ticket -> update the ticket #
		    #-------------------------------------------#

		    print "Updating Ticket: $OTRSTicketID\n";
		    print "Remedy ticket Misc: $RemedyTicket->{misc}\n" if $RemedyTicket->{misc};
	
		    my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
			TicketID => $OTRSTicketID,
			DynamicFields => 1,
			UserID => 1,
		    );
		
		    # We only update OTRS ticket is it belongs in a valid queue
		    next if !($Config->{RemedyQueue} =~ /(^$Ticket{Queue}$)|(^$Ticket{Queue}\;)|(\;$Ticket{Queue}$)|(\;$Ticket{Queue}\;)/);
	
		    print "DEBUG: Remedy Ticket: $RemedyTicket->{incidentID}, OTRSTicketID: $Ticket{TicketID}\n";
		    print "Remedy OTRS Referenz: $RemedyTicket->{externalKey}\n" if $RemedyTicket->{externalKey};

                    $Remedy_Webservice_Subs->UpdateRemedyExternalKey(
                        Config => $Config,
                        RemedyTicket => $RemedyTicket,
                        OTRSTicket => \%Ticket
                    ) if $RemedyTicket->{externalKey};

                    local @ARGV = ("$OTRSTicketID");
                    do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_SetExternalKey.pl" if !$RemedyTicket->{externalKey};

		    print "DEBUG: ExternalKey done!\n";

		    $Remedy_Webservice_Subs->UpdateTicketType(
			Config => $Config,
			RemedyTicket => $RemedyTicket,
			OTRSTicket => \%Ticket
		    );

		    print "DEBUG: Type ...done!\n";

		    $Remedy_Webservice_Subs->UpdateTicketPriority(
			Config => $Config,
			RemedyTicket => $RemedyTicket,
			OTRSTicket => \%Ticket
		    );

		    print "DEBUG: Priority ...done!\n";
			
		    local @ARGV = ("$Ticket{TicketID}");
                    do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_GetDetails.pl";

                    print "DEBUG: Details ...done!\n";

		    $Remedy_Webservice_Subs->UpdateTicketState(
			Config => $Config,
			RemedyTicket => $RemedyTicket,
			OTRSTicket => \%Ticket
		    );

	            print "DEBUG: State ...done!\n";
		
		    #local @ARGV = ("$Ticket{TicketID}");
		    #do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_GetDetails.pl";

		}
		
	}
}
