#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";

use Kernel::System::ObjectManager;
# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Webservice arvato Systems',
    },
);

my $CustomerID;
if (defined $ARGV[0] ) {
   $CustomerID = $ARGV[0];
} else {
   die "UpdateOTRSTickets called without CustomerID";
}

my @TicketIDs = $Kernel::OM->Get('Kernel::System::Ticket')->TicketSearch(
	Result => 'ARRAY',
	CustomerID => $ARGV[0],
        # Queues => [ $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$CustomerID."::General")->{RemedyQueue} ],
	StateType => [ 'new', 'open', 'pending reminder' ], 
	DynamicField_RemedyTicketNumber => {
		Like              => 'INC*',  
	},
	UserID => 1,
	OrderBy => 'Down',
	ArchiveFlags => ['n'],
);

foreach my $TicketID (@TicketIDs) {

	my $Warning;
	my $File = "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_GetIncidentByIncidentID.pl";
        #unless (my $return = do $File) {
        #    $Warning .= "couldn't parse $File: $@" if $@;
        #    $Warning .= "couldn't do $File: $!" unless defined $return;
        #    $Warning .= "couldn't run $File" unless $return;
        #}
	print "WARNING: $Warning\n" if $Warning;
	local @ARGV = ($TicketID);
	do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_GetIncidentByIncidentID.pl";
	print "Remedy_GetIncidentByIncidentID completed for Ticket $TicketID\n";
}
