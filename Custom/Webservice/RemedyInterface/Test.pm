#!/usr/bin/perl -w

use strict;
use warnings;

#print "Hello World\n";
use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $tmp = $Webservice->ErrorHandling(
     ErrorMessage => '$soap->transport->status = 500 Internal Server Error
Fault: = $VAR1 = {
          detail => {
                      hostname => degtluv3114
                    },
          faultcode => soapenv:Server.userException,
          faultstring => ERROR (10000): ; Missing entry in at one or more fields for  \'Customer\'\'s Name\', \'Customer\'\'s Phone\', \'Company\' OR \'Short Description\'. 
        };

<?xml version="1.0" encoding="utf-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><soapenv:Fault><faultcode>soapenv:Server.userException</faultcode><faultstring>ERROR (10000): ; Missing entry in at one or more fields for  \'Customer\'\'s Name\', \'Customer\'\'s Phone\', \'Company\' OR \'Short Description\'. </faultstring><detail><ns1:hostname xmlns:ns1="http://xml.apache.org/axis/">degtluv3114</ns1:hostname></detail></soapenv:Fault></soapenv:Body></soapenv:Envelope>;

Request = $VAR1 = <?xml version="1.0" encoding="UTF-8"?><soap:Envelope soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:namesp4="WS_CreateUpdate_IncidentService" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soap:Header>
<AuthenticationInfo>
        <userName>WSSAPLL_P</userName>
        <password><![CDATA[WSSAPLL_P]]></password>
</AuthenticationInfo>
</soap:Header><soap:Body><namesp4:UpdateIncident RemedyWebservice="Yes" UpdateExternalKey="Yes" xmlns:namesp4="WS_CreateUpdate_IncidentService"><Details><![CDATA[Ticket wird geschlossen, da bereits das Ticket 100151347 fÃÂŒr diese Anforderung erÃÂ¶ffnet ist.

GruÃÂ

Christof Kowol
]]></Details>
<Status>Resolved</Status>
<Priority>Low</Priority>
<TicketID>INC04388718</TicketID>
<externalKey>100151780</externalKey><IncidentSolution><![CDATA[Solution logged in OTRS]]></IncidentSolution> <IncidentCause><![CDATA[Cause logged in OTRS]]></IncidentCause> </namesp4:UpdateIncident></soap:Body></soap:Envelope>;'
);
