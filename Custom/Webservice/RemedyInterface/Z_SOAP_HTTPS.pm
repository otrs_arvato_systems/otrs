package Z_SOAP_HTTPS;
 
use strict;
use warnings;

use File::Path;
use utf8;
use Encode ();
use MIME::Base64;

use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";

our @ObjectDependencies = (
    'Kernel::Config',
    'Kernel::System::Log',
    'Kernel::System::Email',
    'Kernel::System::Ticket',
);

use Data::Dumper;
#use SOAP::Lite on_fault => sub 
# { my($soap1, $res1) = @_;
#    if(defined $res1) {
#	my $ErrorMessage = '$soap->transport->status = '.$soap1->transport->status;
#	$ErrorMessage .= "\n\nResponse = ".Dumper($soap1->transport()->proxy()->http_response()->content());
#        $ErrorMessage .= "\nRequest = ".Dumper($soap1->transport()->proxy()->http_response()->request()->content());
#	$ErrorMessage =~ s/\$VAR1 = \'\n\s*//g;
#	$ErrorMessage =~ s/\n\s*';\n//g;
#	ErrorHandling( {}, ErrorMessage => $ErrorMessage);
#    }
# };
use SOAP::Lite +trace => [ transport => sub { print $_[0]->as_string } ];

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );
	
    return $Self;
}

sub WSDLRequest {
	
	my ( $Self, %Param ) = @_;
	
	if ( !$Param{Proxy}
		|| !$Param{URI}
		||	!$Param{Operation}
		|| (			!$Param{XMLHeader}
				&&	!$Param{XMLData}
			)
	  )
	{
		return "Error: wrong Parameters!\n";
	}

	#print $Param{URI}, $Param{Operation}, "\n";
	print "Proxy:\n $Param{Proxy} \n";	
        #my $User     = 'OTRS-TEST';
        #my $Password = 'Summer28!';
	#my $URL = '';
	#$URL = $Param{Proxy};
        #$URL = $Param{Proxy} =~ s{ ( http s? :// ) }{$1$User:$Password@}xmsi;
	#my $httpsURL = 'https://';
	#$Param{Proxy} =~ s/$httpsURL/$httpsURL$User:$Password@/g;
	#print "After auth: \n$Param{Proxy}";
	
	my $Operation = $Param{Operation};
	my $SOAPHeader = SOAP::Header->type( 'xml' => $Param{XMLHeader} );
	my $SOAPData = SOAP::Data->type( 'xml' => $Param{XMLData} );
	my @Params = ($SOAPData, $SOAPHeader);
	
	$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 1;
	my $SOAPAction = "http://sap.com/xi/WebService/soap1.1";
	my $Soap = SOAP::Lite->new(uri => $Param{URI}, proxy => $Param{Proxy});
	#$Soap->service("file:/WORK/DUMP/neum032/SI_GetIncidentByIncidentID_Request_OUTService.wsdl");
	
	#$Soap->ns("$Param{URI}", 'urn');
	my $authoriz = 'Basic '.encode_base64('OTRS-TEST:Summer28!');
        $Soap->transport->http_request->headers->push_header('Authorization' => $authoriz);
	my $Log = $Param{XMLData};
        $Log =~ s/\n/ /g;
	#$Soap->readable(1);
	$Kernel::OM->Get('Kernel::System::Log')->Log(
            Priority => 'info',
            Message  => "[RemedyInterface] Operation: $Operation, Data: $Log"
        );

	if ($Param{Proxy} =~ /safir/) {
		$Soap->call(($Operation) => @Params);
	} elsif($Param{Proxy} =~ /pi-dev/) {
		$Soap->call(SOAP::Data->name($Operation)->uri( $Param{URI} ) => @Params);
	} else {
		if ($Operation eq 'CreateIncident') {
			$Soap->call(SOAP::Data->name($Operation)->uri( $Param{URI} )->attr({RemedyWebservice => 'Yes', SubmitTicket => => 'Yes' }) => @Params);
		} elsif ($Operation =~ /Update/) {
			$Soap->call(SOAP::Data->name($Operation)->uri( $Param{URI} )->attr({RemedyWebservice => 'Yes', UpdateExternalKey => 'Yes' }) => @Params);
		} else {
			$Soap->call(SOAP::Data->name($Operation)->uri( $Param{URI} ) => @Params);
		}
	}

	$Soap -> print();
	#print "Dumper transport http_response: ".Dumper($Soap->transport->http_response)."\n";
	#print "Dumper transport: ".Dumper($Soap->transport)."\n";
	
        my $XMLRequest = $Soap->transport->http_request->as_string;
        #print $XMLRequest;

	my $XMLResponse = $Soap->transport->http_response->content;
	print "\n\n".Dumper($XMLResponse);

	my $Deserialized = eval { SOAP::Deserializer->deserialize($XMLResponse); };
		
	my $Body;

	if($Deserialized) {
		$Body = $Deserialized->body();
	}

	if (!$Body) {
		return "Error: empty Response"
	} else {
		return $Body;
	}	
}

sub ErrorHandling {

	my ( $Self, %Param ) = @_;
	
	if (!$Param{ErrorMessage}) {
		return 0;
	}
	
	my $Body;
	foreach my $Key (keys %Param) {
		if (defined $Param{$Key}) {
			$Body .= "$Key: $Param{$Key}\n";
		}
	}
	
	print $Body, "\n";
	
	# Get email object
	my $EmailObject = $Kernel::OM->Get('Kernel::System::Email');

	my $Sent = $EmailObject->Send(
		From => 'otrs@arvato-systems.de',
		To => 'khaishaun.ng@bertelsmann.de', 
		Cc => 'jonas.neumann@bertelsmann.de',
		Subject => 'OTRS - Error in Webservice',
		Charset => 'iso-8859-15',
		#Charset => 'utf-8',
		MimeType => 'text/plain',
		Body => $Body,
	);
	#my $Sent = 1;


	if( length($Param{ErrorMessage}) >= 100 ) {

		my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

		#ID 524 = NGKH001 (Shaun Ng)
	        my $OwnerID = 1;
	        my $ResponsibleID = 1;

	        my $TicketID = $TicketObject->TicketCreate(
			Title        => 'OTRS - Error in Webservice',
			Queue        => 'Tools::OTRS::System Errors',
		        Lock         => 'unlock',
		  	Priority     => '3 - medium',
			State        => 'new',
			CustomerID   => 'internal',
			CustomerUser => 'Internal_arvato',
			OwnerID      => $OwnerID,
			ResponsibleID=> $ResponsibleID,
			UserID       => 1,
       		);
		
		if (defined $TicketID) {
			my $ArticleID = $TicketObject->ArticleCreate(
				TicketID => $TicketID,
				ArticleType => 'note-external',
				SenderType => 'system',
				From => 'Internal_arvato',
				To => 'otrs@arvato-systems.de',
				Subject => 'OTRS - Error in Webservice',
				Body => $Body,
				Charset => 'ISO-8859-15',
				MimeType => 'text/plain',
				HistoryType => 'AddNote',
				HistoryComment => 'Created from Webservice',
				UserID => 1,
			);	
		} 
        	print "DEBUG: Ticket created: $TicketID\n";
	}


	return 1;
}

1;
