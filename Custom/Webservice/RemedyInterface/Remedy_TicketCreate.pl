#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $LogObject = $Kernel::OM->Get('Kernel::System::Log');

if (!$ARGV[0]) {
    $LogObject->Log(
        Priority => 'error',
        Message  => "RemedyTicketCreate called without TicketID",
    );
	die "RemedyTicketCreate called without TicketID";
}

my $ParamTicketID = $ARGV[0];
#$LogObject->Log(
#        Priority => 'error',
#        Message => "TEST - Remedy Ticket Create is called for ID: $ParamTicketID"
#);


# get ticket object
my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

my %Ticket = $TicketObject->TicketGet(
	TicketID => $ParamTicketID,
	UserID => 1,
	DynamicFields => 1,
);

my %Article = $TicketObject->ArticleFirstArticle(
        TicketID      => $ParamTicketID,
        DynamicFields => 0,     # 0 or 1, see ArticleGet()
);

# get config object
my $ConfigObject = $Kernel::OM->Get('Kernel::Config');

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

foreach my $Key (keys %$ConfigCustomer) {
	$Config->{$Key} = $ConfigCustomer->{$Key}
}

# Check if the required Config settings are set
if ( !$Config->{User}
	|| !$Config->{Password}
	|| !$Config->{RemedyQueue}
	|| !$Config->{DispatchingQueue}
	|| !$Config->{WebserviceUserID}
	) {

	$Webservice->ErrorHandling(
		ErrorMessage => "Config for Customer $Ticket{CustomerID} is incomplete",
	);
	exit;
}

my $TicketType = $ConfigObject->Get("Webservice::Remedy::TypeMapping")->{$Ticket{'Type'}};

$TicketType =~ s/_/ /g;

my $Priority = $ConfigObject->Get("Webservice::Remedy::PriorityMapping")->{$Ticket{'Priority'}};
my $Status = $ConfigObject->Get("Webservice::Remedy::PMS::StateMapping")->{$Ticket{'State'}};

if ($Status eq 'Closed' || $Status eq 'closed') {
    # print "Status Closed will be set by the UHD\n";
    exit 1;
}

my $Company = $ConfigObject->Get("Webservice::".$Ticket{'CustomerID'}."::General")->{$Ticket{'CustomerID'}};

# get customeruser object
my $CustomerUserObject = $Kernel::OM->Get('Kernel::System::CustomerUser');
my %CustomerUser = $CustomerUserObject->CustomerUserDataGet(
	User => $Ticket{'CustomerUserID'},
);

my $CustomerPhone = $CustomerUser{'UserPhone'};
$CustomerPhone = '+49524180' if (!$CustomerPhone); 

my $Operation = 'CreateIncident';
my $XMLHeader = "
<AuthenticationInfo>
        <userName>$Config->{User}</userName>
        <password><![CDATA[$Config->{Password}]]></password>
</AuthenticationInfo>
";

#$LogObject->Log(
#        Priority => 'error',
#        Message => "TEST - Remedy Ticket Create is called for ID: $ParamTicketID with customer user $CustomerUser{'UserEmail'}"
#);

$CustomerUser{'UserEmail'} = '' if $CustomerUser{'UserEmail'} =~ /localhost/;
$CustomerUser{'UserEmail'} = '' if $CustomerUser{'UserEmail'} eq 'otrs@arvato-systems.de';

# Setting CustomerUser email to empty string
$CustomerUser{'UserEmail'} = '' if $CustomerUser{'UserEmail'} eq 'csm-send@lekkerland.com';

my $ShortDescription = substr($Ticket{Title}, 0, 119);
my $Details = substr($Article{Body}, 0, 4999);

my $XMLData = 
"<shortDescription><![CDATA[$ShortDescription]]></shortDescription>
<externalKey>$Ticket{TicketNumber}</externalKey>
<ticketType>$TicketType</ticketType>
<details><![CDATA[$Details]]></details>
<firstName><![CDATA[$CustomerUser{'UserFirstname'}]]></firstName>
<lastName><![CDATA[$CustomerUser{'UserLastname'}]]></lastName>
<customerPhone><![CDATA[$CustomerPhone]]></customerPhone>
<customerMail><![CDATA[$CustomerUser{'UserEmail'}]]></customerMail>
<company><![CDATA[$Company]]></company>
";

$XMLData .= "\n<incidentType>$Ticket{Queue}</incidentType>" if ($Ticket{Queue} =~ /User helpdesk/);

$XMLData .= "\n<priority><![CDATA[$Priority]]></priority>";

if (defined $Ticket{DynamicField_Application}[0]) {
	$Ticket{DynamicField_Application}[0] =~ s/^$Ticket{CustomerID} //g;
	$XMLData .= "\n<applicationName>$Ticket{DynamicField_Application}[0]</applicationName>";
}

# print "$XMLData\n";
# exit 1;
# print  "$WSDL\n$Proxy\n$Operation\n$XMLHeader\n$XMLData\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
        Proxy => $Config->{PMS_Proxy},
        URI => $Config->{PMS_URI},
        Operation => $Operation,
        XMLHeader => $XMLHeader,
        XMLData => $XMLData,
);


if ($Result->{Fault}) {
	# print "Error:\n Code:    $Result->{Fault}->{faultcode}\n String:  $Result->{Fault}->{faultstring}\n"
} else {
	# print Dumper($Result->{$Operation."Response"});
	my $LogObject = $Kernel::OM->Get('Kernel::System::Log');

	# print Dumper($Result);

	my $RemedyTicket = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
            Result => $Result,
            Operation => $Operation,
        );

	$RemedyTicket->{incidentID} = $RemedyTicket->{TicketID} if (!$RemedyTicket->{incidentID} && $RemedyTicket->{TicketID});
	
	# print "DEBUG: Ticket Created ...done!\n", Dumper($RemedyTicket);
		
	$Remedy_Webservice_Subs->UpdateRemedyTicketNumber(
		Config => $Config,
		RemedyTicket => $RemedyTicket,
		OTRSTicket => \%Ticket
	);

	local @ARGV = ("$ParamTicketID");
	do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_GetTicketByExternalKey.pl";	
	
	# print "DEBUG: RemedyTicketNumber ...done!\n";
	my %Article = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleGet(
		TicketID => $ParamTicketID,
		ArticleID => $Article{ArticleID},
		UserID => 1,
	);

	$Article{Subject} = "$RemedyTicket->{TicketID} $Article{Subject}";

	delete $Article{From};
	delete $Article{To};
	delete $Article{Cc};

	my $Sent = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleSend(
	    TicketID => $ParamTicketID,
	    From => 'otrs@arvato-systems.de',
	    To => 'ticket@arvato-systems.de',
	    UserID => $Config->{WebserviceUserID},
	    HistoryType => 'Forward', 
	    HistoryComment => 'Forwarded to Remedy',
	    %Article,
	) if $RemedyTicket->{TicketID};

	local @ARGV = ("$ParamTicketID", "$Article{ArticleID}");
	do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_AddAttachment.pl";
}

