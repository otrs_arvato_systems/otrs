#!/usr/bin/perl -w

use strict;
use warnings;

no warnings 'once';

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $LogObject = $Kernel::OM->Get('Kernel::System::Log');

my $P_TicketID;
if (defined $ARGV[0] ) {
   $P_TicketID = $ARGV[0];
} else {
   my $tmp = $Webservice->ErrorHandling(
        ErrorMessage => "Remedy_SetExternalKey.pl called without TicketID",
   );
   exit;
}

my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
	TicketID => $P_TicketID,
	DynamicFields => 1,
	UserID => 1,
);

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
#my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

#foreach my $Key (keys %$ConfigCustomer) {
#	$Config->{$Key} = $ConfigCustomer->{$Key}
#}

# Check if the required Config settings are set
if ( !$Config->{User}
	|| !$Config->{Password}
	|| !$Config->{RemedyQueue}
	# || !$Config->{DispatchingQueue}
	|| !$Config->{WebserviceUserID}
	) {

	$Webservice->ErrorHandling(
		ErrorMessage => "Remedy Config is incomplete",
	);
	exit;
}

my $DynamicField = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
    ID   => $Config->{DynamicField_ExternalKey_ID},
);


if($Ticket{'DynamicField_'.$DynamicField->{Name}})
{
	#print "External key found $Ticket{'DynamicField_'.$DynamicField->{Name}}. No external key is updated.\n";
	exit 1;
}

#exit 1 if $Ticket{'DynamicField_'.$DynamicField->{Name}};

# my $XMLHeader = "
# <AuthenticationInfo>
	# <userName><![CDATA[$Config->{User}]]></userName>
	# <password><![CDATA[$Config->{Password}]]></password>
# </AuthenticationInfo>
# ";


my $XMLHeader = "
<AuthenticationInfo>
        <userName>$Config->{User}</userName>
        <password><![CDATA[$Config->{Password}]]></password>
</AuthenticationInfo>
";


# my $XMLHeader = "
# <AuthenticationInfo>
#	<userName><![CDATA[WSTNGSAP]]></userName>
#	<password><![CDATA[WSTNGSAP]]></password>
# </AuthenticationInfo>
# ";

$DynamicField = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
	ID   => $Config->{DynamicField_RemedyTicketNumber_ID},
);

my $RemedyTicketNumber = $Ticket{'DynamicField_'.$DynamicField->{Name}};

exit 1 if !$RemedyTicketNumber;

# prepare the XMLData for the SOAP Request
my $checkXMLData =
"
<login><![CDATA[$Config->{User}]]></login>
<password><![CDATA[$Config->{Password}]]></password>
<incidentID>$Ticket{'DynamicField_'.$DynamicField->{Name}}</incidentID>
";

my $checkOperation = 'GetIncidentByIncidentIDRequest';

# Call the SOAP Request
my $checkResult = $Webservice->WSDLRequest(
        Proxy => $Config->{Proxy},
        URI => $Config->{URI},
        Operation => $checkOperation,
        XMLHeader => '',
    XMLData => $checkXMLData,
);

#print Dumper($checkResult);

if($checkResult->{Fault})
{

}
else
{
	my $RemedyTicket = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
            Result => $checkResult,
            Operation => $checkOperation,
        );

# =================== Check if external key exists ===================
	
	if($RemedyTicket->{externalKey})
	{
	        $LogObject->Log(
	                Priority => 'debug',
        	        Message => "Not updating external key for $RemedyTicketNumber. Existing external key $RemedyTicket->{externalKey}."
       		);
		#print "TEST - Not updating external key for $RemedyTicketNumber. Existing external key $RemedyTicket->{externalKey}.";
	}
# =================== End checking ===================
	else {
		# prepare the XMLData for the SOAP Request
		my $XMLData = 
			"<TicketID><![CDATA[$RemedyTicketNumber]]></TicketID>
			<NewExternalKey><![CDATA[$Ticket{TicketNumber}]]></NewExternalKey>
			";

		my $Operation = 'UpdateExternalKey';

		#print $XMLData, "\n";

		# Call the SOAP Request
		my $Result = $Webservice->WSDLRequest(
			Proxy => $Config->{PMS_Proxy},
			URI => $Config->{PMS_URI},
			Operation => $Operation,
			XMLHeader => $XMLHeader,
			XMLData => $XMLData,
		);

		#print Dumper($Result);
		# In case of a faulty response, call errorhandling
		if ($Result->{Fault}) {

			# my $Dump = Dumper($Result);
			# $Dump  =~ s/\$VAR1 = \'\n\s*//g;
			# $Dump =~ s/\n\s*';\n//g;

			# my $tmp = $Webservice->ErrorHandling(
			#	ErrorMessage => "Error:\n Code: $Result->{Fault}->{faultcode}\n String: $Result->{Fault}->{faultstring}\n$Dump",
			# );

		} else {

        		my $DynamicFieldConfig = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
                        	ID   => $Config->{DynamicField_ExternalKey_ID},
        		);

       			my $Success = $Kernel::OM->Get('Kernel::System::DynamicField::Backend')->ValueSet(
                		DynamicFieldConfig => $DynamicFieldConfig,
                		ObjectID => $Ticket{TicketID},
		                Value => $Ticket{TicketNumber},
                		UserID => $Config->{WebserviceUserID},
        		);
			#print "DEBUG: ExternalKey set!\n";
		}
	}
}

return 1;
