#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use OTRS_SyncConfigItem;
my $OTRS_SyncObject = OTRS_SyncConfigItem->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'ServiceDB Webservice',
    },
);

my $XMLHeader = "
<AuthenticationInfo>
        <userName>WS_CMDB_OTRS</userName>
        <password>WS_CMDB_OTRS</password>
</AuthenticationInfo>
";

die "Bitte einen Debitor angeben\n" if !$ARGV[0];

my $Qualification = "'ApplDebitor' = \"$ARGV[0]\" AND 'Status' = \"Valid\""; # "'Application Name' LIKE \"%otto%\"  AND 'Status' = \"Valid\"";
# my $Qualification = "'Application Name' LIKE \"%nma-o%\"  AND 'Status' = \"Valid\"";

my $XMLData =
#"<Qualification>'Application Name' LIKE \"%test%\" AND 'Status' = \"Valid\"</Qualification>
"<Qualification>$Qualification</Qualification>
<startRecord></startRecord>
<maxLimit></maxLimit>
";

print $XMLData;

my $Operation = 'GetList';

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
    # Proxy => 'http://145.228.78.96:23002/midtier_linux/services/ARService?server=pmsinte&webService=WS_ADB_Applications',
    Proxy => 'http://145.228.78.97:23002/midtier_linux/services/ARService?server=pms&webService=WS_ADB_Applications',
    URI => 'WS_ADB_ApplicationsService', 
    Operation => $Operation,
    XMLHeader => $XMLHeader,
    XMLData => $XMLData,
);

if ( defined  $Result->{$Operation."Response"} && defined $Result->{$Operation."Response"}->{getListValues} ) {
    
    # ---
    # Error Handling wenn die Response kein Array ist
    # ---

    my @ApplicationArray = @{ $Result->{$Operation."Response"}->{getListValues} };

    foreach my $Application (@ApplicationArray) {

	# print Dumper($Application);
	# exit 1;
#        print "Synching $Application->{ApplicationName} $Application->{ApplicationID}\n";
#
#        my $SyncResponseCMDB = $OTRS_SyncObject->HandleApplicationCMDB(
#            Application => $Application,
#        );
#
#        if ($SyncResponseCMDB->{Success}) {
#            print "Success! $SyncResponseCMDB->{Message}\n";
#        } else {
#            print "Error! $SyncResponseCMDB->{Message}\n";
#        }


        my $SyncResponseDynamicField = $OTRS_SyncObject->HandleApplicationDynamicField(
            Application => $Application,
        );

        if ($SyncResponseDynamicField->{Success}) {
            print "Success! $SyncResponseDynamicField->{Message}\n";
        } else {
            print "Error! $SyncResponseDynamicField->{Message}\n";
        }

    }

} else {
    print Dumper($Result);
}
