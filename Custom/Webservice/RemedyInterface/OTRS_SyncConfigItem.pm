package OTRS_SyncConfigItem;

use strict;
use warnings;

use utf8;
use Encode ();
use Data::Dumper;

use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";

use Kernel::System::ObjectManager;
# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Webservice arvato Systems (OTRS <-> Remedy)',
    },
);

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

    $Self->{ClassID} = 99;
    
    my %Mapping = (
        SLA => "SLASDC",
        DebitorNo => "Customer",
    );

    $Self->{Mapping} = \%Mapping;

    my %SLAs = (
        Basic => 111,
        Professional => 112,
        Premium => 113,
        No_SLA => 114,
    );

    $Self->{SLAs} = \%SLAs;

    return $Self;
}

sub HandleApplicationDynamicField {
    my ( $Self, %Param ) = @_;

    return {
        Success => 0,
        Message => 'No ClassID set',
    } if !$Self->{ClassID};

    return {
        Success => 0,
        Message => 'No Application found in the parameters',
    } if !$Param{Application};


    my $Key = $Param{Application}->{ApplicationID}." ".$Param{Application}->{DebitorNo};
    my $Value = $Param{Application}->{ApplicationName};
    print "Key = $Key\nValue=$Value\n";

    my $DynamicField = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
        ID => 50, #DynamicField_Application ... The Remedy Application
    );

    # my %PossibleValues = %{$DynamicField->{Config}->{PossibleValues}};
    if ($DynamicField->{Config}->{PossibleValues}->{$Key}) {
  
        return {
            Success => 1,
            Message => 'ConfigItem exists',
        };
  
    } else {

        $DynamicField->{Config}->{PossibleValues}->{$Key} = $Value;

        my $Success = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldUpdate(
            %{$DynamicField},
            Reorder => 0,
            UserID => 1,
        );

        return {
            Success => 1,
            Message => 'ConfigItem created',
        } if $Success eq 1;

    }

    return {
        Success => 0,
        Message => 'some error happend, check the log',
    };

}

sub HandleApplicationCMDB {
    my ( $Self, %Param ) = @_;

    return {
        Success => 0,
        Message => 'No ClassID set',
    } if !$Self->{ClassID};

    return {
        Success => 0,
        Message => 'No Application found in the parameters',
    } if !$Param{Application};

    my $Application = $Param{Application};

    return {
        Success => 0,
        Message => "No SLA Mapping defined for SLA $Application->{SLA}",
    } if !$Self->{SLAs}->{$Application->{SLA}};

    $Application->{DebitorNo} = "internal" if $Application->{DebitorNo} eq "997";
    
    my $ConfigItemObject = $Kernel::OM->Get('Kernel::System::ITSMConfigItem');

    # ConfigItemSearch to identify if the CI exists
    my $Result = $ConfigItemObject->ConfigItemSearch(
        Number => $Application->{ApplicationID},
        UserID  => 1,
    );

    # if a valid ConfigItemID was found update it if necessary, if not create a new config item
    if ( $Result->[0] ) {

        my $ConfigItemID = $Result->[0];

        # get the current version of the ConfigItem
        my $Version = $ConfigItemObject->VersionGet(
            ConfigItemID => $ConfigItemID,
            XMLDataGet => 1,
            UserID  => 1,
        );

        # compare the remedy application to the current version of the otrs config item, if any changes are found the config item will be updated
        my $SameValues = "true";

        $SameValues = "false" if $Version->{Name} ne $Application->{ApplicationName};

        foreach my $Key (keys %{$Self->{Mapping}}) {
            $SameValues = "false" if ( defined $Application->{$Key} ne defined $Version->{XMLData}[1]{Version}[1]{$Self->{Mapping}->{$Key}}[1]->{Content} ); 
        }

        return {
            Success => 1,
            Message => 'ConfigItem and Application are the same',
        } if $SameValues eq "true";

        # get the current definition of the config item, in case there were updates
        my $Definition = $ConfigItemObject->DefinitionGet(
            ClassID => $Self->{ClassID},
            UserID  => 1,
        );

        my $XMLData;
        $XMLData->[1]->{Version}->[1]->{Customer}->[1]->{Content} = $Application->{DebitorNo};
        $XMLData->[1]->{Version}->[1]->{SLASDC}->[1]->{Content} = $Self->{SLAs}->{$Application->{SLA}};
        $XMLData->[1]->{Version}->[1]->{Description}->[1]->{Content} = $Application->{Description} if $Application->{Description};

        print Dumper($XMLData);

        # update config utem
        my $VersionAddResult = $ConfigItemObject->VersionAdd(
            ConfigItemID => $ConfigItemID,
            Name => $Application->{ApplicationName},
            DefinitionID => $Definition->{DefinitionID},
            DeplStateID => 32,   # Production
            InciStateID => 1,   # Operation
            XMLData => $XMLData,
            UserID => 1,
        );

        return {
            Success => 1,
            Message => 'ConfigItem updated',
        };
            
    } else {

        # in case there was no otrs config item found for the remedy application id, create a new config item
        my $ConfigItemID = $ConfigItemObject->ConfigItemAdd(
            Number  => $Application->{ApplicationID},
            ClassID => $Self->{ClassID},
            UserID  => 1,
        );

        # Due to the dynamic configuration a version needs to be added to insert all nessecary information. For that the corrent class definition needs to be loaded
        my $Definition = $ConfigItemObject->DefinitionGet(
            ClassID => $Self->{ClassID},
            UserID  => 1,
        );

        my $XMLData;
        $XMLData->[1]->{Version}->[1]->{Customer}->[1]->{Content} = $Application->{DebitorNo};
        $XMLData->[1]->{Version}->[1]->{SLASDC}->[1]->{Content} = $Self->{SLAs}->{$Application->{SLA}};
        $XMLData->[1]->{Version}->[1]->{Description}->[1]->{Content} = $Application->{Description} if $Application->{Description};

        # update config utem
        my $VersionAddResult = $ConfigItemObject->VersionAdd(
            ConfigItemID => $ConfigItemID,
            Name => $Application->{ApplicationName},
            DefinitionID => $Definition->{DefinitionID},
            DeplStateID => 32,   # Production
            InciStateID => 1,   # Operation 
            XMLData => $XMLData,
            UserID => 1,
        );
        
        return {
            Success => 1,
            Message => 'ConfigItem created',
        };

    }

    return {
        Success => 1,
        Message => 'This should not happen!',
     };
  
}
