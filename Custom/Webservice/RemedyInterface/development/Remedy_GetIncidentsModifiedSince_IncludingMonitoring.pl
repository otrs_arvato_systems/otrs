#!/usr/bin/perl -w

use strict;
use warnings;

no warnings 'once';

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
my $ConfigObject = $Kernel::OM->Get('Kernel::Config');

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");

# Check if the required Config settings are set
if ( !$Config->{User}
	|| !$Config->{Password}
	|| !$Config->{RemedyQueue}
	# || !$Config->{DispatchingQueue}
	|| !$Config->{WebserviceUserID}
	) {

	$Webservice->ErrorHandling(
		ErrorMessage => "Remedy Config is incomplete",
	);
	exit;
}

# prepare the XMLData for the SOAP Request
my $XMLData =
"<login><![CDATA[$Config->{User}]]></login>
<password><![CDATA[$Config->{Password}]]></password>
";

# my $Operation = 'GetIncidentsModifiedSinceRequest';
# my $Operation = 'GetIncidentsNotClosedRequest';
my $Operation = 'GetAllIncidentsRequest';
# $XMLData .= "<seconds>50000</seconds>"; 

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{Proxy},
	URI => $Config->{URI},
	Operation => $Operation,
	XMLData => $XMLData,
);

# In case of a faulty response, call errorhandling
if ($Result->{Fault}) {

	my $Dump = Dumper($Result);
	$Dump  =~ s/\$VAR1 = \'\n\s*//g;
	$Dump =~ s/\n\s*';\n//g;

	my $tmp = $Webservice->ErrorHandling(
		ErrorMessage => "Error:\n Code: $Result->{Fault}->{faultcode}\n String: $Result->{Fault}->{faultstring}\n$Dump",
	);

} else {

	my $RemedyTickets = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
		Result => $Result,
		Operation => $Operation
	);

 # If there is only one Ticket that was changed, the format of TicketList has to be changed to an Arrayref
	if (ref $RemedyTickets eq 'HASH') {
		print "one Ticket only\n";
		my $tmp = $RemedyTickets;
		undef $RemedyTickets;
		$RemedyTickets->[0] = $tmp;
	}

	foreach my $RemedyTicket (@{$RemedyTickets}) {
	
                print $RemedyTicket->{debitorNo}, "\n";
                next;
		# next if ($RemedyTicket->{debitorNo} eq 105103); # TNG
		# next if ($RemedyTicket->{debitorNo} ne 100268); # Aldi Sued
                next if ($RemedyTicket->{debitorNo} ne 100385);
		print "___\nRemedy Ticket: $RemedyTicket->{debitorNo} $RemedyTicket->{incidentID} $RemedyTicket->{shortDescription}\n";

		# next if $RemedyTicket->{customerName} eq 'Tivoli';

		my $OTRSTicketID = $Remedy_Webservice_Subs->GetOTRSTicketID(
			RemedyTicket => $RemedyTicket,
			Config => $Config,
		);	

		if (!$OTRSTicketID) {

			my $CustomerUser = $Remedy_Webservice_Subs->GetCustomerUser(
				 RemedyTicket => $RemedyTicket,
				 Config => $Config,
			);

                        my $Queue = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::GroupMapping")->{$RemedyTicket->{currentGroup}} || "User helpdesk";
			
                        # print "$Queue <-> $RemedyTicket->{currentGroup}\n";
                        # next;

			my $TicketID = $TicketObject->TicketCreate(
				Title        => $RemedyTicket->{shortDescription},
				Queue        => $Queue,
				Lock         => 'unlock',
				Priority     => $ConfigObject->Get("Webservice::Remedy::PriorityMapping")->{$RemedyTicket->{priority}},
				State        => $ConfigObject->Get("Webservice::Remedy::StateMapping")->{$RemedyTicket->{status}},
				CustomerID   => $RemedyTicket->{debitorNo} || 'internal',
				CustomerUser => $CustomerUser || 'Internal_arvato',
				OwnerID      => 1,
				UserID       => $Config->{WebserviceUserID},
			);
			print "DEBUG: Ticket created: $TicketID\n";
			$OTRSTicketID = $TicketID;

			my $Body;
			my $value;
			foreach my $name (sort keys %{$RemedyTicket}) {
				if (defined $RemedyTicket->{$name}) {
						$value = $RemedyTicket->{$name};
				} else {
						$value = " ";
				}
				$Body .= sprintf("%-30s %s\n", $name, $value);
			}
			
			my $ArticleID = $TicketObject->ArticleCreate(
				TicketID => $OTRSTicketID,
				ArticleType => 'note-initial',                        
				SenderType => 'agent',                                
				From => 'ticket@arvato-systems.de',       
				To => 'otrs@arvato-systems.de', 
				Subject => $RemedyTicket->{shortDescription},               
				Body => $Body,                  
				ContentType => 'text/plain; charset=ISO-8859-15',      
				HistoryType => 'AddNote',
				HistoryComment => 'Initial note added via Interface',
				UserID => 1,
			) if $OTRSTicketID;
			print "DEBUG: Article created: $ArticleID\n";
		

		    my %OTRSTicket = $TicketObject->TicketGet(
			TicketID      => $OTRSTicketID,
			DynamicFields => 1,
			UserID        => 1,
		    );

		    $Remedy_Webservice_Subs->UpdateRemedyTicketNumber(
			Config => $Config,
			RemedyTicket => $RemedyTicket,
			OTRSTicket => \%OTRSTicket
		    );
		
                    local @ARGV = ("$OTRSTicketID");
                    do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_SetExternalKey.pl";

                    local @ARGV = ("$OTRSTicketID");
                    do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_GetDetails.pl";


                    die "test\n";

                } else {
	
		    #-------------------------------------------#
		    # If there is a ticket -> update the ticket #
		    #-------------------------------------------#
                    next;

		    print "Updating Ticket: $OTRSTicketID\n";
	
		    my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
			TicketID => $OTRSTicketID,
			DynamicFields => 1,
			UserID => 1,
		    );
	
		    print "DEBUG: Remedy Ticket: $RemedyTicket->{incidentID}, OTRSTicketID: $Ticket{TicketID}, Remedy OTRS Referenz: $RemedyTicket->{externalKey}\n";

                    $Remedy_Webservice_Subs->UpdateQueue(
                        Config => $Config,
                        RemedyTicket => $RemedyTicket,
                        OTRSTicket => \%Ticket
                    );

                    print "DEBUG: Queue ...done!\n";

		    $Remedy_Webservice_Subs->UpdateTicketType(
			Config => $Config,
			RemedyTicket => $RemedyTicket,
			OTRSTicket => \%Ticket
		    );

		    print "DEBUG: Type ...done!\n";

		    $Remedy_Webservice_Subs->UpdateTicketPriority(
			Config => $Config,
			RemedyTicket => $RemedyTicket,
			OTRSTicket => \%Ticket
		    );

		    print "DEBUG: Priority ...done!\n";

		    $Remedy_Webservice_Subs->UpdateTicketState(
			Config => $Config,
			RemedyTicket => $RemedyTicket,
			OTRSTicket => \%Ticket
		    );

	            print "DEBUG: State ...done!\n";
		
		    local @ARGV = ("$Ticket{TicketID}");
		    do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_GetDetails.pl";
		}
		
	}
}
