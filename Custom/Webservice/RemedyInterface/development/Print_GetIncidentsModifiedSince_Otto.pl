#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# prepare the XMLData for the SOAP Request
my $XMLData =
"<login>WSOTTO2</login>
<password><![CDATA[Ott0&AMS]]></password>
";

local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $ConfigObject = $Kernel::OM->Get('Kernel::Config');
my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");

# my $Operation = 'GetAllIncidentsRequest';
my $Operation = 'GetIncidentsModifiedSinceRequest';
$XMLData .= "<seconds>50000</seconds>"; 

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{Proxy},
	URI => $Config->{URI},
	Operation => $Operation,
	XMLData => $XMLData,
);

# print Dumper($Result);

# In case of a faulty response, call errorhandling
if ($Result->{Fault}) {

	my $Dump = Dumper($Result);
	$Dump  =~ s/\$VAR1 = \'\n\s*//g;
	$Dump =~ s/\n\s*';\n//g;

	my $tmp = $Webservice->ErrorHandling(
		ErrorMessage => "Error:\n Code: $Result->{Fault}->{faultcode}\n String: $Result->{Fault}->{faultstring}\n$Dump",
	);

} else {

    print Dumper($Result);
    exit 1;

	my $RemedyTickets = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
		Result => $Result,
		Operation => $Operation
	);

 # If there is only one Ticket that was changed, the format of TicketList has to be changed to an Arrayref
	if (ref $RemedyTickets eq 'HASH') {
		print "one Ticket only\n";
		my $tmp = $RemedyTickets;
		undef $RemedyTickets;
		$RemedyTickets->[0] = $tmp;
	}


	foreach my $RemedyTicket (@{$RemedyTickets}) {

		# print "Remedy Ticket: $RemedyTicket->{incidentID} $RemedyTicket->{debitorNo}, $RemedyTicket->{company}\n";
		print Dumper($RemedyTicket);
		# print "$RemedyTicket->{responsibleGroup};$RemedyTicket->{incidentClass};$RemedyTicket->{incidentType}\n";
		# print "$RemedyTicket->{incidentID}: $RemedyTicket->{externalKey} $RemedyTicket->{status}, $RemedyTicket->{shortDescription}\n";
		
	}
}
