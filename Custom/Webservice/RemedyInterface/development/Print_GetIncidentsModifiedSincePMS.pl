#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $ConfigObject = $Kernel::OM->Get('Kernel::Config');

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");

my $XMLHeader = "
<AuthenticationInfo>
        <userName>WSAMS_P</userName>
        <password>WSAMS_P</password>
</AuthenticationInfo>
";


my $Operation = 'GetIncidentsModifiedSince';
# my $Operation = 'GetIncidentsNotClosedRequest';
# my $Operation = 'GetAllIncidents';
my $XMLData .= "<seconds>500000</seconds>"; 

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{PMS_Proxy},
	URI => $Config->{PMS_URI},
	Operation => $Operation,
        XMLHeader => $XMLHeader,
	XMLData => $XMLData,
);

# print Dumper($Result);

if ($Result) {

	# my $RemedyTickets = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
	#	Result => $Result,
	#	Operation => $Operation
	# );

 # If there is only one Ticket that was changed, the format of TicketList has to be changed to an Arrayref
	# if (ref $RemedyTickets eq 'HASH') {
	#	print "one Ticket only\n";
	#	my $tmp = $RemedyTickets;
	#	undef $RemedyTickets;
	#	$RemedyTickets->[0] = $tmp;
	# }

        my $Debitors = "Debitoren: ";
   
        my @RemedyTickets = @{$Result->{$Operation."Response"}->{getListValues}};
        print "Number of Elements: $#RemedyTickets\n";

        foreach my $RemedyTicket (@RemedyTickets) {

                $Debitors .= "$RemedyTicket->{debitorNo} " if (!($Debitors =~ m/$RemedyTicket->{debitorNo}/));

		# print "___\nRemedy Ticket: $RemedyTicket->{incidentID}\n";
		# print Dumper($RemedyTicket);
		# print "$RemedyTicket->{responsibleGroup};$RemedyTicket->{incidentClass};$RemedyTicket->{incidentType}\n";
		# print "$RemedyTicket->{incidentID}: $RemedyTicket->{externalKey} $RemedyTicket->{status}, $RemedyTicket->{shortDescription}\n";
		
	}
        print $Debitors, "\n";
}
