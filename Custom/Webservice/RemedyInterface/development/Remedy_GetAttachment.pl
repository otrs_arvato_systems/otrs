#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";
use MIME::Base64;
use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

my $P_TicketID;
if (defined $ARGV[0] ) {
   $P_TicketID = $ARGV[0];
} else {
   #my $tmp = $Webservice->ErrorHandling(
   #     ErrorMessage => "Remedy_GetAttachment called without TicketID",
   #);
   print "Remedy_GetAttachment called without TicketID\n";
   exit;
}

my %Ticket = $TicketObject->TicketGet(
        TicketID => $P_TicketID,
        DynamicFields => 1,
        UserID => 1,
);

my $TicketNumber = $Ticket{TicketNumber};

print "CustomerID for ticket $P_TicketID = $Ticket{CustomerID}\n";
print "CustomerUserID for Ticket $P_TicketID = $Ticket{CustomerUserID}\n";

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

foreach my $Key (keys %$ConfigCustomer) {
        $Config->{$Key} = $ConfigCustomer->{$Key}
}

# Check if the required Config settings are set
if ( !$Config->{User}
        || !$Config->{Password}
        || !$Config->{RemedyQueue}
        || !$Config->{DispatchingQueue}
        || !$Config->{WebserviceUserID}
        ) {

        #$Webservice->ErrorHandling(
        #        ErrorMessage => "Config for Customer $Ticket{CustomerID} is incomplete",
        #);
	print "Config for Customer $Ticket{CustomerID} is incomplete";
        exit;
}

my $XMLHeader = "";

my $Operation = 'GetAttachmentRequest';

my $XMLData = "<login>WSSAPLL_P</login>
		<password>WSSAPLL_P</password>
		<externalKey>$TicketNumber</externalKey>
               	<id>1</id>";

my $Result = $Webservice->WSDLRequest(
      		Proxy => $Config->{Proxy},
      		URI => $Config->{URI},
      		Operation => $Operation,
    		XMLHeader => '',
	       	XMLData => $XMLData,
);

#if ($Result->{Fault}) {
#	print "Error:\n Code: $Result->{Fault}->{faultcode}\n String:  $Result->{Fault}->{faultstring}\n";
#	my $tmp = $Webservice->ErrorHandling(
#		ErrorMessage => "Error:\n Code: $Result->{Fault}->{faultcode}\n String: $Result->{Fault}->{faultstring}\n$Dump",
#	);

#}	
print Dumper($Result);

