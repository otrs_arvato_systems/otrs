#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'ServiceDB Webservice',
    },
);

my $XMLHeader = "
<AuthenticationInfo>
        <userName>WS_CMDB_OTRS</userName>
        <password>WS_CMDB_OTRS</password>
</AuthenticationInfo>
";

my $Qualification =  "'ApplDebitor' = \"102989\"";# AND 'Status' = \"Valid\"";
# "'Hostname' = \"gtlnmiwvm1566\" AND 'Status' = \"Valid\""; # "'ApplDebitor' = \"101121\""; # "'Application Name' LIKE \"%otto%\"  AND 'Status' = \"Valid\"";

my $XMLData =
#"<Qualification>'Application Name' LIKE \"%test%\" AND 'Status' = \"Valid\"</Qualification>
"<Qualification>$Qualification</Qualification>
<startRecord>0</startRecord>
<maxLimit>100</maxLimit>
";

print $XMLData;

my $Operation = 'GetList';

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
    Proxy => 'http://145.228.78.96:23002/midtier_linux/services/ARService?server=pmsinte&webService=WS_ADB_Components_All_Get',
    URI => 'WS_ADB_Components_All_GetService', 
    Operation => $Operation,
    XMLHeader => $XMLHeader,
    XMLData => $XMLData,
);

if ( defined  $Result->{$Operation."Response"} && defined $Result->{$Operation."Response"}->{getListValues} ) {
    
    # ---
    # Error Handling wenn die Response kein Array ist
    # ---

    my @ComponentArray = @{ $Result->{$Operation."Response"}->{getListValues} };

    foreach my $Component (@ComponentArray) {
	print Dumper($Component);
        # print "$Component->{Responsible_Group}: $Component->{Hostname}\n";
    }

} else {
    print Dumper($Result);
}
