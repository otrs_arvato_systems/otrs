#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'ServiceDB Webservice',
    },
);

my $XMLHeader = "
<AuthenticationInfo>
        <userName>WS_CMDB_OTRS</userName>
        <password>WS_CMDB_OTRS</password>
</AuthenticationInfo>
";

# my $Qualification = "'Application Name' LIKE \"%test%\" AND 'Status' = \"Valid\"";

my $XMLData = "<ApplicationID>AD219804153700TE1QXQ0YYq8A7obs</ApplicationID>";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
    Proxy => 'http://145.228.78.96:23002/midtier_linux/services/ARService?server=pmsinte&webService=WS_ADB_Applications',
    URI => 'WS_ADB_ApplicationsService', 
    Operation => 'Get',
    XMLHeader => $XMLHeader,
    XMLData => $XMLData,
);

print Dumper($Result);
