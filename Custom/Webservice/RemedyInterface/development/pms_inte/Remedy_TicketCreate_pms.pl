#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $LogObject = $Kernel::OM->Get('Kernel::System::Log');

if (!$ARGV[0]) {
    $LogObject->Log(
        Priority => 'error',
        Message  => "RemedyTicketCreate called without TicketID",
    );
	die "RemedyTicketCreate called without TicketID";
}

my $ParamTicketID = $ARGV[0];
# get ticket object
my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

my %Ticket = $TicketObject->TicketGet(
	TicketID => $ParamTicketID,
	UserID => 1,
	DynamicFields => 1
);

my %Article = $TicketObject->ArticleFirstArticle(
        TicketID      => $ParamTicketID,
        DynamicFields => 0,     # 0 or 1, see ArticleGet()
);

#my %Index = $TicketObject->ArticleAttachmentIndex(
#        ArticleID => $Article{'ArticleID'},
#        UserID    => 1,
#);

#my %Attachment = $TicketObject->ArticleAttachment(
#        ArticleID       => $Article{'ArticleID'},
#	FileID		=> 1,
#        UserID          => 1,
#);

#my %ArticleID = $TicketObject->ArticleSend(
#        TicketID         => $ParamTicketID,
#        ArticleType      => $Article{'ArticleType'},
#        SenderType       => 'system',
#        From             => 'Test agent <otrs@arvato-systems.de>',
#        To               => 'Shaun Ng <khaishaun.ng@bertelsmann.de>',
#        Subject          => 'Test send attachment',
#        Body             => 'Something attached',
#	Charset		 => 'iso-8859-15',
#        MimeType 	 => 'text/plain',
#        ContentType      => 'text/plain; charset=ISO-8859-15',
#        HistoryType      => 'OwnerUpdate',
#        HistoryComment   => 'Some free text!',
#        UserID           => 1,
#	Attachment	 => [
#            {
#                Content     => $Attachment{'Content'},
#                ContentType => $Attachment{'ContentType'},
#                Filename    => $Attachment{'Filename'},
#            },
#	],
#);

# get config object
my $ConfigObject = $Kernel::OM->Get('Kernel::Config');

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

foreach my $Key (keys %$ConfigCustomer) {
	$Config->{$Key} = $ConfigCustomer->{$Key}
}

# Check if the required Config settings are set
if ( !$Config->{RemedyQueue}
	|| !$Config->{DispatchingQueue}
	|| !$Config->{WebserviceUserID}
	) {

	$Webservice->ErrorHandling(
		ErrorMessage => "Config for Customer $Ticket{CustomerID} is incomplete",
	);
	exit;
}

my $TicketType = $ConfigObject->Get("Webservice::Remedy::TypeMapping")->{$Ticket{'Type'}};
$TicketType =~ s/_/ /g;
my $Priority = $ConfigObject->Get("Webservice::Remedy::PriorityMapping")->{$Ticket{'Priority'}};
my $Company = $ConfigObject->Get("Webservice::".$Ticket{'CustomerID'}."::General")->{$Ticket{'CustomerID'}};

# get customeruser object
my $CustomerUserObject = $Kernel::OM->Get('Kernel::System::CustomerUser');
my %CustomerUser = $CustomerUserObject->CustomerUserDataGet(
	User => $Ticket{'CustomerUserID'},
);

my $CustomerPhone = $CustomerUser{'UserPhone'};
$CustomerPhone = '+49524180' if (!$CustomerPhone); 

my $Operation = 'CreateIncident';
my $XMLHeader = "
<AuthenticationInfo>
        <userName>WSTNGSAP_T</userName>
        <password>WSTNGSAP_T</password>
</AuthenticationInfo>
";

$CustomerUser{'UserEmail'} = "" if $CustomerUser{'UserEmail'} =~ /localhost/;

my $ShortDescription = substr($Ticket{Title}, 0, 119);
my $Details = substr($Article{Body}, 0, 4999);

#my $TicketNumber = $Ticket{TicketNumber}; 
# Shaun - For testing in recreating existing tickets in remedy
my $TicketNumber = 9999982;

my $XMLData = 
 "<shortDescription><![CDATA[$ShortDescription]]></shortDescription>
<externalKey>$TicketNumber</externalKey>
<ticketType>$TicketType</ticketType>
<details><![CDATA[$Details]]></details>
<firstName><![CDATA[$CustomerUser{'UserFirstname'}]]></firstName>
<lastName><![CDATA[$CustomerUser{'UserLastname'}]]></lastName>
<customerPhone><![CDATA[$CustomerPhone]]></customerPhone>
<customerMail><![CDATA[$CustomerUser{'UserEmail'}]]></customerMail>
<company><![CDATA[$Company]]></company>";

# $XMLData .= "<incidentType>$Ticket{Queue}</incidentType>" if ($Ticket{Queue} =~ /User helpdesk/);

# ---
# TEST
# ---
$Ticket{Queue} = $ARGV[1] if $ARGV[1];
# ---
# TEST
# ---

$XMLData .= "\n<incidentType>$Ticket{Queue}</incidentType>";



$XMLData .= "\n<priority><![CDATA[$Priority]]></priority>";

if (defined $Ticket{DynamicField_Application}) {
	$XMLData .= "\n<applicationName><![CDATA[$Ticket{DynamicField_Application}[0]]]></applicationName>";
}

print "$XMLData\n";
# exit 1;
# print  "$WSDL\n$Proxy\n$Operation\n$XMLHeader\n$XMLData\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
        Proxy => 'http://145.228.78.96:23002/midtier_linux/services/ARService?server=pmsinte&webService=WS_CreateUpdate_Incident',
        URI => 'WS_CreateUpdate_IncidentService',
        Operation => $Operation,
        XMLHeader => $XMLHeader,
        XMLData => $XMLData,
);

if ($Result->{Fault}) {
        # print "Error:\n Code:    $Result->{Fault}->{faultcode}\n String:  $Result->{Fault}->{faultstring}\n"
} else {
	local @ARGV = ("$Article{TicketID}", "$Article{ArticleID}");
	do "/opt/otrs/Custom/Webservice/RemedyInterface/development/pms_inte/Remedy_AddAttachment.pl";
}

print Dumper($Result);
