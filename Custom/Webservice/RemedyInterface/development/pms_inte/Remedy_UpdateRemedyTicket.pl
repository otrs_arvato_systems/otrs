#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");

# Check if the required Config settings are set
my $XMLHeader = "
<AuthenticationInfo>
        <userName>WSTNGSAP_T</userName>
        <password>WSTNGSAP_T</password>
</AuthenticationInfo>
";

my $Details = 'Hehehehehehe Statuschange - no further Message provided
Priorität: 1 - urgent
Organsiation: Lekkerland information systems GmbH First Name: Rainer
LastName: Frey
Mail: rainer.frey@lekkerland.de
Phone: +4922341821223
Location: Zentrale / HQ, Lekkerland Deutschland GmbH & Co.KG / Lekkerland AG & Co. KG
Country: DE
';

my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
    TicketID => $ARGV[0],
    DynamicFields => 1,
    UserID => 1,
);

$Ticket{TicketNumber} = "100146284";
$Ticket{DynamicField_RemedyTicketNumber} = "INC03915774";
my $Status = "Customer Pending";
my $Priority = "Medium";

if ($Status eq 'Closed') {
    print "Status Closed will be set by the UHD\n";
    exit 1;
}


my $XMLData =
"<Details><![CDATA[$Details]]></Details>
<Status>$Status</Status>
<Priority>$Priority</Priority>
<TicketID>$Ticket{DynamicField_RemedyTicketNumber}</TicketID>
<externalKey>$Ticket{TicketNumber}</externalKey>
";

# --
# Pending States
#  if a ticket is in a pending state, the fields PendingReason and PendingUntil need to be filled with values
# ---
my $PendingReason;
$PendingReason = 'see details' if $Status =~ /pending|Pending/;
my $PendingUnitl;
$PendingUnitl = '2099-12-12T00:00:01+02:00' if $Status =~ /pending|Pending/;

$XMLData .= "<PendingReason><![CDATA[$PendingReason]]></PendingReason>\n" if $PendingReason;
$XMLData .= "<PendingUntil><![CDATA[$PendingUnitl]]></PendingUntil>\n" if $PendingUnitl;

# ---
# Solution Info
#  if a ticket is set to resolved or closed, the fields IncidentCause and IncidentSolution need to be filled with values
# ---
my $IncidentSolution;
$IncidentSolution = 'Solution logged in OTRS' if ($Status eq 'Resolved' || $Status eq 'Closed');
my $IncidentCause;
$IncidentCause = 'Cause logged in OTRS' if ($Status eq 'Resolved' || $Status eq 'Closed');

$XMLData .= "<IncidentSolution><![CDATA[$IncidentSolution]]></IncidentSolution>\n" if $IncidentSolution;
$XMLData .= "<IncidentCause><![CDATA[$IncidentCause]]></IncidentCause>\n" if $IncidentCause;

# ---
# Incident Type serves for the Remedy Group Assignment
# ---

my $Group = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::GroupMapping")->{$Ticket{Queue}};
$XMLData .= "<ResponsibleGroup>$Group</ResponsibleGroup>" if $Group; # if ($Ticket{Queue} =~ /User helpdesk/);

my $Operation = 'UpdateIncident';

print $XMLHeader, "\n";
print $XMLData, "\n";

die "test\n";
# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
        Proxy => "http://145.228.78.96:23002/midtier_linux/services/ARService?server=pmsinte&webService=WS_CreateUpdate_Incident",
        URI => 'WS_CreateUpdate_IncidentService',
        Operation => $Operation,
        XMLHeader => $XMLHeader,
        XMLData => $XMLData,
);

print Dumper($Result);
