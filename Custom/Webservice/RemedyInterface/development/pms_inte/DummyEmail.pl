#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Z_SOAP;
my $Webservice = Z_SOAP->new();

$Webservice->ErrorHandling(
                ErrorMessage => '@Jonas, this is a test email
Statuschange - no further Message provided
Priorität: 1 - urgent
Organsiation: Lekkerland information systems GmbH
First Name: Rainer
LastName: Frey
Mail: rainer.frey@lekkerland.de
Phone: +4922341821223
Location: Zentrale / HQ, Lekkerland Deutschland GmbH & Co.KG / Lekkerland AG &
Co. KG
Country: DE' );
