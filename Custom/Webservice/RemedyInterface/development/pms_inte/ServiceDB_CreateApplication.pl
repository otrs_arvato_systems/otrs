#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use OTRS_SyncConfigItem;
my $OTRS_SyncObject = OTRS_SyncConfigItem->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'ServiceDB Webservice',
    },
);

my $XMLHeader = "
<AuthenticationInfo>
        <userName>WS_CMDB_OTRS</userName>
        <password>WS_CMDB_OTRS</password>
</AuthenticationInfo>
";

die "Bitte einen Debitor angeben\n" if !$ARGV[0];


my $XMLData =
"<StatusCode>Valid</StatusCode>
<ApplicationName>fre sap fiori dev otto</ApplicationName>
<Description>FRE SAP FIORI Entwicklungssystem von Otto</Description>
<DebitorNo>102989</DebitorNo>
<CompanyName>Otto GmbH & Co KG</CompanyName>
<ServiceTimeUrgent></ServiceTimeUrgent>
<ServiceTimeHigh></ServiceTimeHigh>
<ServiceTimeMedium></ServiceTimeMedium>
<ServiceTimeLow></ServiceTimeLow>
<HolidayListUrgent></HolidayListUrgent>
<HolidayListHigh></HolidayListHigh>
<HolidayListMedium></HolidayListMedium>
<HolidayListLow></HolidayListLow>
<IncidentNotification>Yes</IncidentNotification>
<OperationStatus>Productive</OperationStatus>
<SLAReportingRelevance>Yes</SLAReportingRelevance>
<SLA>Basic</SLA>
";

print $XMLData;

my $Operation = 'GetList';

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
    Proxy => 'http://145.228.78.96:23002/midtier_linux/services/ARService?server=pmsinte&webService=WS_ADB_Applications',
    # Proxy => 'http://145.228.78.97:23002/midtier_linux/services/ARService?server=pms&webService=WS_ADB_Applications',
    URI => 'WS_ADB_ApplicationsService', 
    Operation => $Operation,
    XMLHeader => $XMLHeader,
    XMLData => $XMLData,
);

if ( defined  $Result->{$Operation."Response"} && defined $Result->{$Operation."Response"}->{getListValues} ) {
    
    # ---
    # Error Handling wenn die Response kein Array ist
    # ---

    my @ApplicationArray = @{ $Result->{$Operation."Response"}->{getListValues} };

    foreach my $Application (@ApplicationArray) {

	# print Dumper($Application);
	# exit 1;
        print "Synching $Application->{ApplicationName} $Application->{ApplicationID}\n";

        my $SyncResponse = $OTRS_SyncObject->HandleApplication(
            Application => $Application,
        );

        if ($SyncResponse->{Success}) {
            print "Success! $SyncResponse->{Message}\n";
        } else {
            print "Error! $SyncResponse->{Message}\n";
        }

        sleep(1);
    }

} else {
    print Dumper($Result);
}
