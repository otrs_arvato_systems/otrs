#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $P_TicketID;
if (defined $ARGV[0] ) {
   $P_TicketID = $ARGV[0];
} else {
#   my $tmp = $Webservice->ErrorHandling(
#        ErrorMessage => "Remedy_UpdateRemedyTicket called without TicketID",
#   );
   print "Remedy_UpdateRemedyTicket called without TicketID";
   exit;
}

my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
	TicketID => $P_TicketID,
	DynamicFields => 1,
	UserID => 1,
);

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

foreach my $Key (keys %$ConfigCustomer) {
	$Config->{$Key} = $ConfigCustomer->{$Key}
}

# Check if the required Config settings are set
if ( !$Config->{User}
	|| !$Config->{Password}
	|| !$Config->{RemedyQueue}
	|| !$Config->{DispatchingQueue}
	|| !$Config->{WebserviceUserID}
	) {

#	$Webservice->ErrorHandling(
#		ErrorMessage => "Config for Customer $Ticket{CustomerID} is incomplete",
#	);
	print "Config for Customer $Ticket{CustomerID} is incomplete";
	exit;
}

#local @ARGV = ("$P_TicketID");
#do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_SetExternalKey.pl" if !$Ticket{DynamicField_RemedyExternalKey};

my $XMLHeader = "
<AuthenticationInfo>
        <userName>WSTNGSAP_T</userName>
        <password>WSTNGSAP_T</password>
</AuthenticationInfo>
";

my @ArticleIDs = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleIndex(
    TicketID => $P_TicketID,
);

my %Article = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleGet(
    ArticleID => $ArticleIDs[-1],
    TicketID => $P_TicketID,
    UserID => 1,
);

#my $Success = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleBounce(
#    From      => 'otrs@arvato-systems.de',
#    To        => 'ticket@arvato-systems.de',
#    TicketID  => $P_TicketID,
#    ArticleID => $ArticleIDs[-1],
#    UserID    => 1,
#);

#my $Details = '@Jonas, this is a test email
#Statuschange - no further Message provided
#Priorität: 1 - urgent
#Organsiation: Lekkerland information systems GmbH
#First Name: Rainer
#LastName: Frey
#Mail: rainer.frey@lekkerland.de
#Phone: +4922341821223
#Location: Zentrale / HQ, Lekkerland Deutschland GmbH & Co.KG / Lekkerland AG &
#Co. KG
#Country: DE';

my $Details = substr($Article{Body}, 0, 4999);
$Details =~ s/</&lt;/g;
$Details =~ s/>/&gt;/g;
$Details =~ s/(\.|\,|\;|\?|\&|\/)/ /g;

my $Priority = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::PriorityMapping")->{$Ticket{'Priority'}};
my $Status = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::PMS::StateMapping")->{$Ticket{'State'}};

if ($Status eq 'Closed' || $Status eq 'closed') {
    # print "Status Closed will be set by the UHD\n";
    exit 1;
}

# prepare the XMLData for the SOAP Request
#my $XMLData =
#"<login><![CDATA[$Config->{User}]]></login>
#<password><![CDATA[$Config->{Password}]]></password>
#<externalKey>$Ticket{TicketNumber}</externalKey>
#<details><![CDATA[$Details]]></details>
#";

my $XMLData =
"<Details><![CDATA[$Details]]></Details>
<Status>$Status</Status>
<Priority>$Priority</Priority>
<TicketID>INC03915876</TicketID>
<externalKey>$Ticket{TicketNumber}</externalKey>";

# Pending Info
my $PendingReason;
$PendingReason = 'see details' if $Ticket{State} =~ /pending/;
my $PendingUnitl;
# $PendingUnitl = '2099-12-12T00:00:00+02:00' if $Ticket{State} =~ /pending/;
$PendingUnitl = $Kernel::OM->Get('Kernel::System::Time')->SystemTime2TimeStamp(SystemTime => $Ticket{RealTillTimeNotUsed}) if $Ticket{State} =~ /pending/;
$PendingUnitl =~ s/ /T/ if $Ticket{State} =~ /pending/;

$XMLData .= "<PendingReason><![CDATA[$PendingReason]]></PendingReason>\n" if $PendingReason;
$XMLData .= "<PendingUntil><![CDATA[$PendingUnitl]]></PendingUntil>\n" if $PendingUnitl;

# Solution Info
my $IncidentSolution;
$IncidentSolution = 'Solution logged in OTRS' if ($Status eq 'Resolved' || $Status eq 'Closed');
my $IncidentCause;
$IncidentCause = 'Cause logged in OTRS' if ($Status eq 'Resolved' || $Status eq 'Closed');

$XMLData .= "<IncidentSolution><![CDATA[$IncidentSolution]]></IncidentSolution>\n" if $IncidentSolution;
$XMLData .= "<IncidentCause><![CDATA[$IncidentCause]]></IncidentCause>\n" if $IncidentCause;

my $Group = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::GroupMapping")->{$Ticket{Queue}};
$XMLData .= "<ResponsibleGroup>$Group</ResponsibleGroup>" if $Group;

my $Operation = 'UpdateIncident';

# print $XMLData, "\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
        Proxy => "http://145.228.78.96:23002/midtier_linux/services/ARService?server=pmsinte&webService=WS_CreateUpdate_Incident",
        URI => 'WS_CreateUpdate_IncidentService',
        Operation => $Operation,
        XMLHeader => $XMLHeader,
        XMLData => $XMLData,
);

#print Dumper($Result);

print "Here....".Dumper($ArticleIDs[-1]);

# In case of a faulty response, call errorhandling
if ($Result->{Fault}) {
	# print "Error:\n Code:    $Result->{Fault}->{faultcode}\n String:  $Result->{Fault}->{faultstring}\n";

	# my $Dump = Dumper($Result);
	# $Dump  =~ s/\$VAR1 = \'\n\s*//g;
	# $Dump =~ s/\n\s*';\n//g;

	# my $tmp = $Webservice->ErrorHandling(
	#	ErrorMessage => "Error:\n Code: $Result->{Fault}->{faultcode}\n String: $Result->{Fault}->{faultstring}\n$Dump",
	# );

} else {

	# print Dumper($Result);

	my $RemedyTicket = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
		Result => $Result,
		Operation => $Operation
	);

	my $OTRSTicketID = $Remedy_Webservice_Subs->GetOTRSTicketID( 
		RemedyTicket => $RemedyTicket,
		Config => $Config,
	);

	if (!$OTRSTicketID) {

		my $DumpRemedyTicket = Dumper($RemedyTicket);
#		$Webservice->ErrorHandling(
#				ErrorMessage => "No OTRS TicketID found for Remedy Ticket:\n$DumpRemedyTicket\n\n$Details",
#		);
		print "No OTRS TicketID found for Remedy Ticket:\n$DumpRemedyTicket\n\n$Details";
		exit;
	}

	# Get OTRS TicketData
	my %OTRSTicket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
		TicketID => $OTRSTicketID,
		DynamicFields => 1,
		UserID => 1,
	);

	local @ARGV = ("$OTRSTicketID","$ArticleIDs[-1]");
	do "/opt/otrs/Custom/Webservice/RemedyInterface/development/pms_inte/Remedy_AddAttachment.pl";

	# DEBUG
	# print "Remedy Ticket: $RemedyTicket->{'Ticket-ID'}, OTRSTicketID: $OTRSTicket{TicketID}, Remedy OTRS Referenz: $RemedyTicket->{externalKey}\n";
	# print "DEBUG: UpdateDone!\n";

}
