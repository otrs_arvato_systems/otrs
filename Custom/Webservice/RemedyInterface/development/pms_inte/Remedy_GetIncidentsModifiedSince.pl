#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $CustomerID;
if (defined $ARGV[0] ) {
   $CustomerID = $ARGV[0];
} else {
   my $tmp = $Webservice->ErrorHandling(
        ErrorMessage => "Remedy_GetTicketByExternalKey called without CustomerID",
   );
   exit;
}

my $Operation = 'GetIncidentsModifiedSinceRequest';

my $XMLHeader = "
<AuthenticationInfo>
        <userName>WSTNGSAP_T</userName>
        <password>WSTNGSAP_T</password>
</AuthenticationInfo>
";

my $XMLData = '<seconds>5000</seconds>';

my $Operation = 'GetIncidentsModifiedSince';

print $XMLData, "\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
        Proxy => "http://145.228.78.96:23002/midtier_linux/services/ARService?server=pmsinte&webService=WS_CreateUpdate_Incident",
        URI => 'WS_CreateUpdate_IncidentService',
        Operation => $Operation,
        XMLHeader => $XMLHeader,
        XMLData => $XMLData,
);

print Dumper($Result);
