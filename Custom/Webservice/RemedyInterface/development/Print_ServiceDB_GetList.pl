#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

# prepare the XMLData for the SOAP Request
my $XMLData =
"<login><![CDATA[$Config->{User}]]></login>
<password><![CDATA[$Config->{Password}]]></password>
";

my $Operation = 'GetList';

my $DynamicField = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
        ID   => $Config->{DynamicField_RemedyTicketNumber_ID},
);

$XMLData .= "<incidentID>$Ticket{'DynamicField_'.$DynamicField->{Name}}</incidentID>";

print $XMLData, "\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{Proxy},
	URI => $Config->{URI},
	Operation => $Operation,
	XMLHeader => '',
	XMLData => $XMLData,
);

print Dumper($Result);
