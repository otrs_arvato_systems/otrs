#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

print "Need IncidentID\n" if (!$ARGV[0]);
exit if (!$ARGV[0]);


my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");

my $XMLHeader = "
<AuthenticationInfo>
	<userName><![CDATA[WSOTTO2]]></userName>
	<password><![CDATA[Ott0&AMS]]></password>
</AuthenticationInfo>
";

# prepare the XMLData for the SOAP Request
my $XMLData = 
"<TicketID>$ARGV[0]</TicketID>";

my $Operation = 'GetDetails';

print $XMLData, "\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{PMS_Details_Proxy},
	URI => $Config->{PMS_Details_URI},
	Operation => $Operation,
	XMLHeader => $XMLHeader,
	XMLData => $XMLData,
);

print Dumper($Result);
