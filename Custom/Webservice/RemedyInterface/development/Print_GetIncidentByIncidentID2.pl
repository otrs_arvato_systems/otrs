#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $P_TicketID;
if (defined $ARGV[0] ) {
   $P_TicketID = $ARGV[0];
} else {
   my $tmp = $Webservice->ErrorHandling(
        ErrorMessage => "Remedy_GetTicketByExternalKey called without IncidentID",
   );
   exit;
}

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");

# prepare the XMLData for the SOAP Request
my $XMLData =
"
<login><![CDATA[WSAMS_P]]></login>
<password><![CDATA[WSAMS_P]]></password>
<incidentID>$ARGV[0]</incidentID>
";

my $Operation = 'GetIncidentByIncidentIDRequest';

print $XMLData, "\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
    Proxy => $Config->{Proxy},
    URI => $Config->{URI},
    Operation => $Operation,
    XMLHeader => '',
    XMLData => $XMLData,
);

print Dumper($Result);
