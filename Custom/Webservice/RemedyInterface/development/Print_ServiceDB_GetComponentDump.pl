#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use OTRS_SyncConfigItem;
my $OTRS_SyncObject = OTRS_SyncConfigItem->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'ServiceDB Webservice',
    },
);

my $XMLHeader = "
<AuthenticationInfo>
        <userName>WS_CMDB_OTRS</userName>
        <password>WS_CMDB_OTRS</password>
</AuthenticationInfo>
";

die "Please call this script with a component name\n" if !$ARGV[0];

my $Qualification = "'Component' = \"$ARGV[0]\" AND 'Status' = \"Valid\""; # "'Application Name' LIKE \"%otto%\"  AND 'Status' = \"Valid\"";
# my $Qualification = "'Application Name' LIKE \"%nma-o%\"  AND 'Status' = \"Valid\"";

my $XMLData =
#"<Qualification>'Application Name' LIKE \"%test%\" AND 'Status' = \"Valid\"</Qualification>
"<Qualification>$Qualification</Qualification>
<startRecord></startRecord>
<maxLimit></maxLimit>
";

# print $XMLData;

my $Operation = 'GetList';

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
    # Proxy => 'http://145.228.78.96:23002/midtier_linux/services/ARService?server=pmsinte&webService=WS_ADB_Applications',
    Proxy => 'http://145.228.78.96:23002/midtier_linux/services/ARService?server=pmsinte&webService=WS_ADB_Components_All_Get',
    URI => 'WS_ADB_Components_All_GetService', 
    Operation => $Operation,
    XMLHeader => $XMLHeader,
    XMLData => $XMLData,
);

# print Dumper ($Result);
# die "test\n";

if ( defined  $Result->{$Operation."Response"} && defined $Result->{$Operation."Response"}->{getListValues} ) {
    
    # ---
    # Error Handling wenn die Response kein Array ist
    # ---

    my $Component = $Result->{$Operation."Response"}->{getListValues};

    print Dumper($Component);

    # my $Hostname = $Component->{Hostname_long} || $Component->{Hostname};
    # print $Hostname, ";", $Component->{Component_Description}, "\n";
	# exit 1;
        # print $Application->{ApplicationName}, "; ", $Application->{Component}, "\n";
#        print "Synching $Application->{ApplicationName} $Application->{ApplicationID}\n";

} else {
    print Dumper($Result);
}
