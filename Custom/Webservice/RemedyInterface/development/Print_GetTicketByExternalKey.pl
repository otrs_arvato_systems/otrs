#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $P_TicketID;
if (defined $ARGV[0] ) {
   $P_TicketID = $ARGV[0];
} else {
   my $tmp = $Webservice->ErrorHandling(
        ErrorMessage => "Remedy_GetTicketByExternalKey called without TicketID",
   );
   exit;
}

my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
	TicketID => $P_TicketID,
	DynamicFields => 1,
	UserID => 1,
);

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

foreach my $Key (keys %$ConfigCustomer) {
	$Config->{$Key} = $ConfigCustomer->{$Key}
}

# Check if the required Config settings are set
if ( !$Config->{User}
	|| !$Config->{Password}
	|| !$Config->{RemedyQueue}
	|| !$Config->{DispatchingQueue}
	|| !$Config->{WebserviceUserID}
	) {

	$Webservice->ErrorHandling(
		ErrorMessage => "Config for Customer $Ticket{CustomerID} is incomplete",
	);
	exit;
}

# prepare the XMLData for the SOAP Request
my $XMLData =
"<login><![CDATA[$Config->{User}]]></login>
<password><![CDATA[$Config->{Password}]]></password>
";

my $Operation = 'GetIncidentByExternalKeyRequest';
$XMLData .= "<externalKey><![CDATA[$Ticket{TicketNumber}]]></externalKey>";    

#print "DEBUG XMLData: $XMLData\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{Proxy},
	URI => $Config->{URI},
	Operation => $Operation,
	XMLData => $XMLData,
);

print Dumper($Result);

