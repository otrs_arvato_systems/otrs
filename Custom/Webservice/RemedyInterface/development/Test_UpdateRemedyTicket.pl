#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $P_TicketID;
if (defined $ARGV[0] ) {
   $P_TicketID = $ARGV[0];
} else {
   my $tmp = $Webservice->ErrorHandling(
        ErrorMessage => "Remedy_UpdateRemedyTicket called without TicketID",
   );
   exit;
}

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");

my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
	TicketID => $P_TicketID,
	DynamicFields => 1,
	UserID => 1,
);

my $DynamicField = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
    ID   => 52,
);

my $XMLHeader = "
<AuthenticationInfo>
        <userName>WSAMS_P</userName>
        <password>WSAMS_P</password>
</AuthenticationInfo>
";

my @ArticleIDs = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleIndex(
    TicketID => $P_TicketID,
);

my %Article = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleGet(
    ArticleID => $ArticleIDs[-1],
    TicketID => $P_TicketID,
    UserID => 1,
);

#print "Ticket --- \n";
#print Dumper(\%Ticket);

#print "Article --- \n";
#print Dumper(\%Article);

#my $Success = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleBounce(
#    From      => 'otrs@arvato-systems.de',
#    To        => 'ticket@arvato-systems.de',
#    TicketID  => $P_TicketID,
#    ArticleID => $ArticleIDs[-1],
#    UserID    => 1,
#);

my $Details = substr($Article{Body}, 0, 4999);
my $Priority = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::PriorityMapping")->{$Ticket{'Priority'}};
my $Status = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::PMS::StateMapping")->{$Ticket{'State'}};
my $PendingReason = '';
$PendingReason = 'see details' if $Ticket{State} =~ /pending/;
my $PendingUnitl;
$PendingUnitl = $Kernel::OM->Get('Kernel::System::Time')->SystemTime2TimeStamp(SystemTime => $Ticket{RealTillTimeNotUsed}) if $Ticket{State} =~ /pending/;
$PendingUnitl =~ s/ /T/;
print $PendingUnitl, "\n";

$Details = "";

# prepare the XMLData for the SOAP Request
my $XMLData =
"<Details><![CDATA[$Details]]></Details>
<Status>$Status</Status>
<Priority><![CDATA[$Priority]]></Priority>
<TicketID>$Ticket{DynamicField_RemedyTicketNumber}</TicketID>
<externalKey>$Ticket{TicketNumber}</externalKey>
<PendingReason><![CDATA[$PendingReason]]></PendingReason>
<PendingUntil><![CDATA[$PendingUnitl]]></PendingUntil>
"; 
$XMLData .= "<IncidentSolution><![CDATA[]]></IncidentSolution>\n";
$XMLData .= "<IncidentCause><![CDATA[]]></IncidentCause>\n";


my $Operation = 'UpdateIncident';

#my $DynamicField = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
#	ID   => $Config->{DynamicField_RemedyTicketNumber_ID},
#);
#
#$XMLData .= "<incidentID><![CDATA[$Ticket{'DynamicField_'.$DynamicField->{Name}}]]></incidentID>";

#print $XMLData, "\n";

#die "test\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{PMS_Proxy},
        URI => $Config->{PMS_URI},
	Operation => $Operation,
	XMLHeader => $XMLHeader,
	XMLData => $XMLData,
);

# In case of a faulty response, call errorhandling
#if ($Result->{Fault}) {
#	print "Error:\n Code:    $Result->{Fault}->{faultcode}\n String:  $Result->{Fault}->{faultstring}\n";

	# my $Dump = Dumper($Result);
	# $Dump  =~ s/\$VAR1 = \'\n\s*//g;
	# $Dump =~ s/\n\s*';\n//g;

	# my $tmp = $Webservice->ErrorHandling(
	#	ErrorMessage => "Error:\n Code: $Result->{Fault}->{faultcode}\n String: $Result->{Fault}->{faultstring}\n$Dump",
	# );

#} else {

#	my $RemedyTicket = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
#		Result => $Result,
#		Operation => $Operation
#	);

#	my $OTRSTicketID = $Remedy_Webservice_Subs->GetOTRSTicketID( 
#		RemedyTicket => $RemedyTicket,
#		Config => $Config,
#	);

#	if (!$OTRSTicketID) {

#		my $DumpRemedyTicket = Dumper($RemedyTicket);
#		$Webservice->ErrorHandling(
#				ErrorMessage => "No OTRS TicketID found for Remedy Ticket:\n$DumpRemedyTicket",
#		);
#		exit;
#	}

	# Get OTRS TicketData
#	my %OTRSTicket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
#		TicketID => $OTRSTicketID,
#		DynamicFields => 1,
#		UserID => 1,
#	);

	# DEBUG
#	print "Remedy Ticket: $RemedyTicket->{incidentID}, OTRSTicketID: $OTRSTicket{TicketID}, Remedy OTRS Referenz: $RemedyTicket->{externalKey}\n";
#	print "DEBUG: UpdateDone!\n";

#}
