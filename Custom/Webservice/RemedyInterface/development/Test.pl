#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Test',
    },
);

# Add comment to test GIT - Edited something here
# Get email object

#my $Soap = SOAP::Lite->new(proxy => $Proxy);
#$Soap->ns("something-something", 'urn');

#my $Result = $Soap->call(($Operation) => @Params) ;
#print Dumper($Result);

my @arr = (2, 4, 8, 10);

#print "First result:\n";
#print scalar @arr; 

#print "\n\nSecond result:\n";
#print $#arr + 1; # Shift numeration with +1 as it shows last index that starts with 0.

#print "\n\nThird result:\n";
my $arrSize = @arr;
#print $arrSize;

#my $tmp = $Webservice->ErrorHandling(
#        ErrorMessage => "Testing",
#);

my $GoogleAnalyticsConfig = $Kernel::OM->Get('Kernel::Config')->Get("GoogleAnalytics");
print "Agent = $GoogleAnalyticsConfig->{Agent_UA_ID}\n";
print "Customer = $GoogleAnalyticsConfig->{Customer_UA_ID}\n";

#my $Success = $Kernel::OM->Get('Kernel::System::Ticket')->TicketQueueSet(
#                                Queue => "Service Desk", # $Param{Config}->{DispatchingQueue},
#                                TicketID => "157208",
#                                UserID => 1,
#                        );

#if (!$Success) {
#	print "Failed";
#}
