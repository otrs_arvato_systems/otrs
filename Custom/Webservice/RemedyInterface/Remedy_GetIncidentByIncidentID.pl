#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $P_TicketID;
if (defined $ARGV[0] ) {
   $P_TicketID = $ARGV[0];
} else {
   my $tmp = $Webservice->ErrorHandling(
        ErrorMessage => "Remedy_GetTicketByExternalKey called without TicketID",
   );
   exit;
}

my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
	TicketID => $P_TicketID,
	DynamicFields => 1,
	UserID => 1,
);

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

foreach my $Key (keys %$ConfigCustomer) {
	$Config->{$Key} = $ConfigCustomer->{$Key}
}

# Check if the required Config settings are set
if ( !$Config->{User}
	|| !$Config->{Password}
	|| !$Config->{RemedyQueue}
	|| !$Config->{DispatchingQueue}
	|| !$Config->{WebserviceUserID}
	) {

	$Webservice->ErrorHandling(
		ErrorMessage => "Config for Customer $Ticket{CustomerID} is incomplete",
	);
	exit;
}

# prepare the XMLData for the SOAP Request
my $XMLData =
"<login><![CDATA[$Config->{User}]]></login>
<password><![CDATA[$Config->{Password}]]></password>
";

my $Operation = 'GetIncidentByIncidentIDRequest';

my $DynamicField = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
        ID   => $Config->{DynamicField_RemedyTicketNumber_ID},
);

$XMLData .= "<incidentID>$Ticket{'DynamicField_'.$DynamicField->{Name}}</incidentID>";

#print $XMLData, "\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{Proxy},
	URI => $Config->{URI},
	Operation => $Operation,
	XMLHeader => '',
    XMLData => $XMLData,
);

# In case of a faulty response, call errorhandling
if ($Result->{Fault}) {
	my $Dump = Dumper($Result);
	$Dump  =~ s/\$VAR1 = \'\n\s*//g;
	$Dump =~ s/\n\s*';\n//g;

	my $tmp = $Webservice->ErrorHandling(
		ErrorMessage => "Error:\n Code: $Result->{Fault}->{faultcode}\n String: $Result->{Fault}->{faultstring}\n$Dump",
	);

} else {

	my $RemedyTicket = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
		Result => $Result,
		Operation => $Operation
	);

	if (!$RemedyTicket || !$RemedyTicket->{incidentID}) {
		$Webservice->ErrorHandling(
			ErrorMessage => "Empty Response from Webservice: $Result",
		);
		exit;
	}

	#print "DEBUG: Remedy Ticket: $RemedyTicket->{incidentID}, OTRSTicketID: $Ticket{TicketID}, Remedy OTRS Referenz: $RemedyTicket->{externalKey}\n";

	$Remedy_Webservice_Subs->UpdateTicketType(
		Config => $Config,
		RemedyTicket => $RemedyTicket,
		OTRSTicket => \%Ticket
	);

	#print "DEBUG: Type ...done!\n";

	$Remedy_Webservice_Subs->UpdateTicketPriority(
		Config => $Config,
		RemedyTicket => $RemedyTicket,
		OTRSTicket => \%Ticket
	);

	#print "DEBUG: Priority ...done!\n";

	$Remedy_Webservice_Subs->UpdateTicketState(
		Config => $Config,
		RemedyTicket => $RemedyTicket,
		OTRSTicket => \%Ticket
	);

	#print "DEBUG: State ...done!\n";
	
	local @ARGV = ("$Ticket{TicketID}");
	do "/opt/otrs/Custom/Webservice/RemedyInterface/Remedy_GetDetails.pl";

}
