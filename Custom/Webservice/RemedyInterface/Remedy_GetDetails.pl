#!/usr/bin/perl -w

use strict;
use warnings;

no warnings 'once';

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $P_TicketID;
if (defined $ARGV[0] ) {
   $P_TicketID = $ARGV[0];
} else {
   my $tmp = $Webservice->ErrorHandling(
        ErrorMessage => "Remedy_GetDetails.pl called without TicketID",
   );
   exit;
}

my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
	TicketID => $P_TicketID,
	DynamicFields => 1,
	UserID => 1,
);

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
# my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

# foreach my $Key (keys %$ConfigCustomer) {
#	$Config->{$Key} = $ConfigCustomer->{$Key}
# }

# Check if the required Config settings are set
if ( !$Config->{User}
	|| !$Config->{Password}
	|| !$Config->{RemedyQueue}
	#|| !$Config->{DispatchingQueue}
	|| !$Config->{WebserviceUserID}
	) {

	$Webservice->ErrorHandling(
		ErrorMessage => "Remedy Config is incomplete",
	);
	exit;
}

my $XMLHeader = "
<AuthenticationInfo>
	<userName><![CDATA[$Config->{User}]]></userName>
	<password><![CDATA[$Config->{Password}]]></password>
</AuthenticationInfo>
";

my $DynamicField = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
	ID   => $Config->{DynamicField_RemedyTicketNumber_ID},
);

my $RemedyTicketNumber = $Ticket{'DynamicField_'.$DynamicField->{Name}};

if (!$RemedyTicketNumber) {
    print "Ticket has no Remedy ticketnumber\n";
    exit 1;
}

# prepare the XMLData for the SOAP Request
my $XMLData = 
"<TicketID>$RemedyTicketNumber</TicketID>";

my $Operation = 'GetDetails';

#print $XMLData, "\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{PMS_Details_Proxy},
	URI => $Config->{PMS_Details_URI},
	Operation => $Operation,
	XMLHeader => $XMLHeader,
	XMLData => $XMLData,
);

# In case of a faulty response, call error handling
if ($Result->{Fault}) {

} else {
	my $Details = $Result->{GetDetailsResponse}{DetailsNote};
	# Details Formatting
	$Details =~ s/\x{00E4}/ae/g;
	$Details =~ s/\x{00F6}/oe/g;
	$Details =~ s/\x{00FC}/ue/g;
	$Details =~ s/[^[:ascii:]]/00_ESCAPE_XX /g;
	my @Times = ( $Details =~ /(\d\d\d\d\d\d\d\d\d\d+)00_ESCAPE_XX /g );
	$Details =~ s/00_ESCAPE_XX//g;
	$Details =~ s/\n /\n/g;
	my $TimeStamp;
	my $OldTimeStamp;
	my $User = $Config->{User};
	foreach my $UnixTime (@Times) {
    		$TimeStamp = $Kernel::OM->Get('Kernel::System::Time')->SystemTime2TimeStamp(SystemTime => $UnixTime);
    		$Details =~ s/$UnixTime/\n\[$TimeStamp\]/g;
		$Details =~ s/\[$OldTimeStamp\] $User.*\[$TimeStamp\]/\[$TimeStamp\]/sg if $OldTimeStamp;
    		#$Details =~ s/\[$OldTimeStamp\] $User(.*?)\[$TimeStamp\]/\[$TimeStamp\]/g if $OldTimeStamp;
    		$OldTimeStamp = $TimeStamp;
	}
	$Details =~ s/\[$OldTimeStamp\] $User(.*?)\[$TimeStamp\]/\[$TimeStamp\]/g;
	$Details =~ s/\[$OldTimeStamp\] $User.*\[$TimeStamp\]/\[$TimeStamp\]/sg if $OldTimeStamp;
	$Details =~ s/\[$TimeStamp\] $User.*//sg if $TimeStamp;
	#print $Details;
	$Details =~ s/^\n//g;
	chop($Details);
	chop($Details);
	
	#print "DEBUG: Details:\n$Details\n";
	
	$Remedy_Webservice_Subs->UpdateTicketDetails(
		Config => $Config,
		Details => $Details,
		OTRSTicket => \%Ticket
	);

}
