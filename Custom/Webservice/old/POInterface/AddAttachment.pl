#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/PiInterface";
use MIME::Base64;
use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Pi_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Pi Webservice',
    },
);

my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
my $LogObject = $Kernel::OM->Get('Kernel::System::Log');

my $P_TicketID;
my $P_ArticleID;
if (defined $ARGV[0] && defined $ARGV[1] ) {
   $P_TicketID = $ARGV[0];
   $P_ArticleID = $ARGV[1];
} else {
   my $tmp = $Webservice->ErrorHandling(
        ErrorMessage => "Pi_AddAttachment called without TicketID, ArticleID",
   );
   #print "Pi_Attachment called without TicketID/ArticleID\n";
   exit;
}

my %Ticket = $TicketObject->TicketGet(
        TicketID => $P_TicketID,
        DynamicFields => 1,
        UserID => 1,
);

my $TicketNumber = $Ticket{TicketNumber};

#print "CustomerID for ticket $P_TicketID = $Ticket{CustomerID}\n";

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Pi::General");
my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

foreach my $Key (keys %$ConfigCustomer) {
        $Config->{$Key} = $ConfigCustomer->{$Key}
}

# Check if the required Config settings are set
if ( !$Config->{User}
        || !$Config->{Password}
        || !$Config->{PiQueue}
        || !$Config->{DispatchingQueue}
        || !$Config->{WebserviceUserID}
        ) {

        $Webservice->ErrorHandling(
                ErrorMessage => "Config for Customer $Ticket{CustomerID} is incomplete",
        );
        exit;
}

my $XMLHeader = "";

my %Article = $TicketObject->ArticleGet(
       TicketID	     => $P_TicketID,
       ArticleID     => $P_ArticleID,
);

#print "ArticleID = $P_ArticleID\nArticle Subject = $Article{Subject}\n";
my $Operation = 'AddAttachmentRequest';
my $StripPlainBodyAsAttachment = 3;

# get attachment index (without attachments)
my %AttachmentIndex = $TicketObject->ArticleAttachmentIndex(
	ArticleID                  => $P_ArticleID,
	StripPlainBodyAsAttachment => $StripPlainBodyAsAttachment,
	Article                    => \%Article,
	UserID                     => 1,
);

#print scalar keys(%AttachmentIndex);

if (scalar keys(%AttachmentIndex) eq 0)
{
	#$LogObject->Log(
        #	Priority => 'error',
        #	Message  => "TEST - No attachment to transfer to Pi",
   	#);
}
else
{
#	$LogObject->Log(
#        	Priority => 'error',
#        	Message => "TEST - Called AddAttachment for Ticket $P_TicketID and Article $P_ArticleID"
#	);

	for my $FileID ( sort keys %AttachmentIndex ) {
		# get a attachment
		my %Attachment = $TicketObject->ArticleAttachment(
			ArticleID => $P_ArticleID,
			FileID    => $FileID,
			UserID    => 1,
		);
		#print $Attachment{Filename} . "\n";

		my $Filename = $Attachment{Filename};
		my $AttachmentContentBase64 = encode_base64($Attachment{Content});

		my $XMLData = "<login>WSAMS_P</login>
				<password>WSAMS_P</password>
				<externalKey>$TicketNumber</externalKey>
       	       		 	<fileName>$Filename</fileName>
       		         	<data>$AttachmentContentBase64</data>";

		my $Result = $Webservice->WSDLRequest(
       			Proxy => $Config->{Proxy},
       			URI => $Config->{URI},
       			Operation => $Operation,
       			XMLHeader => '',
	       		XMLData => $XMLData,
		);

		if ($Result->{Fault}) {
			#print "Error:\n Code: $Result->{Fault}->{faultcode}\n String:  $Result->{Fault}->{faultstring}\n";
		        $LogObject->Log(
                		Priority => 'debug',
                		Message  => "Pi Add Attachment Error:\n Code: $Result->{Fault}->{faultcode}\n String:  $Result->{Fault}->{faultstring}\n",
        		);
			#my $tmp = $Webservice->ErrorHandling(
			#	ErrorMessage => "Error:\n Code: $Result->{Fault}->{faultcode}\n String: $Result->{Fault}->{faultstring}\n$Dump",
			#);
			exit;
		}
	}
}

#my $XMLData = "<externalKey>$P_TicketID</externalKey>
#	  	<fileName></fileName>
#		<data></data>";

#my $Operation = 'AddAttachmentRequest';

# Call the SOAP Request
#my $Result = $Webservice->WSDLRequest(
#        Proxy => $Config->{Proxy},
#        URI => $Config->{URI},
#        Operation => $Operation,
#	XMLHeader => '',
#	XMLData => $XMLData,
#);

# In case of a faulty response, call errorhandling
#if ($Result->{Fault}) {
#        print "Error:\n Code:    $Result->{Fault}->{faultcode}\n String:  $Result->{Fault}->{faultstring}\n";

#        my $Dump = Dumper($Result);
#        $Dump  =~ s/\$VAR1 = \'\n\s*//g;
#        $Dump =~ s/\n\s*';\n//g;
#        my $tmp = $Webservice->ErrorHandling(
#              ErrorMessage => "Error:\n Code: $Result->{Fault}->{faultcode}\n String: $Result->{Fault}->{faultstring}\n$Dump",
#        );
