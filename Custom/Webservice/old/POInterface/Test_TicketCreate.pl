#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/PiInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Pi_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Pi Webservice',
    },
);

my $LogObject = $Kernel::OM->Get('Kernel::System::Log');

if (!$ARGV[0]) {
    $LogObject->Log(
        Priority => 'error',
        Message  => "PiTicketCreate called without TicketID",
    );
	die "PiTicketCreate called without TicketID";
}

my $ParamTicketID = $ARGV[0];

# get ticket object
my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

my %Ticket = $TicketObject->TicketGet(
	TicketID => $ParamTicketID,
	UserID => 1,
	DynamicFields => 1,
);

my %Article = $TicketObject->ArticleFirstArticle(
        TicketID      => $ParamTicketID,
        DynamicFields => 0,     # 0 or 1, see ArticleGet()
);

# get config object
my $ConfigObject = $Kernel::OM->Get('Kernel::Config');

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Pi::General");
my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

foreach my $Key (keys %$ConfigCustomer) {
	$Config->{$Key} = $ConfigCustomer->{$Key}
}

# Check if the required Config settings are set
if ( !$Config->{User}
	|| !$Config->{Password}
	|| !$Config->{PiQueue}
#	|| !$Config->{DispatchingQueue}
	|| !$Config->{WebserviceUserID}
	) {

	$Webservice->ErrorHandling(
		ErrorMessage => "Config for Customer $Ticket{CustomerID} is incomplete",
	);
	exit;
}

my $TicketType = 'Shalalala';

# Retrieve company information
my $Company = "Unknown Company: ". $Ticket{'CustomerID'};
my $CustomerData = $ConfigObject->Get("Webservice::".$Ticket{'CustomerID'}."::General");
if ($CustomerData && $CustomerData->{Company}) {
	$Company = $CustomerData->{Company};
}

# get customeruser object
my $CustomerUserObject = $Kernel::OM->Get('Kernel::System::CustomerUser');
my %CustomerUser = $CustomerUserObject->CustomerUserDataGet(
	User => $Ticket{'CustomerUserID'},
);

my $CustomerPhone = $CustomerUser{'UserPhone'};
$CustomerPhone = '+49524180' if (!$CustomerPhone); 

my $Operation = 'doAction';
#my $XMLHeader = "
#<AuthenticationInfo>
#        <userName>$Config->{User}</userName>
#        <password><![CDATA[$Config->{Password}]]></password>
#</AuthenticationInfo>
#";
my $XMLHeader = "";

#$LogObject->Log(
#        Priority => 'error',
#        Message => "TEST - Pi Ticket Create is called for ID: $ParamTicketID with customer user $CustomerUser{'UserEmail'}"
#);

$CustomerUser{'UserEmail'} = '' if $CustomerUser{'UserEmail'} =~ /localhost/;
$CustomerUser{'UserEmail'} = '' if $CustomerUser{'UserEmail'} eq 'otrs@arvato-systems.de';

my $ShortDescription = substr($Ticket{Title}, 0, 119);
my $Details = substr($Article{Body}, 0, 4999);

my $XMLData = 
"<action>create</action>
<ticket id=\"$Ticket{TicketID}\">
	<ext-id>$Ticket{TicketNumber}</ext-id><!-- Human-world ID -->
	<title>$Ticket{Title}</title>
	<service>$Ticket{Service}</service>
	<description><![CDATA[$Details]]></description>
	<assignee>$Ticket{Responsible}</assignee>
	<type>$Ticket{Type}</type>
	<queue>$Ticket{Queue}</ticketType>
	<priority>$Ticket{PriorityID}</priority>
</ticket>
<customer>
	<firstname><![CDATA[$CustomerUser{'UserFirstname'}]]></firstname>
	<lastname><![CDATA[$CustomerUser{'UserLastname'}]]></lastname>
	<phone><![CDATA[$CustomerPhone]]></phone>
	<mail><![CDATA[$CustomerUser{'UserEmail'}]]></mail>
	<company><![CDATA[$Company]]></company>
</customer>";

my $DynamicExist = 0;
foreach my $Key (sort(keys %Ticket)) {
	if (index($Key, 'DynamicField_') == 0) { # begins with ...
		if ($DynamicExist == 0) {
			$XMLData .= "\n<dynamic-fields>";
		}
		my $SimpleKey = $Key;
		$SimpleKey =~ s/DynamicField_//g;
		my $Value = '';
		$Value = $Ticket{$Key} if $Ticket{$Key};

		# print $SimpleKey .' = '. ref($Value) ."\n";
		if (ref($Value) eq 'ARRAY') {
#			foreach my $V ($Value) {
#				$XMLData .= "\n\t<field name=\"$SimpleKey\">$V</field>";
#			}
		} else {
			$XMLData .= "\n\t<field name=\"$SimpleKey\">$Value</field>";
		}
		$DynamicExist += 1;
	}
}
if ($DynamicExist != 0) {
	$XMLData .= "\n</dynamic-fields>";
}

print "$XMLData\n";
# exit 1;

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
        Proxy => $Config->{Proxy}, # e.g. http://localhost:3000/pi 
        URI => $Config->{URI},     # e.g. http://www.arvato.de/webservice/
        Operation => $Operation,
        XMLHeader => $XMLHeader,
        XMLData => $XMLData,
);

print $Result;

if ($Result->{Fault}) {
	print "Error:\n Code:    $Result->{Fault}->{faultcode}\n String:  $Result->{Fault}->{faultstring}\n"
} else {
	# print Dumper($Result->{$Operation."Response"});
	my $LogObject = $Kernel::OM->Get('Kernel::System::Log');

	print Dumper($Result);

	my $PiTicket = $Pi_Webservice_Subs->getTicketListfromSoapResponse(
            Result => $Result,
            Operation => $Operation,
        );

	$PiTicket->{incidentID} = $PiTicket->{TicketID} if (!$PiTicket->{incidentID} && $PiTicket->{TicketID});
	
	# print "DEBUG: Ticket Created ...done!\n", Dumper($PiTicket);
		
	$Pi_Webservice_Subs->UpdatePiTicketNumber(
		Config => $Config,
		PiTicket => $PiTicket,
		OTRSTicket => \%Ticket
	);

	local @ARGV = ("$ParamTicketID");
	do "/opt/otrs/Custom/Webservice/PiInterface/GetTicketByExternalKey.pl";	
	
	# print "DEBUG: PiTicketNumber ...done!\n";
	my %Article = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleGet(
		TicketID => $ParamTicketID,
		ArticleID => $Article{ArticleID},
		UserID => 1,
	);

	$Article{Subject} = "$PiTicket->{TicketID} $Article{Subject}";

	delete $Article{From};
	delete $Article{To};
	delete $Article{Cc};

	my $Sent = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleSend(
	    TicketID => $ParamTicketID,
	    From => 'otrs@arvato-systems.de',
	    To => 'ticket@arvato-systems.de',
	    UserID => $Config->{WebserviceUserID},
	    HistoryType => 'Forward', 
	    HistoryComment => 'Forwarded to Pi',
	    %Article,
	) if $PiTicket->{TicketID};

	local @ARGV = ("$ParamTicketID", "$Article{ArticleID}");
	do "/opt/otrs/Custom/Webservice/PiInterface/AddAttachment.pl";
}

