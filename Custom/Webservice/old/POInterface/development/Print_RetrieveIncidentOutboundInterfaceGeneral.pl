#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom/Webservice/POInterface/development";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);

my $P_TicketID;
if (defined $ARGV[0] ) {
   $P_TicketID = $ARGV[0];
} else {
   print "GetTicket called without TicketID";
   #my $tmp = $Webservice->ErrorHandling(
   #     ErrorMessage => "Remedy_GetIncidentByIncidentID called without TicketID",
   #);
   exit;
}

#my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
#	TicketID => $P_TicketID,
#	DynamicFields => 1,
#	UserID => 1,
#);

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
#my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

#foreach my $Key (keys %$ConfigCustomer) {
#	$Config->{$Key} = $ConfigCustomer->{$Key}
#}

# Check if the required Config settings are set
#if ( !$Config->{User}
#	|| !$Config->{Password}
#	|| !$Config->{RemedyQueue}
#	|| !$Config->{DispatchingQueue}
#	|| !$Config->{WebserviceUserID}
#	) {
	
#	print "Config for Customer $Ticket{CustomerID} is incomplete";
#	$Webservice->ErrorHandling(
#		ErrorMessage => "Config for Customer $Ticket{CustomerID} is incomplete",
#	);
#	exit;
#}

#my $DynamicField = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
#        ID   => $Config->{DynamicField_RemedyTicketNumber_ID},
#);

my $IncidentID = $ARGV[0];

# prepare the XMLData for the SOAP Request
my $XMLData =
"
<login>$Config->{User}</login>
<password>$Config->{Password}</password>
<incidentID>$IncidentID</incidentID>
";

my $Operation = 'GetIncidentByIncidentIDRequest';

print $XMLData, "\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => $Config->{Proxy},
	URI => $Config->{URI},
	Operation => $Operation,
	XMLHeader => '',
	XMLData => $XMLData,
);

print Dumper($Result);

#if($Result->{Fault})
#{

#}
#else
#{
#	my $RemedyTicket = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
#            Result => $Result,
#            Operation => $Operation,
#        );

#        if($RemedyTicket->{externalKey})
#        {
#                $LogObject->Log(
#                        Priority => 'error',
#                        Message => "TEST - Not updating external key for $RemedyTicketNumber. Existing external key $RemedyTicket->{externalKey}."
#                );
#		print "TEST - Not updating external key for $Ticket{'DynamicField_'.$DynamicField->{Name}}. Existing external key $RemedyTicket->{externalKey}.\n";
#                exit 1;
#        }


#	print "External key incoming.....";
#	print $RemedyTicket->{externalKey};

#}
