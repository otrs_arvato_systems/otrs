package Z_Subs;

use strict;
use warnings;

no warnings 'once';

use utf8;
use Encode ();
use Data::Dumper;

use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Kernel::System::ObjectManager;
# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Webservice arvato Systems (OTRS <-> Remedy)',
    },
);

use Z_SOAP;
my $Webservice = Z_SOAP->new();

sub new {
    my ( $Type, %Param ) = @_;

    # allocate new hash for object
    my $Self = {};
    bless( $Self, $Type );

	return $Self;
}


sub getTicketListfromSoapResponse {

    my ( $Self, %Param ) = @_;

    if (!$Param{Operation}
         || !$Param{Result}) {

	die "Please use Operation and Result as Input Parameters for sub: getTicketListfromSoapResponse";
    }

    # print "DEBUG: Operation: $Param{Operation}\n";
    # Change The "Request" in the Operation to "Response" to be able to read the soap response
    my $ResponseName = $Param{Operation};
    $ResponseName =~ s/Request/Response/g;
    # print "DEBUG: ResponseName: $ResponseName\n";
    # print "DEBUG: Result: ", Dumper($Param{Result});
    return [] if !$Param{Result}->{$ResponseName} && !$Param{Result}->{$ResponseName."Response"};

    my $RemedyTicket; 
    $RemedyTicket = $Param{Result}->{$ResponseName}->{list} if $Param{Result}->{$ResponseName}->{list};
    $RemedyTicket = $Param{Result}->{$ResponseName."Response"} if $Param{Result}->{$ResponseName."Response"};
    return $RemedyTicket;

}

sub GetOTRSTicketID {

	my ( $Self, %Param ) = @_;

	if (!$Param{RemedyTicket}
	 || !$Param{Config}) {

		die "Please use RemedyTicket and Config as Input Parameters for sub: GetOTRSTicketID";
	}	

    $Param{RemedyTicket}->{externalKey} =~ s/Commerce#//g if ($Param{RemedyTicket}->{externalKey});

    my $OTRSTicketID; 

####################################################
### Is the Remedy Ticket linked to an OTRS Ticket?
###   First check if there is an OTRS Reference in the Remedy Ticket
###   Secondly check if there is a Remedy Reference in an OTRS Ticket
###   If neither are true, create a new OTRS Ticket and proceed
#####################################################

    if ($Param{RemedyTicket}->{incidentID}) {

	$Kernel::OM->Get('Kernel::System::DB')->Prepare(
			SQL   => "SELECT ticket.id From ticket, dynamic_field_value
											WHERE ticket.id = dynamic_field_value.object_id
											AND dynamic_field_value.field_id = $Param{Config}->{DynamicField_RemedyTicketNumber_ID}
											AND dynamic_field_value.value_text = \"$Param{RemedyTicket}->{incidentID}\"",
			Limit => 1
	);

	my @Row = $Kernel::OM->Get('Kernel::System::DB')->FetchrowArray();
	$OTRSTicketID = $Row[0] if defined $Row[0];

    }
#############################################################
### Check if there is an OTRS TicketID from the Remedy / OTRS Referenced TicketNumber
#############################################################

	if (!$OTRSTicketID && defined $Param{RemedyTicket}->{externalKey}) {
		$Kernel::OM->Get('Kernel::System::DB')->Prepare(
			SQL   => "SELECT id FROM ticket where tn = \"$Param{RemedyTicket}->{externalKey}\"",
			Limit => 1
		);

		my @Row2 = $Kernel::OM->Get('Kernel::System::DB')->FetchrowArray();
		$OTRSTicketID = $Row2[0] if defined $Row2[0];

	}

	return $OTRSTicketID;

}

sub UpdateRemedyTicketNumber {

	my ( $Self, %Param ) = @_;

	if (!$Param{Config}
	 || !$Param{OTRSTicket}
	 || !$Param{RemedyTicket}) {

		 die "Please use Config, OTRSTicket and RemedyTicket as Input Parameters for sub: UpdateRemedyTicketNumber";
	}

	my $DynamicFieldConfig = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
			ID   => $Param{Config}->{DynamicField_RemedyTicketNumber_ID},
	);

	my $Success = $Kernel::OM->Get('Kernel::System::DynamicField::Backend')->ValueSet(
		DynamicFieldConfig => $DynamicFieldConfig,
		ObjectID => $Param{OTRSTicket}{TicketID},
		Value => $Param{RemedyTicket}->{incidentID},
		UserID => $Param{Config}->{WebserviceUserID},
	);

	# DEBUG
	#print "DEBUG: RemedyTicketNumber Update\n";

	return 1;

}


sub UpdateRemedyExternalKey {

        my ( $Self, %Param ) = @_;

        if (!$Param{Config}
         || !$Param{OTRSTicket}
         || !$Param{RemedyTicket}) {

                 die "Please use Config, OTRSTicket and RemedyTicket as Input Parameters for sub: UpdateRemedyTicketNumber";
        }

        my $DynamicFieldConfig = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
                        ID   => $Param{Config}->{DynamicField_ExternalKey_ID},
        );

        my $Success = $Kernel::OM->Get('Kernel::System::DynamicField::Backend')->ValueSet(
                DynamicFieldConfig => $DynamicFieldConfig,
                ObjectID => $Param{OTRSTicket}{TicketID},
                Value => $Param{RemedyTicket}->{externalKey},
                UserID => $Param{Config}->{WebserviceUserID},
        );

        # DEBUG
        print "DEBUG: RemedyExternalKey Update\n";

        return 1;

}

sub GetCustomerUser {

	my ( $Self, %Param ) = @_;

	if (!$Param{Config}
	 || !$Param{RemedyTicket}) {

		 die "Please use Config and RemedyTicket as Input Parameters for sub: GetCustomerUser";
	}

	return 0 if !$Param{RemedyTicket}->{customerMail};

	my $Emailadress = $Param{RemedyTicket}->{customerMail};

	# email search
	my %CustomerUser = $Kernel::OM->Get('Kernel::System::CustomerUser')->CustomerSearch(
		PostMasterSearch => $Emailadress,
		Valid            => 1,
	);

	if (%CustomerUser) {

		my @Login = keys %CustomerUser;
		return $Login[0];

	} else {

		my $Firstname;
		my $Lastname;

		if ($Emailadress =~ /^(.*?)\.(.*?)\@/) {
			$Firstname = ucfirst $1;
			$Lastname = ucfirst $2;
		} elsif ($Emailadress =~ /(.*?)\@/) {
			$Lastname = ucfirst $1;
		}

		$Firstname = 'tbd' if !$Firstname;
		$Lastname = 'tbd' if !$Lastname;

		my $UserLogin = $Kernel::OM->Get('Kernel::System::CustomerUser')->CustomerUserAdd(
			Source         => 'CustomerUser', # CustomerUser source config
			UserFirstname  => $Firstname,
			UserLastname   => $Lastname,
			UserCustomerID => $Param{RemedyTicket}->{debitorNo}, # $Param{Config}->{$Param{RemedyTicket}->{company}},
			UserLogin      => $Emailadress,
			# UserPassword   => $Emailadress, # not required
			UserEmail      => $Emailadress,
			ValidID        => 1,
			UserID         => $Param{Config}->{WebserviceUserID},
		);
	        return $UserLogin;
	}
}


sub UpdateQueue {

     my ( $Self, %Param ) = @_;

        if (!$Param{Config}
         || !$Param{OTRSTicket}
         || !$Param{RemedyTicket}) {

                 die "Please use Config, OTRSTicket and RemedyTicket as Input Parameters for sub: UpdateTicketType";
        }

        # print "DEBUG: Remedy Tickettype: $Param{RemedyTicket}->{ticketType}\n";

        my $Queue = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::GroupMapping")->{$Param{RemedyTicket}->{currentGroup}} || "User helpdesk";

        # print "DEBUG: OTRS Tickettype: $Type\n";


        if ($Param{OTRSTicket}{Queue} ne $Queue) {

            my $Success = $Kernel::OM->Get('Kernel::System::Ticket')->TicketQueueSet(
                    Queue => $Queue,
                    TicketID => $Param{OTRSTicket}{TicketID},
                    UserID => $Param{Config}->{WebserviceUserID},
            );

            #print "DEBUG: The Queue of ticket ($Param{OTRSTicket}{TicketNumber}) was updated to $Queue\n";

        }

    return 1;

}


sub UpdateTicketType {

     my ( $Self, %Param ) = @_;

	if (!$Param{Config}
	 || !$Param{OTRSTicket}
	 || !$Param{RemedyTicket}) {

		 die "Please use Config, OTRSTicket and RemedyTicket as Input Parameters for sub: UpdateTicketType";
	}

	# print "DEBUG: Remedy Tickettype: $Param{RemedyTicket}->{ticketType}\n";

	my $Type = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::TypeMapping")->{$Param{RemedyTicket}->{ticketType}};

	# print "DEBUG: OTRS Tickettype: $Type\n";

	if (!$Type) {
		$Webservice->ErrorHandling(
			ErrorMessage => "$Param{OTRSTicket}{TicketID}, no TypeMapping-entry found for Remedy Type $Param{RemedyTicket}->{ticketType}",
		);
		die "$Param{OTRSTicket}{TicketID}, no TypeMapping-entry found for Remedy Type $Param{RemedyTicket}->{ticketType}\n";
	}

	if ($Param{OTRSTicket}{Type} ne $Type) {

		my $Success = $Kernel::OM->Get('Kernel::System::Ticket')->TicketTypeSet(
			Type => $Type,
			TicketID => $Param{OTRSTicket}{TicketID},
			UserID => $Param{Config}->{WebserviceUserID},
		);	

	    #print "DEBUG: The Tickettype of ticket ($Param{OTRSTicket}{TicketNumber}) was updated to $Type\n";   

	}

    return 1;

}

sub UpdateTicketPriority {

	my ( $Self, %Param ) = @_;

	if (!$Param{Config}
	 || !$Param{OTRSTicket}
	 || !$Param{RemedyTicket}) {

		 die "Please use Config, OTRSTicket and RemedyTicket as Input Parameters for sub: UpdateTicketPriority";
	}

	# print "DEBUG: Remedy Priority: $Param{RemedyTicket}->{ticketType}\n";

	my $Priority = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::PriorityMapping")->{$Param{RemedyTicket}->{priority}};

	# print "DEBUG: OTRS Priority: $Priority\n";

	if (!$Priority) {
			$Webservice->ErrorHandling(
					  ErrorMessage => "$Param{OTRSTicket}{TicketID}, no PriorityMapping-entry found for Remedy Priority $Param{RemedyTicket}->{priority}",
			);
	die "$Param{OTRSTicket}{TicketID}, no PriorityMapping-entry found for Remedy Priority $Param{RemedyTicket}->{priority}\n";
	}

	if ($Param{OTRSTicket}{Priority} ne $Priority) {

		my $Success = $Kernel::OM->Get('Kernel::System::Ticket')->TicketPrioritySet(
			Priority => $Priority,
			TicketID => $Param{OTRSTicket}{TicketID},
			UserID => $Param{Config}->{WebserviceUserID},
		);

		#print "DEBUG: The Priority of ticket ($Param{OTRSTicket}{TicketNumber}) was updated to $Priority\n";

	}

	return 1;

}

sub UpdateTicketDetails {

	my ( $Self, %Param ) = @_;

	if (!$Param{Config}
	 || !$Param{OTRSTicket}) {

		 die "Please use Config, OTRSTicket and RemedyTicket as Input Parameters for sub: UpdateTicketDetails";
	}
	return 1 if !$Param{Details};
	return 1 if $Param{Details} eq "";

	# Update Ticket-Details
	my $DynamicFieldConfig = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
			ID   => $Param{Config}->{DynamicField_Details_ID},
	);

	my $RemedyDetails = "DynamicField_".$DynamicFieldConfig->{Name};

	if (defined $Param{Details} && (defined $Param{OTRSTicket}{$RemedyDetails} ne $Param{Details})) {

		my $Success = $Kernel::OM->Get('Kernel::System::DynamicField::Backend')->ValueSet(
			DynamicFieldConfig => $DynamicFieldConfig,
			ObjectID => $Param{OTRSTicket}{TicketID},
			Value => $Param{Details},
			UserID => $Param{Config}->{WebserviceUserID},
		);
		
		#print "DEBUG: Details updated\n";
		my $Details = $Param{Details};
		
		my $tmp = $Param{OTRSTicket}{$RemedyDetails};
		$Details =~ s/\Q$tmp\E//g if $tmp;
		
		my $ArticleID = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleCreate(
			TicketID => $Param{OTRSTicket}{TicketID},
			ArticleType => 'note-internal',                        
			SenderType => 'agent',                                
			From => 'ticket@arvato-systems.de',       
			To => 'otrs@arvato-systems.de', 
			Subject => 'Details update',
			Body => $Details,                  
			ContentType => 'text/plain; charset=ISO-8859-15',      
			HistoryType => 'AddNote',
			HistoryComment => 'internal note added via Interface',
			UserID => $Param{Config}->{WebserviceUserID},
		) if $Details =~ /\d|\w/g;
		
		#print "DEBUG: Article created: $ArticleID\n" if $ArticleID;

	}

	return 1;

}

sub UpdateTicketState {

    my ( $Self, %Param ) = @_;

	if (!$Param{Config}
	 || !$Param{OTRSTicket}
	 || !$Param{RemedyTicket}) {

		 die "Please use Config, OTRSTicket and RemedyTicket as Input Parameters for sub: getTicketListfromSoapResponse";
	}

	# print "DEBUG: Remedy Ticketstate: $Param{RemedyTicket}->{status}, OTRS Ticketstate: $Param{OTRSTicket}{State}\n";

	my $State = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::StateMapping")->{$Param{RemedyTicket}->{status}};

	if (!$State) {
		$Webservice->ErrorHandling(
			ErrorMessage => "$Param{OTRSTicket}{TicketID}, no StateMapping-entry found for Remedy State $Param{RemedyTicket}->{status}",
		);
		die "$Param{OTRSTicket}{TicketID}, no StateMapping-entry found for Remedy State $Param{RemedyTicket}->{status}\n";
	}

	# Shaun 20170620 - Pending state issue
        if($Param{OTRSTicket}{State} && $Param{OTRSTicket}{State} =~ /pending/ && $Param{RemedyTicket}->{status} eq 'Assigned') {
                return 1;
        }


    if ($Param{OTRSTicket}{State} ne $State) {

###############################################################
### If the Remedy Ticket is wrongly assigned, the Ticket will be assigned back to the OTRS
###############################################################
		#if( defined $Param{RemedyTicket}->{misc} )
		#{
		#	$Webservice->ErrorHandling(
        	#                ErrorMessage => "Misc is defined for ticket $Param{OTRSTicket}{TicketID}, Misc:  $Param{RemedyTicket}->{misc}",
	        #        );

		#}
		#if (defined $Param{RemedyTicket}->{misc} eq 'Not arvato Systems Infrastructure Responsibility') {
		if ($Param{RemedyTicket}->{status} eq 'Resolved' && defined $Param{RemedyTicket}->{misc} ne '' && lc($Param{RemedyTicket}->{misc}) ne 'ticket solved') {
			
			# Sending an email to myself to troubleshoot the state update
			#my $tmp = $Webservice->ErrorHandling(
			#	ErrorMessage => "$Param{OTRSTicket}{TicketID}, updating Remedy Misc value $Param{RemedyTicket}->{misc} with state $State",
			#);

			# Update RemedyMisc value
			my $RemedyMiscDynamicFieldConfig = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
				ID	=> $Param{Config}->{DynamicField_RemedyMisc_ID},
			);
			
			my $Success = $Kernel::OM->Get('Kernel::System::DynamicField::Backend')->ValueSet(
				DynamicFieldConfig => $RemedyMiscDynamicFieldConfig,
			        ObjectID           => $Param{OTRSTicket}{TicketID},
        			Value              => $Param{RemedyTicket}->{misc},
        			UserID             => 1,
			);

                        if (!$Success) {
                                my $tmp = $Webservice->ErrorHandling(
                                        ErrorMessage => "$Param{OTRSTicket}{TicketID}, Remedy Misc value unable to update to $Param{RemedyTicket}->{misc}",
                                );
                        }
			else
			{			
				# Perform Remedy Ticket Number remove
                                my $DynamicFieldConfig = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
	                                ID   => $Param{Config}->{DynamicField_RemedyTicketNumber_ID},
                                );

                                $Success = $Kernel::OM->Get('Kernel::System::DynamicField::Backend')->ValueDelete(
                                        DynamicFieldConfig => $DynamicFieldConfig,
                                        ObjectID => $Param{OTRSTicket}{TicketID},
                                        UserID => $Param{Config}->{WebserviceUserID},
                                );

                                if (!$Success) {
                                        my $tmp = $Webservice->ErrorHandling(
                                        	ErrorMessage => "$Param{OTRSTicket}{TicketID}, Failed to remove RemedyTicketNumber from this ticket",
                                        );
                                }
				else
				{
					# Update Queue to Bechtle
	                                $Success = $Kernel::OM->Get('Kernel::System::Ticket')->TicketQueueSet(
        	                                Queue => "Service Desk", # $Param{Config}->{DispatchingQueue},
                	                        TicketID => $Param{OTRSTicket}{TicketID},
                        	                UserID => 1, # $Param{Config}->{WebserviceUserID},
                               		);

	                        	if (!$Success) {
	                                	my $tmp = $Webservice->ErrorHandling(
	                                        	ErrorMessage => "$Param{OTRSTicket}{TicketID}, Wrongly assigned Remedy Ticket could not be moved to Service Desk Queue",
	                                	);
	                        	}
				}
			}
		}
		else {

			# State Update
			my $Success = $Kernel::OM->Get('Kernel::System::Ticket')->TicketStateSet(
				State     => $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::StateMapping")->{$Param{RemedyTicket}->{status}},
				TicketID  => $Param{OTRSTicket}{TicketID},
				UserID    => $Param{Config}->{WebserviceUserID},
			);

			if (!$Success) {
				my $tmp = $Webservice->ErrorHandling(
					ErrorMessage => "State could not be updated from Remedy for Ticket $Param{OTRSTicket}{TicketID}",
				);
			}

#			print "DEBUG: StatusUpdate von $Param{OTRSTicket}{State} auf ", $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::StateMapping")->{$Param{RemedyTicket}->{status}}, "\n";

#################################################################################
### When the ticket is set to resolved an article with the incident cause and solution as well as the details is created
#################################################################################
			if ($Param{RemedyTicket}->{status} eq 'Resolved') {

				my $ArticleID = $Kernel::OM->Get('Kernel::System::Ticket')->ArticleCreate(
					TicketID => $Param{OTRSTicket}{TicketID},
					ArticleType => $Param{Config}->{SolvedArticleType},
					SenderType => 'agent',
					Subject => "$Param{RemedyTicket}->{incidentID} solved",
					Body => "Cause: $Param{RemedyTicket}->{incidentCause}\nSolution: $Param{RemedyTicket}->{incidentSolution}",
					ContentType => 'text/plain; charset=ISO-8859-15',
					HistoryType => 'AddNote',
					HistoryComment => 'Insert note from Remedy',
					UserID => $Param{Config}->{WebserviceUserID},
				);
				if (!$ArticleID) {
					my $tmp = $Webservice->ErrorHandling(
						ErrorMessage => "Article (note-solved) could not be created for Ticket $Param{OTRSTicket}{TicketID}",
					);
				} else {
					#print "DEBUG: Article: $ArticleID erstellt.\n";
				}
			}
		}
    }
    return 1;
}
