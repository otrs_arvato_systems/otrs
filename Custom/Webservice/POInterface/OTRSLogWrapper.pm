package OTRSLogWrapper;

use strict;
use warnings;
use utf8;

# Path to search for modules
#use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/PDIS/users/ggpotrs/perl5/lib/perl5";
use lib "/opt/otrs/";
use lib "/opt/otrs/Custom/Webservice/POInterface";

use MIME::Base64;
use Kernel::System::ObjectManager;
use Exporter;

our @ISA= qw( Exporter );

# these CAN be exported.
our @EXPORT_OK = qw(get_otrs_logger);


sub new {
  my $class = shift;

  my $self = {
    __log_object => shift,
    __other_logger => shift
  };

  bless $self, $class;
  return $self;
}

sub debug {
  my ($self, $message) = @_;
  $self->__log('debug', $message);
}
sub info {
  my ($self, $message) = @_;
  $self->__log('info', $message);
}
sub warn {
  my ($self, $message) = @_;
  $self->__log('warn', $message);
}
sub error {
  my ($self, $message) = @_;
  $self->__log('error', $message);
}

sub __log {
  my ($self, $level, $message) = @_;

  if ($self->{__other_logger}) {
    $self->{__other_logger}->$level($message);
  }
  $self->{__log_object}->Log(
    Priority => ($level eq 'warn' ? 'notice' : $level),
    Message  => $message
  );
}

sub get_otrs_logger {
  my $om = shift; 
  my $other_logger = shift; 
  my $LogObject = $om->Get('Kernel::System::Log');
  return new OTRSLogWrapper($LogObject, $other_logger);
}

