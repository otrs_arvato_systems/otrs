# This module, wrap webservice calls owned by PO
# Author: Muhammad Ichsan <muhammad.ichsan@bertelsmann.de>
#
# This code complies with otrs-interface-compromise Java source code.

# package Arvato::OTRS::PoInterface::ResponseExtractor;
package PoService;

use strict;
use warnings;
use utf8;
#use diagnostics;

# Path to search for modules
#use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/PDIS/users/ggpotrs/perl5/lib/perl5";

# Modules required
use SOAP::Lite;
use SOAP::Data::Builder;
use Data::Dumper;

use Exporter 'import';
our @EXPORT_OK = qw(new save_ticket create_article);

=begin nd

Constructor

Params:
  proxy  - The proxy URI defined in the WSDL file: Str.

=cut
sub new {
  my ($class, $proxy) = @_;
  my $ns = 'http://iface.otrs.arvato.com/';

  my $self = {
    _soap => SOAP::Lite
      ->ns($ns, 'fn')
      ->proxy($proxy)
      ->on_action( sub { join '', $ns, $_[1] } ) # To auto populate SOAPAction HTTP header.
      ->autotype(0),
    _server_tz => __read_server_tz()
  };

  bless $self, $class;
  return $self;
}

my ($creds_username, $creds_password);
sub set_basicauth {
  my $self = shift;
  ($main::creds_username, $main::creds_password) = @_;

  sub SOAP::Transport::HTTP::Client::get_basic_credentials {
    return "$main::creds_username" => "$main::creds_password";
  }

  return $self;
}

=begin nd

Enable/disable debugging of WSDL

Params:
  value  - Will be disabled if 0: int.

Returns:
  The value itself.

=cut
sub set_debug {
  my ($self, $value) = @_;
  if ($value) {
    $self->{_soap}->on_debug(sub{print@_});
  } else {
    $self->{_soap}->on_debug(sub{});
  }
  return $self;
}


=begin nd

Create a new ticket

Params:
  data  - All data required to create a new ticket. Read data to retrieve more
        on http://doc.otrs.com/doc/api/otrs/stable/Perl/Kernel/GenericInterface/Operation/Ticket/TicketGet.pm.html
        : Hash.

=cut
sub save_ticket {
  my ($self, $data) = @_;

  my $elem_func = SOAP::Data::Builder->new();

  # Section: /app-id/text()
  $elem_func->add_elem(name => 'AppID', value => 'HPSM');

  if ($data->{DynamicField}->{AppAction}) {
    $elem_func->add_elem(name => 'Action', value => $data->{DynamicField}->{AppAction});
  } elsif ($data->{DynamicField}->{AppTicketID}) {
    $elem_func->add_elem(name => 'Action', value => 'Update');
  } else {
    $elem_func->add_elem(name => 'Action', value => 'Create');
  }

  # Section: /tickets/ticket
  my $elem_tickets = $elem_func->add_elem(name => 'Tickets');
  my $elem_ticket = $self->__const_ticket($elem_tickets, $data, 1, 0); #No article to send along with the ticket, because HPSM doesn't support it

  # my $response = $self->{_soap}->MT_OTRS_Outbound_Request($elem_func->to_soap_data);
  my $response = $self->{_soap}->Request($elem_func->to_soap_data);

  if ($response->fault) {
    die ($response->faultstring);
  } else {
    my $payload = $response->result;
    my $app_ticket_id = $response->valueof('//Ticket/AppTicketID');
    return $app_ticket_id;
  }

}

sub create_article {
  my ($self, $data, $app_ticket_id) = @_;

  my $elem_func = SOAP::Data::Builder->new();

  # Section: /AppID/text()
  $elem_func->add_elem(name => 'AppID', value => 'HPSM');
  $elem_func->add_elem(name => 'Action', value => 'Update');

  # Section: /Ticket
  my $elem_tickets = $elem_func->add_elem(name => 'Tickets');
  my $elem_ticket  = $elem_tickets->add_elem(name => 'Ticket');
  $elem_ticket->add_elem(name => 'AppTicketID', value => $app_ticket_id);

  # Data which belongs to ticket
  $elem_ticket->add_elem(name => 'OTRSTicketNo', value => $data->{TicketNumber});
  $elem_ticket->add_elem(name => 'Responsible', value => xmlesc('David.Schmidt'));
  $elem_ticket->add_elem(name => 'ContactName', value => xmlesc('David.Schmidt'));
  $elem_ticket->add_elem(name => 'Submitter', value => xmlesc('technical.commerce'));
  $elem_ticket->add_elem(name => 'CreateBy', value => xmlesc('technical.commerce')); # According to David
  $elem_ticket->add_elem(name => 'State', value => $data->{State});
  
  if($data->{State} eq 'solved') {
    #$elem_ticket->add_elem(name => 'IncidentSolution', value => 'Resolved Successfully');
    $elem_ticket->add_elem(name => 'IncidentSolution', value => $data->{Body}) if $data->{State} eq "solved";
  }

  # Section: /Ticket/Article
  my $elem_articles = $elem_ticket->add_elem(name => 'Articles');
  my $elem_article  = $self->__const_article($elem_articles, $data, 1, 1);

  # return 1; # Prevent sending update_ticket
  my $response = $self->{_soap}->Request($elem_func->to_soap_data);

  if ($response->fault) {
    die ($response->faultstring);
  } else {
        my $payload = $response->result;
=pod
        my $response_fields;
        # $response_fields->{'DynamicField_AppTicketID'} = $response->valueof("//app-ticket-id") ."\n";

        my $i = 0;
        foreach my $a ($response->dataof("//dynamic-fields/*")) {
          $i++;
          my $elem = $response->dataof("//dynamic-fields/[$i]");
          $name = $elem->attr->{name};
          $value = $elem->attr('name')->value;
          $response_fields->{"DynamicField_$name"} = $value;
        }
        print Dumper($response_fields);
=cut
        return $payload;
  }

}

sub __const_ticket {
  my ($self, $elem_parent, $data, $with_dynamics, $with_articles) = @_;

  my $elem_ticket  = $elem_parent->add_elem(name => 'Ticket');
  $elem_ticket->add_elem(name => 'AppTicketID', value => ($data->{DynamicField}->{AppTicketID}));

  $elem_ticket->add_elem(name => 'OTRSTicketNo', value => $data->{TicketNumber}, attributes => {'xsi:type' => 'int'});
  $elem_ticket->add_elem(name => 'Title', value => xmlesc($data->{Title}));
  $elem_ticket->add_elem(name => 'Service', value => xmlesc('lekkerland multicash produktion')); # Faking hpsm:PrimaryAffectedService

  $elem_ticket->add_elem(name => 'ResponsibleGroup', value => 'Production Control File Transfer Protocol (Arvato Systems)'); # Faking hpsm:AssignmentGroup
  $elem_ticket->add_elem(name => 'Responsible', value => xmlesc('David.Schmidt'));
  $elem_ticket->add_elem(name => 'Type', value => xmlesc($data->{TicketType}));
  $elem_ticket->add_elem(name => 'Queue', value => xmlesc($data->{Queue}));
  $elem_ticket->add_elem(name => 'Priority', value => $data->{PriorityID});
  $elem_ticket->add_elem(name => 'State', value => $data->{State});

  $elem_ticket->add_elem(name => 'CreateBy', value => xmlesc('technical.commerce')); # According to David
  $elem_ticket->add_elem(name => 'CreateTime', value => $self->__reformat_time($data->{Created}), type => 'dateTime')
    if $data->{Created};
  $elem_ticket->add_elem(name => 'ModifyTime', value => $self->__reformat_time($data->{Changed}), type => 'dateTime')
    if $data->{Changed};

  $elem_ticket->add_elem(name => 'ContactName', value => xmlesc('David.Schmidt'));
  $elem_ticket->add_elem(name => 'Submitter', value => xmlesc('technical.commerce'));
#  $elem_ticket->add_elem(name => 'Description', value => xmlesc('Ticket from OTRS'));
  $elem_ticket->add_elem(name => 'Description', value => xmlesc($self->__first_article_content($data->{Article})));

  if ($with_dynamics && $data->{DynamicField}) {
    # Section: /DynamicField[]
    my $elem_dynamics = $elem_ticket->add_elem(name => 'DynamicFields');
    while (my($k, $v) = each %{$data->{DynamicField}}) {
      $self->__const_dynamic_field($elem_dynamics, $k, $v);
    }
  }

  if ($with_articles && $data->{Article}) {
    # Section: /Article[]
    my @articles = @{$data->{Article}};
    my $elem_articles = $elem_ticket->add_elem(name => 'Articles');
    foreach my $article_data (@articles) {
      $self->__const_article($elem_articles, $article_data, 1, 1);
    }
  }

  return $elem_ticket;
}

sub __first_article_content {
  my ($self, $articles) = @_;

  if ($articles) {
    my @articles = @{$articles};
    foreach my $article_data (@articles) {
      return $article_data->{Body};
    }
  }

  return '';
}

sub __const_article {
  my ($self, $elem_parent, $data, $with_dynamics, $with_attachments) = @_;

  my $elem_article = $elem_parent->add_elem(name => 'Article');

  $elem_article->add_elem(name => 'From', value => xmlesc($data->{From}));
  $elem_article->add_elem(name => 'Subject', value => xmlesc($data->{Subject}));
  $elem_article->add_elem(name => 'Body', value => xmlesc($data->{Body}));
  $elem_article->add_elem(name => 'CreateTime', value => $self->__reformat_time($data->{Created}))
    if $data->{Created};

  if ($with_dynamics && $data->{DynamicField}) {
    # Section: /DynamicField[]
    my $elem_dynamics = $elem_article->add_elem(name => 'DynamicFields');
    while (my($k, $v) = each %{$data->{DynamicField}}) {
      $self->__const_dynamic_field($elem_dynamics, $k, $v);
    }
  }

  if ($with_attachments && $data->{Attachment}) {
    # Section: /Attachment[]
    my $elem_attachments = $elem_article->add_elem(name => 'Attachments');
    foreach my $attachment_data (@{$data->{Attachment}}) {
      if (!($attachment_data->{Filename} eq 'file-2')) { # Ignore the message body as an attachment
        $self->__const_attachment($elem_attachments, $attachment_data);
      }
    }
  }

  return $elem_article;
}

sub __const_dynamic_field {
  my ($self, $elem_parent, $name, $value) = @_;
  if (ref($value) eq "HASH" || ref($value) eq "ARRAY") {
    return undef;
  }
  return $elem_parent->add_elem(name => 'DynamicField', attributes => {name => $name}, value => $value);
}

sub __const_attachment {
  my ($self, $elem_article, $data) = @_;
  my $elem_attachment = $elem_article->add_elem(name => 'Attachment');

  $elem_attachment->add_elem(name => 'Filename', value => xmlesc($data->{Filename}));
  $elem_attachment->add_elem(name => 'Content', value => $data->{Content}, attributes => {'xsi:type' => 'base64binary'});
  $elem_attachment->add_elem(name => 'Size', value => $data->{FilesizeRaw});

  return $elem_attachment;
}

sub __reformat_time {
  my ($self, $time) = @_;

  if ($time) {
    substr($time, 10, 1) = 'T';
  }

  $time .= $self->{_server_tz};
  return $time;
}

sub __read_server_tz {
  my @lt = localtime();
  my @gt = gmtime();

  my $hour_diff = $lt[2] - $gt[2];
  my $min_diff  = $lt[1] - $gt[1];

  my $total_diff = $hour_diff * 60 + $min_diff;
  my $hour = int($total_diff / 60);
  my $min = abs($total_diff - $hour * 60);

  my $result = sprintf("%+03d:%02d", $hour, $min); # for +02:00 format

  return $result;
}

sub find_saved {
  my ($self, $time_start, $time_end) = @_;

  my $elem_func = SOAP::Data::Builder->new();
  $elem_func->add_elem(name => 'AppID', value => 'HPSM');
  $elem_func->add_elem(name => 'Action', value => 'RetrieveList');
  $elem_func->add_elem(name => 'TimeStart', value => $self->__reformat_time($time_start));
  $elem_func->add_elem(name => 'TimeEnd', value => $self->__reformat_time($time_end));

  my $response = $self->{_soap}->Request($elem_func->to_soap_data);

  if ($response->fault) {
    die ($response->faultstring);
  } else {
    my $payload = $response->result;
    my @app_ticket_ids;

    foreach ($response->valueof('//Ticket/AppTicketID')) {
      push @app_ticket_ids, $_ if $_ && $_ =~ /(IM\d\d\d\d\d\d\d\d\d+)/;
    }

    return @app_ticket_ids;
  }

}

sub load {
  my ($self, $app_ticket_id) = @_;

  my $elem_func = SOAP::Data::Builder->new();
  $elem_func->add_elem(name => 'AppID', value => 'HPSM');
  $elem_func->add_elem(name => 'Action', value => 'Retrieve');

  # Section: /ticket
  my $elem_tickets = $elem_func->add_elem(name => 'Tickets');
  my $elem_ticket  = $elem_tickets->add_elem(name => 'Ticket');
  $elem_ticket->add_elem(name => "AppTicketID", value => $app_ticket_id);

  my $response = $self->{_soap}->Request($elem_func->to_soap_data);

  if ($response->fault) {
    die ($response->faultstring);
  } else {
    return $self->__to_internal_ticket($response);
  }
}

sub __to_internal_ticket {
  my ($self, $som) = @_;

  my $data = {};

  # Section: /
  $data->{TicketNumber} = $som->valueof('//Ticket/OTRSTicketNo')
    if $som->valueof('//Ticket/OTRSTicketNo');
  $data->{Title} = $som->valueof('//Ticket/Title');
  $data->{Service} = $som->valueof('//Ticket/Service')
    if $som->valueof('//Ticket/Service');
  $data->{Queue} = $som->valueof('//Ticket/Queue')
    if $som->valueof('//Ticket/Queue');
  $data->{TicketType} = $som->valueof('//Ticket/Type')
    if $som->valueof('//Ticket/Type');
  $data->{PriorityID} = $som->valueof('//Ticket/Priority')
    if $som->valueof('//Ticket/Priority');
  $data->{Responsible} = $som->valueof('//Ticket/Responsible')
    if $som->valueof('//Ticket/Responsible');
  $data->{Created} = $som->valueof('//Ticket/CreateTime')
    if $som->valueof('//Ticket/CreateTime');
  $data->{State} = $som->valueof('//Ticket/State')
    if $som->valueof('//Ticket/State');
  $data->{Description} = $som->valueof('//Ticket/Description')
    if $som->valueof('//Ticket/Description');
  $data->{CustomerID} = $som->valueof('//Ticket/CustomerID')
    if $som->valueof('//Ticket/CustomerID');
  $data->{IncidentCause} = $som->valueof('//Ticket/IncidentCause')
    if $som->valueof('//Ticket/IncidentCause');
  $data->{IncidentSolution} = $som->valueof('//Ticket/IncidentSolution')
     if $som->valueof('//Ticket/IncidentSolution');
  $data->{AppTicketID} = $som->valueof('//Ticket/AppTicketID')
     if $som->valueof('//Ticket/AppTicketID');

  # Section: /DynamicField[]
  my $dynamics = {};
  $dynamics->{AppTicketID} = $som->valueof('//Ticket/AppTicketID');
  foreach ($som->dataof('//DynamicField')) {
    # print $_->attr->{Name} ." = ". $_->value() ."\n";
    $dynamics->{$_->attr->{Name}} = $_->value();
  }
  $data->{DynamicField} = $dynamics if $dynamics;

  # Section: /Article[]
  my @articles = $self->__extract_articles($som);
  @{$data->{Article}} = @articles if @articles;

  #print Dumper($data);
  return $data;
}

sub __extract_articles {
  my ($self, $som) = @_;

  my @articles;
  foreach my $article_data ($som->valueof("//Article")) {
    my $article = {};
    $article->{Subject} = $article_data->{Subject};
    $article->{Body} = $article_data->{Body};
    $article->{Created} = $article_data->{CreateTime};

    if ($article_data->{Attachments}) {
      @{$article->{Attachment}} = $self->__extract_attachments($article_data->{Attachments}->{Attachment});
    }

    push @articles, $article;
  }

  return @articles;
}

sub __extract_attachments {
  my ($self, $attachment_ptr) = @_;
  my @attachments;

  foreach my $attachment_data ($self->__som_as_arr($attachment_ptr)) {
    my $attachment = {};
    $attachment->{Filename} = $attachment_data->{Filename};
    $attachment->{FilesizeRaw} = $attachment_data->{Size};
    $attachment->{Content} = $attachment_data->{Content};

    push @attachments, $attachment;
  }

  return @attachments;
}

sub __som_as_arr {
  my ($self, $v) = @_;

  my @data_arr;

  if ((ref $v) eq 'HASH') {
    push @data_arr, $v;
  } elsif ((ref $v) eq 'ARRAY') {
    @data_arr = @{$v};
  }

  return @data_arr;
}

=pod
sub __set_dynamic_val {
  my ($self, $data, $field_name, $field) = @_;

  if ($data) {
    my @dynamics = @{$data};
    foreach my $dynamic_data (@dynamics) {
      if ($dynamic_data->{Name} eq $field_name) {
        return $dynamic_data->{Value};
      }
    }
  }

  return undef;
}
=cut

sub xmlesc {
  my $text = shift;

  if ($text) {
    $text =~ s/&/&amp;/sg;
    $text =~ s/</&lt;/sg;
    $text =~ s/>/&gt;/sg;
    $text =~ s/"/&quot;/sg;
  }

  return $text;
}

return 1;

