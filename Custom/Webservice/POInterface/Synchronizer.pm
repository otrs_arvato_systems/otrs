# To synchronize tickets to PO
#
# Author: Muhammad Ichsan <muhammad.ichsan@bertelsmann.de>
#
package Synchronizer;

use strict;
use warnings;
use utf8;

# Path to search for modules
#use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/PDIS/users/ggpotrs/perl5/lib/perl5";
use lib "/opt/otrs/";
use lib "/opt/otrs/Custom/Webservice/POInterface";

use PoService;
use Data::Dumper;
use MIME::Base64;
use Kernel::System::ObjectManager;
use Try::Tiny;
use Log::Log4perl qw(get_logger);
use OTRSLogWrapper qw(get_otrs_logger);

use Exporter 'import';
our @EXPORT_OK = qw(new syncout);

# Initialize logging behavior
Log::Log4perl->init('/opt/otrs/Custom/Webservice/POInterface/' .'log4perl.conf');
# Obtain a logger instance
my $logger = get_logger("Synchronizer");

my $PO_USER = 117;
my @IGNORED_TICKET_FIELDS = qw(Responsible Queue);

local $Kernel::OM = Kernel::System::ObjectManager->new(
  'Kernel::System::Log' => {
    LogPrefix => 'PO::Synchronizer'
  }
);
my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
my $DynamicFieldObject = $Kernel::OM->Get('Kernel::System::DynamicField');
my $DynamicFieldBackendObject = $Kernel::OM->Get('Kernel::System::DynamicField::Backend');
my $DynamicFieldValueObject = $Kernel::OM->Get('Kernel::System::DynamicFieldValue');
# OTRS logger
$logger = get_otrs_logger($Kernel::OM, $logger);
# $logger->error('Test error. Hi there....');
# $logger->debug('Test debug. Hi there....');exit;

sub new {
  my $class = shift;

  my $self = {
    _iface => shift,
    _persistent => 1,
    _resending_allowed => 0,
  };

  bless $self, $class;

  $self->__new_dynamic_fields();

  return $self;
}

sub __new_dynamic_fields {
  my ($self) = @_;

  my $empty_hashref = {};

  # $self->__cleanup_invalid_fields(qw(AppTicketID SyncArticleSentAmt SyncNumOfTry AppTicketId SyncCutArticleId PropagationCutArticleId PropagationNumOfTry));

  if (!%{$DynamicFieldObject->DynamicFieldGet(Name => 'SyncCutTime')}) {
    $DynamicFieldObject->DynamicFieldAdd(
      FieldOrder  => 1, InternalField => 1, ObjectType  => 'Ticket',
      Name        => 'SyncCutTime',
      Label       => 'Synchronization cut time',
      FieldType   => 'DateTime',
      Config      => $empty_hashref, ValidID => 1, UserID => $PO_USER
    );
  }

  if (!%{$DynamicFieldObject->DynamicFieldGet(Name => 'AppTicketID')}) {
    $DynamicFieldObject->DynamicFieldAdd(
      FieldOrder  => 2, InternalField => 0, ObjectType  => 'Ticket',
      Name        => 'AppTicketID',
      Label       => 'Remote ticket ID',
      FieldType   => 'Text',
      Config      => $empty_hashref, ValidID => 1, UserID => $PO_USER
    );
  }

  if (!%{$DynamicFieldObject->DynamicFieldGet(Name => 'SyncArticleSentAmt')}) {
    $DynamicFieldObject->DynamicFieldAdd(
      FieldOrder  => 3, InternalField => 1, ObjectType  => 'Ticket',
      Name        => 'SyncArticleSentAmt',
      Label       => "Last article ID sync'ed out",
      FieldType   => 'Text',  # Cannot be Int :(
      Config      => $empty_hashref, ValidID => 1, UserID => $PO_USER
    );
  }


  if (!%{$DynamicFieldObject->DynamicFieldGet(Name => 'SyncNumOfTry')}) {
    $DynamicFieldObject->DynamicFieldAdd(
      FieldOrder  => 4, InternalField => 1, ObjectType  => 'Ticket',
      Name        => 'SyncNumOfTry',
      Label       => 'Number of try to synchronize',
      FieldType   => 'Text',  # Cannot be Int :(
      Config      => $empty_hashref, ValidID => 1, UserID => $PO_USER
    );
  }

  if (!%{$DynamicFieldObject->DynamicFieldGet(Name => 'AppTicketState')}) {
    $DynamicFieldObject->DynamicFieldAdd(
      FieldOrder  => 6, InternalField => 0, ObjectType  => 'Ticket',
      Name        => 'AppTicketState',
      Label       => 'Remote ticket state',
      FieldType   => 'Text',
      Config      => $empty_hashref, ValidID => 1, UserID => $PO_USER
    );
  }

}

sub __cleanup_invalid_fields {
  my ($self, @field_names) = @_;

  foreach my $name (@field_names) {
    my $field = $DynamicFieldObject->DynamicFieldGet(Name => $name);
    if ($field->{FieldType} && ($field->{FieldType} eq 'Integer' || $field->{FieldType} eq 'Int')) {
      $DynamicFieldObject->DynamicFieldUpdate(
       ID           => ($field->{ID}),
       Name         => ($field->{Name}),
       Label        => ($field->{Label}),
       FieldOrder   => ($field->{FieldOrder}),
       FieldType    => 'Text',
       ObjectType   => ($field->{ObjectType}),
       Config       => ($field->{Config}),
       ValidID      => ($field->{ValidID}),
       UserID  	    => $PO_USER
      );
    }
    if ($name eq "AppTicketId") {
      $DynamicFieldObject->DynamicFieldUpdate(
       ID           => ($field->{ID}),
       Name         => ('AppTicketID'),
       Label        => ($field->{Label}),
       FieldOrder   => ($field->{FieldOrder}),
       FieldType    => 'Text',
       ObjectType   => ($field->{ObjectType}),
       Config       => ($field->{Config}),
       ValidID      => ($field->{ValidID}),
       UserID 	    => $PO_USER
      );
    }
  }
}

sub sync_out {
  my ($self, $otrs_tid) = @_;

  # Get current ticket data
  my %ticket = $TicketObject->TicketGet(
    TicketID => $otrs_tid,
    UserID => $PO_USER,
    DynamicFields => 1
  );

  if (%ticket) {
    if ($self->{_resending_allowed} || !$ticket{DynamicField_AppTicketID}) {
      $self->sync_out_new_ticket(\%ticket);
    } else {
      $self->sync_out_updated_ticket(\%ticket);
    }
  }
}

sub sync_out_new_ticket {
  my ($self, $ticket) = @_;

  # Mark that this is a first try (new ticket is always the first try)
  $self->__update_ticket_dfield_value($ticket->{TicketID}, 'SyncNumOfTry', 1);

  # Construct a DynamicField array
  $self->__dynamic_fields_as_hash($ticket);

  # Propagate a ticket and the first article to PO
  $self->__add_article1($ticket);

  if (! defined $ticket->{DynamicField}->{ProcessManagementProcessID}) {
    $logger->debug("[TicketID = $ticket->{TicketID}] Ignoring incomplete (being-processed) ticket");
    return 0; # Ignore the first ticket creation from the web, as it's incomplete.
  }

  if ($self->{_resending_allowed}) {
    delete $ticket->{DynamicField}->{AppTicketID};
  }

  my $remote_tid = $self->{_iface}->save_ticket($ticket);

  if (!$remote_tid) {
    return 0;
  }

  # Add first note to say that Remote ticket is received
  $TicketObject->ArticleCreate(
    TicketID 	   => $ticket->{TicketID},
    ArticleType    => 'note-internal',
    SenderType 	   => 'agent',
    Subject 	   => "$remote_tid created",
    Body 	   => "Remote ticket $remote_tid created",
    ContentType    => 'text/plain; charset=ISO-8859-15',
    HistoryType    => 'AddNote',
    HistoryComment => 'Internal note added via Interface',
    UserID 	   => $PO_USER,
  );

  if ($self->{_persistent}) {
    # Record a successful creation
    $self->__update_ticket_dfield_value($ticket->{TicketID}, 'AppTicketID', $remote_tid);
    # Mark last sync cut time (to prevent resending the same ticket)
    $self->__update_ticket_dfield_value($ticket->{TicketID}, 'SyncCutTime', $ticket->{Created});
    # Mark last article ID sync'd ou (HPSM doesn't support sending article immediately
    # $self->__update_ticket_dfield_value($ticket->{TicketID}, 'SyncArticleSentAmt', $ticket->{Article}[0]->{ArticleID});
  }

  # Propagate the rest articles
  my @add_articles = $self->__additional_articles($ticket->{TicketID});
  foreach my $article_data (@add_articles) {
    $self->__enrich_article($article_data);
    # print Dumper($article_data);exit;
    $self->sync_out_article($article_data, $remote_tid);
  }
  $logger->info("[TicketID = $ticket->{TicketID}] sync'd out successfully for the first time. Remote TID = $remote_tid"); 
}

sub set_persistent {
  my ($self, $persistent) = @_;
  $self->{_persistent} = $persistent;
}

sub set_resending_allowed {
  my ($self, $resending_allowed) = @_;
  $self->{_resending_allowed} = $resending_allowed;
}

sub __add_article1 {
  my ($self, $ticket) = @_;

  # Load the first article
  my %article1 = $TicketObject->ArticleFirstArticle(
    TicketID      => $ticket->{TicketID},
    DynamicFields => 1
  );

  # If no article found, return
  if (!%article1) {
    return;
  }

  $self->__enrich_article(\%article1);

  # The only article attached to the ticket
  @{$ticket->{Article}} = (\%article1);
}

sub __dynamic_fields_as_hash {
  my ($self, $hash) = @_;

  my $dynamic_fields = {};
  while (my ($key, $value) = each %$hash) {
    if (index($key, 'DynamicField_') == 0) {
      my $name = $key;
      $name=~ s/DynamicField_//g;
      $dynamic_fields->{$name} = $value;
      delete $hash->{$key};
    }
  }

  if ($dynamic_fields) {
    $hash->{DynamicField} = $dynamic_fields;
  }
}

sub __with_attachments {
  my ($self, $article) = @_;

  # This can't be called immidately via web. Hence, sleep is required.
  my %index = $TicketObject->ArticleAttachmentIndex(
    ArticleID => $article->{ArticleID},
    UserID    => $PO_USER
  );

  my @attachments;
  while (my ($idx, $attachment) = each %index) {
      my %details = $TicketObject->ArticleAttachment(
        ArticleID => $article->{ArticleID},
        FileID    => $idx,   # as returned by ArticleAttachmentIndex
        UserID    => $PO_USER,
      );

      if ($details{Filename} ne 'file-2') { # Skip the message as an attachment
        $attachment->{Content} =  encode_base64($details{Content});
        push @attachments, $attachment;
      }
  }

  if (@attachments) {
    @{$article->{Attachment}} = @attachments;
  }
}

sub __to_field_id {
  my ($self, $field_name) = @_;

  my $DynamicField = $DynamicFieldObject->DynamicFieldGet(
    Name => $field_name
  );

  return $DynamicField->{ID};
}

# Shaun: Method to update process and activity ID in OTRS
sub __update_ticket_process_activity {
my ($self, $object_id, $ticket) = @_;

  return if !$object_id;
  return if !$self->{_persistent};

  my $processID = 'Process-a369e5279a3fd7ab2fce90cfd699643f';
  #$processID = 'Process-a369e5279a3fd7ab2fce90cfd699643f' if $ticket->{Type} eq 'Incident';

  my $processIDTest = $Kernel::OM->Get('Kernel::Config')->Get('CreateConnectedTicket::Type-Process Mapping');
  $logger->info("Test Process ID: $processIDTest->{Incident}");

  my $processID_config = $DynamicFieldObject->DynamicFieldGet( ID => 1 );

  $DynamicFieldBackendObject->ValueSet(
    DynamicFieldConfig => $processID_config,
    ObjectID           => $object_id,
    Value              => $processID,
    UserID             => $PO_USER,
  );

  my $activityID_config = $DynamicFieldObject->DynamicFieldGet( ID => 2 );

  my $activityID = 'Activity-0656dd6522c821002955ea3880ced1fb';
  $activityID = 'Activity-8869b2954467ee7b4ead35b869bb2005' if $ticket->{State} eq 'in progress';

  $DynamicFieldBackendObject->ValueSet(
    DynamicFieldConfig => $activityID_config,
    ObjectID           => $object_id,
    Value              => $activityID,
    UserID             => $PO_USER,
  );
}

# Ichsan: set and get works!
sub __update_ticket_dfield_value {
  my ($self, $object_id, $field_name, $field_value) = @_;

  return if !$object_id;
  return if !$self->{_persistent};

  my $fid = $self->__to_field_id($field_name);
  
  if (!$fid) {
    $logger->warn("[TicketID = $object_id] Dynamic field $field_name was not defined!");
    return;
  }

  my $dyn_field_config = $DynamicFieldObject->DynamicFieldGet(
                           ID => $fid
                         );

  $DynamicFieldBackendObject->ValueSet(
    DynamicFieldConfig => $dyn_field_config,
    ObjectID           => $object_id,
    Value              => $field_value,
    UserID             => $PO_USER,
  );
}

sub __retrieve_ticket_dfield_value {
  my ($self, $object_id, $field_name, $default_value) = @_;

  return if !$object_id;

  my $dyn_field_config = $DynamicFieldObject->DynamicFieldGet(
                           ID => $self->__to_field_id($field_name)
                         );

  my $value = $DynamicFieldBackendObject->ValueGet(
                DynamicFieldConfig => $dyn_field_config,
                ObjectID           => $object_id
              );

  if (defined $value) {
    return $value;
  } else {
    return $default_value;
  }
}

# Get additional articles (created in OTRS) since last sync
sub __additional_articles {
  my ($self, $ticket_id) = @_;

  my @article_ids = $TicketObject->ArticleIndex(
    TicketID => $ticket_id
  );

  my $lastsync_article_amt = $self->__retrieve_ticket_dfield_value($ticket_id, 'SyncArticleSentAmt', 0);
  my @basic_articles; # No attachments

  my $int_article_idx = 0; # Index of articles created by OTRS users
  for (my $i = 0; $i <= $#article_ids; $i++) {
    my $article_id = $article_ids[$i];

    my %article = $TicketObject->ArticleGet(
      ArticleID     => $article_id,
      DynamicFields => 1,
      UserID        => 1
    );

    if ($article{CreatedBy} ne $PO_USER) {
      if ($int_article_idx >= $lastsync_article_amt) {
        $article{_zindex} = $int_article_idx;
        push @basic_articles, \%article;
      }

      $int_article_idx++;
    }
    
  }

  return @basic_articles;
}

sub sync_out_updated_ticket {
  my ($self, $ticket, $sync_out_articles) = @_;
  $sync_out_articles = defined $sync_out_articles ? $sync_out_articles : 1;

  # Construct a DynamicField array
  $self->__dynamic_fields_as_hash($ticket);

  # print "@ sync_out_updated_ticket: ". Dumper($ticket) ."\n";

  no warnings 'uninitialized';
  if ($ticket->{Created} ne $self->__retrieve_ticket_dfield_value($ticket->{TicketID}, 'SyncCutTime')) {
    # Record that this is another try
    my $num_of_try = $self->__retrieve_ticket_dfield_value($ticket->{TicketID}, 'SyncNumOfTry', 0);
    $self->__update_ticket_dfield_value($ticket->{TicketID}, 'SyncNumOfTry', $num_of_try + 1);

    # Propagate the ticket
    $self->{_iface}->save_ticket($ticket);
    $self->__update_ticket_dfield_value($ticket->{TicketID}, 'SyncNumOfTry', 0);

    # Mark a successful update
    $self->__update_ticket_dfield_value($ticket->{TicketID}, 'SyncCutTime', $ticket->{Created});
  }
  
  return if (!$sync_out_articles);

  # Propagate the articles
  my @add_articles = $self->__additional_articles($ticket->{TicketID});
  foreach my $article_data (@add_articles) {
    $self->__enrich_article($article_data);
    my $remote_tid = $self->__retrieve_ticket_dfield_value($ticket->{TicketID}, 'AppTicketID');
    $self->sync_out_article($article_data, $remote_tid);
  }
  $logger->info("[TicketID = $ticket->{TicketID}] sync'd out successfully");
}

sub __enrich_article {
  my ($self, $article) = @_;
  $self->__dynamic_fields_as_hash($article);
  $self->__with_attachments($article);
}

sub sync_out_article {
  my ($self, $article, $remote_tid) = @_;

  $self->{_iface}->create_article($article, $remote_tid);

  # Mark the last article recorded (to prevent resending the same article)
  if ($self->{_persistent}) {
    $self->__update_ticket_dfield_value($article->{TicketID}, 'SyncArticleSentAmt', $article->{_zindex} + 1);
  }
}

sub sync_in_tickets {
  my ($self, $time_start, $time_end) = @_;

  my @app_ticket_ids = $self->{_iface}->find_saved($time_start, $time_end);

  if (@app_ticket_ids) {
    my $fail_counter = 0;

    foreach (@app_ticket_ids) {
      my $tid = $_;

      try {
        $self->sync_in_ticket($tid);
      } catch {
        $self->__send_mail("Failed to sync in ticket (ID = $tid). Cause: $_");
        $fail_counter++;        
      };


      # Totally stop when more than 50% tickets fail to update
      if ($fail_counter > (($#app_ticket_ids + 1) / 2)) {
        $self->__send_mail('Massive ticket sync-in failure. Aborting the rest');
        die('Massive failure');
      }
    }
  }
}


sub __send_mail {
  my ($self, $message) = @_;
  print "Sending mail about: $message\n";
}

sub __create_ticket {
  my ($self, $ticket) = @_;

  my $remote_ticket_id = $ticket->{DynamicField}->{AppTicketID};

  my $dynamic_fields = {};
  if ($ticket->{DynamicField}) {
    $dynamic_fields = $ticket->{DynamicField};
    delete $ticket->{DynamicField};
  }

  my @articles;
  if ($ticket->{Article}) {
    @articles = @{$ticket->{Article}};
    delete $ticket->{Article};
  }
  
  $logger->debug("AppTicketID found = @{[$remote_ticket_id // '--undef--']}");

  # Put the description as the first article
  if ($ticket->{Description}) {
    # add Remote Ticket ID into first article
    my $description = "Remote ticket ID: $remote_ticket_id\n\n$ticket->{Description}";
    my $first_article = { 'Body' => $description };
    unshift @articles, $first_article;
    delete $ticket->{Description};
  }

  $logger->debug("CustomerID = @{[$ticket->{CustomerID} // '--undef--']}");

  # Fill-up missing but required fields
  $ticket->{Type} = 'Incident';
  $ticket->{Lock} = 'unlock';
  $ticket->{State} = 'new';
  $ticket->{CustomerUser} = 'BechtleInterfaceUser';
  $ticket->{OwnerID} = 1;
  $ticket->{UserID} = $PO_USER;
  $ticket->{PriorityID} = 3 if !$ticket->{PriorityID};
  $ticket->{Queue} = $self->__get_main_queue($ticket->{CustomerID}) if !$ticket->{Queue};

  return 0 if !$self->{_persistent};

  my $ticket_id = $TicketObject->TicketCreate(%{$ticket});
  $logger->debug("[TicketID = $ticket_id] This is just created");

  # Shaun: Update Process and Activity ID
  $self->__update_ticket_process_activity($ticket_id, $ticket);

  # Update dynamic fields
  while (my($k, $v) = each %{$dynamic_fields}) {
    $self->__update_ticket_dfield_value($ticket_id, $k, $v);
  }

  # Save articles
  foreach (@articles) {
    $self->__create_article($ticket_id, $_);
  }

  return $ticket_id;
}

sub sync_in_ticket {
  my ($self, $remote_tid) = @_;
  my $ticket = $self->{_iface}->load($remote_tid);

  if ($ticket->{TicketNumber}) {
    $logger->info("Updating ticket ID = $remote_tid (". $ticket->{TicketNumber} .")");
    $self->__update_ticket($ticket);
  } else {
    $logger->debug("Creating ticket ID = $remote_tid");
    my $tid = $self->__create_ticket($ticket);
    $logger->info("[TicketID = $tid] A ticket created from remote TID = $remote_tid");

    if ($tid) {
      my $successful = $self->__sync_out_ticket_no($tid);
      if (!$successful) {
        $TicketObject->TicketDelete(
          TicketID => $tid,
          UserID   => 1,
        );
        $self->__send_mail("Failed to sync out OTRS ticket no (ID = $tid).");
      }
    }
  }
}

sub __sync_out_ticket_no {
  my ($self, $otrs_tid) = @_;

  $logger->debug("[TicketID = $otrs_tid] Sync out ticket number (human readable)");

  # Get current ticket data
  my %ticket = $TicketObject->TicketGet(
    TicketID => $otrs_tid,
    UserID => $PO_USER,
    DynamicFields => 1
  );

  # Add intent which might differentiate in how PO should act
  # $ticket{DynamicField_Intent} = 'SyncOTRSTicketNo';
  $ticket{DynamicField_AppAction} = 'UpdateExtID'; # To override remote action
 
  if (%ticket) {
    # Sending update without articles (with articles = 0)
    $self->sync_out_updated_ticket(\%ticket, 0);
  }

  return 1;
}

sub __update_ticket {
  my ($self, $ticket) = @_;

  my $ticket_id = $TicketObject->TicketIDLookup(
    TicketNumber => $ticket->{TicketNumber},
  );
  
  # Extract related objects
  my $dynamic_fields = {};
  if ($ticket->{DynamicField}) {
    $dynamic_fields = $ticket->{DynamicField};
    delete $ticket->{DynamicField};
  }

  my @articles;
  if ($ticket->{Article}) {
    @articles = @{$ticket->{Article}};
    delete $ticket->{Article};
  }

  # Retrieve the stored ticket and only update its field values if changed.
  my %ret_ticket = $TicketObject->TicketGet(
    TicketID => $ticket_id,
    UserID => $PO_USER,
    DynamicFields => 1
  );
  $self->__dynamic_fields_as_hash(\%ret_ticket);

  # Do not update ticket if it is in Service Desk queue
  #return 1 if $ret_ticket{Queue} eq "Service Desk";

  # Update ticket fields
  while (my($k, $v) = each %{$ticket}) {
    my $ret_v = $ret_ticket{$k};
    if ($ret_v && !($k ~~ @IGNORED_TICKET_FIELDS) && $v ne $ret_v) {
      $logger->debug("[TicketID = $ticket_id] Field $k changed from $ret_v to $v");
      $self->__update_ticket_field_value($ticket_id, $k, $v);
    }
  }

  # Update dynamic fields of the ticket
  while (my($k, $v) = each %{$dynamic_fields}) {
    my $ret_v = $ret_ticket{DynamicField}->{$k};
    if (!$ret_v || $v ne $ret_v) {
      if ($ret_v) {
        $logger->debug("[TicketID = $ticket_id] Field $k dynamic changed from $ret_v to $v");
      } else {
        $logger->debug("[TicketID = $ticket_id] Field $k dynamic created with value $v");
      }
      $self->__update_ticket_dfield_value($ticket_id, $k, $v);
    }
  }

  my $lastsync_article_amt = $self->__retrieve_ticket_dfield_value(
      $ticket_id, 'SyncArticleSentAmt', 0);

#  if ($#articles == 0) {
#    @articles = $self->__do_po_task($articles[0]);
#  }
#  print Dumper(@articles);
#  exit 1;

  # Save new articles
  foreach (@articles) {
    $self->__create_article($ticket_id, $_);
  }
 
  # Shaun: Update Process and ActivityID
  $self->__update_ticket_process_activity($ticket_id, $ticket);

  # Shaun: Remove AppTicketState if state = solved
  if($ticket->{State} eq 'solved' || $ticket->{State} eq 'rejected' || $ticket->{State} eq 'closed') {
    my $appTicketState_Config = $DynamicFieldObject->DynamicFieldGet(Name => 'AppTicketState');
    $Kernel::OM->Get('Kernel::System::DynamicField::Backend')->ValueDelete(
      DynamicFieldConfig => $appTicketState_Config,
      ObjectID => $ticket_id,
      UserID => $PO_USER,
    );
    
    $TicketObject->ArticleCreate(
      TicketID 	     => $ticket_id,
      ArticleType    => 'note-solution',
      SenderType     => 'agent',
      Subject        => "$ticket->{AppTicketID} solved",
      Body           => "Cause: $ticket->{IncidentCause}\nSolution: $ticket->{IncidentSolution}",
      ContentType    => 'text/plain; charset=ISO-8859-15',
      HistoryType    => 'AddNote',
      HistoryComment => 'Internal note added via Interface',
      UserID         => $PO_USER,
    );
  }
}

# This task should be done by PO not by OTRS: Split big description into articles
sub __do_po_task {
  my ($self, $article) = @_;
  my @articles;

  print $article->{Body};
  foreach (reverse(split(/\|/, $article->{Body}))) {
    my ($date, $visibility, $upd_type, $desc) = split(/,/, $_, 4);
    print "$_ =>>> $upd_type\n";
    if (defined $upd_type && $upd_type eq '  Update Type: Update from Customer') {
      my $new_article = {Body => $desc};
      push @articles, $new_article;
    }
  }

  return @articles;
}

sub __update_ticket_field_value {
  my ($self, $ticket_id, $field_name, $field_value) = @_;

  return if !$ticket_id;
  return if !$self->{_persistent};

  if ($field_name eq 'Title') {
    return $TicketObject->TicketTitleUpdate(
      Title    => $field_value,
      TicketID => $ticket_id,
      UserID   => $PO_USER
    );
  }

  elsif ($field_name eq 'Queue') {
    return $TicketObject->TicketQueueSet(
      Queue    => $field_value,
      TicketID => $ticket_id,
      UserID   => $PO_USER
    );
  }

  elsif ($field_name eq 'State') {
    # If state is solved/rejected/closed, move ticket to Service Desk queue and remove its AppTicketID reference
    if($field_value eq 'solved' || $field_value eq 'rejected' || $field_value eq 'closed') {
      return $self->__handle_solved($ticket_id);
    }

    return $TicketObject->TicketStateSet(
      State    => $field_value,
      TicketID => $ticket_id,
      UserID   => $PO_USER
    );
    
  }

  elsif ($field_name eq 'Responsible') {
    return $TicketObject->TicketResponsibleSet(
      NewUser     => $field_value,
      TicketID    => $ticket_id,
      UserID      => $PO_USER
    );
  }

  elsif ($field_name eq 'PriorityID') {
    return $TicketObject->TicketPrioritySet(
      PriorityID  => $field_value,
      TicketID    => $ticket_id,
      UserID      => $PO_USER
    );
  }
}

sub __create_article {
  my ($self, $ticket_id, $article) = @_;

  return if !$ticket_id;

  # Fill-up missing but required fields
  $article->{TicketID}       = $ticket_id;
  $article->{ArticleType}    = 'note-internal';
  $article->{SenderType}     = 'agent';
  $article->{ContentType}    = 'text/plain; charset=utf8';
  $article->{UserID}         = $PO_USER;
  $article->{HistoryType}    = 'AddNote' if !$article->{HistoryType};
  $article->{HistoryComment} = 'Internal note added from HPSM';
  $article->{From}	     = 'HPSM';
  $article->{Subject}	     = 'Internal note added via Interface';

  my $article_id = $TicketObject->ArticleCreate(%{$article});
  return $article_id;
}

sub __get_main_queue {
  my ($self, $customerID) = @_;

  my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$customerID."::General");
  
  my $queue = $ConfigCustomer->{DispatchingQueue} || 'Service Desk';

  return $queue;
}

sub __handle_solved {
  my ($self, $ticket_id) = @_;
  
  my $appTicketID_Config = $DynamicFieldObject->DynamicFieldGet(Name => 'AppTicketID');

  $Kernel::OM->Get('Kernel::System::DynamicField::Backend')->ValueDelete(
    DynamicFieldConfig => $appTicketID_Config,
    ObjectID => $ticket_id,
    UserID => $PO_USER,
  );

  return $TicketObject->TicketQueueSet(
    Queue    => "Service Desk",
    TicketID => $ticket_id,
    UserID   => $PO_USER
  );
}

return 1;

