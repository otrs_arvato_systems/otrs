#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom/Webservice/POInterface/development";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'PO Webservice',
    },
);

#my $P_TicketID;
#if (defined $ARGV[0] ) {
#   $P_TicketID = $ARGV[0];
#} else {
#   print "GetTicket called without TicketID";
   #my $tmp = $Webservice->ErrorHandling(
   #     ErrorMessage => "Remedy_GetIncidentByIncidentID called without TicketID",
   #);
#   exit;
#}

#my %Ticket = $Kernel::OM->Get('Kernel::System::Ticket')->TicketGet(
#	TicketID => $P_TicketID,
#	DynamicFields => 1,
#	UserID => 1,
#);

my $Config = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::Remedy::General");
#my $ConfigCustomer = $Kernel::OM->Get('Kernel::Config')->Get("Webservice::".$Ticket{CustomerID}."::General");

#foreach my $Key (keys %$ConfigCustomer) {
#	$Config->{$Key} = $ConfigCustomer->{$Key}
#}

# Check if the required Config settings are set
#if ( !$Config->{User}
#	|| !$Config->{Password}
#	|| !$Config->{RemedyQueue}
#	|| !$Config->{DispatchingQueue}
#	|| !$Config->{WebserviceUserID}
#	) {
	
#	print "Config for Customer $Ticket{CustomerID} is incomplete";
#	$Webservice->ErrorHandling(
#		ErrorMessage => "Config for Customer $Ticket{CustomerID} is incomplete",
#	);
#	exit;
#}

#my $DynamicField = $Kernel::OM->Get('Kernel::System::DynamicField')->DynamicFieldGet(
#        ID   => $Config->{DynamicField_RemedyTicketNumber_ID},
#);

# prepare the XMLData for the SOAP Request
my $XMLData =
"
<action>Create</action>
<ticket>
        <otrsTicketID>OTRS_Test_Ticket_1</otrsTicketID>
        <title>OTRS Ticket Test, please ignore</title>
        <type>Incident</type>
        <priority>4</priority>
        <submitter>technical.commerce</submitter>
        <state>Open</state>
        <createTime>2017-07-24T05:54:00</createTime>
        <modifyTime>2017-07-24T05:54:00</modifyTime>
	<article>
        	<from>OTRS Sender</from>
        	<subject>Test article 1</subject>
        	<body>Test article content 1</body>
        	<description>Test article description 1</description>
        	<createTime>2017-07-24T05:54:00</createTime>
	</article>
</ticket>
";

my $Operation = 'MT_OTRS_Outbound_Request';

print $XMLData, "\n";

# Call the SOAP Request
my $Result = $Webservice->WSDLRequest(
	Proxy => 'https://po-dev.arvato-systems.de/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_OTRS&receiverParty=&receiverService=&interface=SI_OTRS_To_HPSM_Request_OUT&interfaceNamespace=http://arvato.com/Z_ARVATO_OTRS_XI/HPSM',
	URI => 'http://arvato.com/Z_ARVATO_OTRS/HPSM',
	Operation => $Operation,
	XMLHeader => '',
	XMLData => $XMLData,
);

print Dumper($Result);

#if($Result->{Fault})
#{

#}
#else
#{
#	my $RemedyTicket = $Remedy_Webservice_Subs->getTicketListfromSoapResponse(
#            Result => $Result,
#            Operation => $Operation,
#        );

#        if($RemedyTicket->{externalKey})
#        {
#                $LogObject->Log(
#                        Priority => 'error',
#                        Message => "TEST - Not updating external key for $RemedyTicketNumber. Existing external key $RemedyTicket->{externalKey}."
#                );
#		print "TEST - Not updating external key for $Ticket{'DynamicField_'.$DynamicField->{Name}}. Existing external key $RemedyTicket->{externalKey}.\n";
#                exit 1;
#        }


#	print "External key incoming.....";
#	print $RemedyTicket->{externalKey};

#}
