#!/usr/bin/perl -w

use strict;
use warnings;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";

use Data::Dumper;

use Kernel::System::ObjectManager;
local $Kernel::OM = Kernel::System::ObjectManager->new();

my $DBObject = $Kernel::OM->Get('Kernel::System::DB');

$DBObject->Prepare(
    SQL   => "SELECT id, text FROM notification_event_message",
);

while (my @Row = $DBObject->FetchrowArray()) {
    my $Id = $Row[0];
    my $Text = $Row[1];

    $Text =~ s/&amp;//g;
    $Text =~ s/&lt;/</g;
    $Text =~ s/&gt;/>/g;
    $Text =~ s/<br \/>/\n/g;

    print "$Id: $Text\n";
    #$DBObject->Do( SQL => "UPDATE notification_event_message set text = \"$Text\" WHERE id = $Id" );
    #die "test\n";
}


