#!/usr/bin/perl -w

use strict;
use warnings;
use Data::Dumper;

use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";

use Kernel::System::ObjectManager;
local $Kernel::OM = Kernel::System::ObjectManager->new();

if(!$ARGV[0]) {
        print "TicketID not found";exit;
}

my $TicketID = $ARGV[0];

# get objects
my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
my $TimeObject = $Kernel::OM->Get('Kernel::System::Time');
my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
my $ConfigObject = $Kernel::OM->Get('Kernel::Config');
my $DynamicFieldObject = $Kernel::OM->Get('Kernel::System::DynamicField');
my $BackendObject = $Kernel::OM->Get('Kernel::System::DynamicField::Backend');

# get current ticket data
my %Ticket = $TicketObject->TicketGet(
        TicketID      => $TicketID,
        UserID        => 1,
        DynamicFields => 1,
);

if(!$Ticket{TicketID}) {
        print "Ticket not found for ticket ID $TicketID";exit;
}

if (!($Ticket{State} eq 'closed')) {
        return 1;
}
return 1 if !$Ticket{SLA};

my %SLA = $Kernel::OM->Get('Kernel::System::SLA')->SLAGet(
        SLAID  => $Ticket{SLAID},
        UserID => 1,
);

if (!($SLA{FirstResponseTime} || $SLA{SolutionTime})){
        return 1;
}

$DBObject->Prepare(
        SQL => 'select ticket_state.name, ticket_history.create_time from ticket_history, ticket_state where ticket_id = ? and history_type_id = 27 and state_id = ticket_state.id order by ticket_history.create_time',
        Bind => [ \$Ticket{TicketID} ],
);

my $SecondsInPausedState = 0;
my $PrevState;
my $PrevTime;
while (my @Row = $DBObject->FetchrowArray()) {
        if (defined $PrevState && defined $PrevTime) {
            if ( $ConfigObject->Get('SLATimeFreeze::PausedStates')->{$PrevState}) {
                $SecondsInPausedState = $SecondsInPausedState +
                ( $TimeObject->TimeStamp2SystemTime( String => $Row[1] ) -
                  $TimeObject->TimeStamp2SystemTime( String => $PrevTime ) )
            }
        }
        $PrevState =  $Row[0];
        $PrevTime  =  $Row[1];
}

    #$Kernel::OM->Get('Kernel::System::Log')->Log(
    #    Priority => 'error',
    #    Message  => "DEBUG",
    #);

my $CloseTime = $TimeObject->TimeStamp2SystemTime( String => $Ticket{DynamicField_SolvedDateTime} ) + (7*86400);
my $StopTime = $CloseTime - $SecondsInPausedState;

print "StopTime = $StopTime\n";
print "CreateTime = $Ticket{CreateTimeUnix}\n";

my $WorkingTime = $TimeObject->WorkingTime(
        StartTime => $Ticket{CreateTimeUnix},
        StopTime  => $StopTime,
        Calendar  => $SLA{Calendar},
);

print "WorkingTime = $WorkingTime\n";

if ($Ticket{State} eq 'closed') {

        return 1 if !$SLA{SolutionTime};

        my $DynamicFieldConfig = $DynamicFieldObject->DynamicFieldGet(
            ID   => 38,
        );

        my $DF_Name = "DynamicField_".$DynamicFieldConfig->{Name};
        #return 1 if $Ticket{$DF_Name};

        my $Time = ($SLA{SolutionTime}*60)-$WorkingTime;
        my $Minutes = $Time/60;
        $Minutes =~ s/\.\d+//g;
        my $Seconds = ($Time/60)%60;

	print "SLA SolutionTime*60 = ".($SLA{SolutionTime}*60)."\n";

        #my $Success = $BackendObject->ValueSet(
        #    DynamicFieldConfig => $DynamicFieldConfig,
        #    ObjectID           => $TicketID,
        #    Value              => $Minutes."m ".$Seconds."s",
        #    UserID             => 1,
        #);
        print "[Ticket $TicketID] Closed SolutionTimeDifference = $Minutes m $Seconds s\n";
}
1;

