#!/usr/bin/perl -w

use strict;
use warnings;
use Data::Dumper;

use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";

use Kernel::System::ObjectManager;
local $Kernel::OM = Kernel::System::ObjectManager->new();

# get objects
my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
my $DynamicFieldObject = $Kernel::OM->Get('Kernel::System::DynamicField');
my $BackendObject = $Kernel::OM->Get('Kernel::System::DynamicField::Backend');

if(!$ARGV[0]) {
        print "CustomerID not found";exit;
}

my $CustomerID = $ARGV[0];

if (!$Kernel::OM->Get('Kernel::Config')->Get('SLA::'.$CustomerID)) {
        print "CustomerID has no SLAs configured\n";exit;
}
my $Config = $Kernel::OM->Get('Kernel::Config')->Get('SLA::'.$CustomerID);


# Prepare SQL for tickets with state that are NOT IN "2 - closed", "5 - removed", "9 - merged", "18 - rejected".
$DBObject->Prepare(
        SQL => 'SELECT * FROM ticket WHERE sla_id IS NULL AND ticket_state_id NOT IN (5,9,18) AND service_id != 1 AND customer_id = ?',
        Bind => [ \$CustomerID ],
);

my @TicketIDs;
while (my @Row = $DBObject->FetchrowArray()) {
        push @TicketIDs, $Row[0];
}

foreach (@TicketIDs) {
    my $TicketID = $_;

    # get current ticket data
    my %Ticket = $TicketObject->TicketGet(
        TicketID      => $TicketID,
        UserID        => 1,
        DynamicFields => 1,
    );

    $Ticket{Type} = 'Unclassified' if !$Ticket{Type};
    $Ticket{Priority} = '3 - medium' if !$Ticket{Priority};

    #if (!$Ticket{Service}) {
    if (!$Ticket{Service} || $Ticket{Service} eq 'undefined' || !$Ticket{DynamicField_SolvedDateTime}) {
        #my $Success = $TicketObject->TicketServiceSet(
        #Service  => 'undefined',
        #TicketID => $TicketID,
        #UserID   => 1,
        #);
        #$Ticket{Service} = 'undefined';
	print "No service or SolvedDateTime found for ticket $TicketID\n";
        next;
    }

    my %ConfigLine;
    foreach my $Key (keys %{$Config}) {
        %ConfigLine = map{split /\=/, $_}(split /\|/, $Key);

        my $SLA = $Config->{$Key};
        #print "Current SLA: $SLA\n";
        foreach my $Key2 (keys %ConfigLine) {
            next if $ConfigLine{$Key2} eq 'Any';;
            next if !$Ticket{$Key2};
            undef $SLA if ($ConfigLine{$Key2} ne $Ticket{$Key2});
        }
        undef %ConfigLine;
        my $Success = $TicketObject->TicketSLASet(
            SLA      => "$SLA",
            TicketID => $TicketID,
            UserID   => 1,
        ) if defined $SLA;
        print "Ticket $TicketID updated with SLA: $SLA\n" if defined $SLA;
        last if defined $SLA;
    }

    local @ARGV = ("$TicketID");
    do "/opt/otrs/Custom/ZSetSLATimesToDynamicField.pl" if $Ticket{State} eq "closed";
}
1;

