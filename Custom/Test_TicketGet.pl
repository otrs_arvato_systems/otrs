#!/usr/bin/perl -w

use strict;
use warnings;

use utf8;

use File::Basename;
use FindBin qw($RealBin);
use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";
use lib "/opt/otrs/Custom";
use lib "/opt/otrs/Custom/Webservice/";
use lib "/opt/otrs/Custom/Webservice/RemedyInterface";

use Data::Dumper;

use Z_SOAP;
my $Webservice = Z_SOAP->new();

use Z_Subs;
my $Remedy_Webservice_Subs = Z_Subs->new();

# create object manager
local $Kernel::OM = Kernel::System::ObjectManager->new(
    'Kernel::System::Log' => {
        LogPrefix => 'Remedy Webservice',
    },
);
my $P_TicketID = $ARGV[0];

if(!$P_TicketID)
{
    print "TicketID not found";
    exit;
}

# Get email object
my $EmailObject = $Kernel::OM->Get('Kernel::System::Email');
my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
#$EmailObject->Send(
#		#From => 'otrs@arvato-systems.de',
#		From => 'marc.blomberg@bertelsmann.de',
#		To => 'service-cc@arvato-systems.de, khaishaun.ng@bertelsmann.de',
#		Subject => 'Test email from OTRS Prod',
#		Charset => 'iso-8859-15',
#		#Charset => 'utf-8',
#		MimeType => 'text/plain',
#		Body => 'Test content',
#);

print "Getting ticket details for ticketID: $P_TicketID...\n";

my %Ticket = $TicketObject->TicketGet(
	TicketID  => $P_TicketID,
	UserID    => 1,
	Extended  => 1,
	DynamicFields => 1,
);

if(%Ticket)
{
	print "Ticket: $P_TicketID\n";
	print Dumper(\%Ticket);
}
