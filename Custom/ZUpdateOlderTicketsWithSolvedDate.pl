#!/usr/bin/perl -w

use strict;
use warnings;

use lib "/opt/otrs/";
use lib "/opt/otrs/Kernel/cpan-lib";

use Kernel::System::ObjectManager;
local $Kernel::OM = Kernel::System::ObjectManager->new();

# get objects
my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');
my $DBObject = $Kernel::OM->Get('Kernel::System::DB');
my $DynamicFieldObject = $Kernel::OM->Get('Kernel::System::DynamicField');
my $BackendObject = $Kernel::OM->Get('Kernel::System::DynamicField::Backend');

# get current ticket data
my %Ticket = $TicketObject->TicketGet(
        TicketID      => $ARGV[1],
        UserID        => 1,
        DynamicFields => 1,
);

$DBObject->Prepare(
        SQL => 'SELECT create_time FROM ticket_history WHERE id = ( SELECT MAX(id) FROM ticket_history WHERE history_type_id = 27 AND state_id = 3 AND ticket_id = ? )',
	Bind => [ \$Ticket{TicketID} ],
);

my $SolvedDateTime;
while (my @Row = $DBObject->FetchrowArray()) {
    $SolvedDateTime = $Row[0];
}

if ( !$SolvedDateTime ) {
    die;
}

my $DynamicFieldConfig = $DynamicFieldObject->DynamicFieldGet(
	ID   => 58,
);

my $Success = $BackendObject->ValueSet(
	DynamicFieldConfig => $DynamicFieldConfig,
        ObjectID           => $ARGV[1],
        Value              => $SolvedDateTime,
	UserID             => 1,
);

1;
