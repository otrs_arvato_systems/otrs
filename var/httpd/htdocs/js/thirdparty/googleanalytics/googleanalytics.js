/* 
* Prod Agent = UA-44919035-3
* Prod Cust. = UA-44919035-4 
* Test Agent = UA-44919035-2
* Test Cust. = UA-44919035-5
*/


(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	if (localStorage.getItem("userType") == "Customer"){
		googleAnalyticsSetCustomer();
	}
	else if (localStorage.getItem("userType") == "Normal"){
		googleAnalyticsSetNormal();
	}	
	else {}	

       function googleAnalyticsSetNormal(){
		ga('create', 'UA-44919035-2','auto','Normal', {'siteSpeedSampleRate': 100});
		localStorage.setItem("userType","Normal");
		ga('Normal.set', 'userId', localStorage.getItem("user_id"));
		ga('Normal.send', 'pageview');
        }

        function googleAnalyticsSetCustomer(){
		ga('create', 'UA-44919035-5','auto','Customer', {'siteSpeedSampleRate': 100});
		localStorage.setItem("userType","Customer");
		ga('Customer.set', 'userId', localStorage.getItem("user_id"));
		ga('Customer.send', 'pageview');
        }

